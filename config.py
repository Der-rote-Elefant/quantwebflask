# -*- coding: UTF-8 -*-

class Config:
    # 设置每次请求结束后会自动提交数据库的改动
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_SIZE = 50
    SQLALCHEMY_MAX_OVERFLOW = 20
    SQLALCHEMY_POOL_RECYCLE = 3600

    # 静态方法
    @staticmethod
    def init_app(app):
        app.secret_key = "BF"
        pass


# 生产环境
class ProductionConfig(Config):
    CLICKHOUSE_HOST = "47.97.251.252"
    CLICKHOUSE_PW = "Baofang660"
    CLICKHOUSE_DATABASE = "stocks"
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://asset:Baofang660@rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com:3306/asset_manager'
    SQLALCHEMY_BINDS = {
        't0': 'mysql+pymysql://mladmin:baofangml@rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com:3306/ml',
        'users': 'mysql+pymysql://bftrader:baofangtrade220@rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com:3306/bftrader',
        'adj': "mysql+pymysql://stock:bfstock330@rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com:3306/stock",
        'm5': "mysql+pymysql://stock:bfstock330@rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com:3306/stock_ths_m5"
    }
    MONGOALCHEMY_DATABASE = "Close"
    MONGOALCHEMY_SERVER = "dds-bp1cdc60acd24ff41221-pub.mongodb.rds.aliyuncs.com:3717"
    MONGOALCHEMY_USER = "root"
    MONGOALCHEMY_PASSWORD = "Baofang660"

    # 缓存
    REDIS_URL = "redis://:Baofang20190730@r-bp1fiid0fim62yfcx5pd.redis.rds.aliyuncs.com:6379/0"


# 个人开发环境
class FangConfig(Config):
    DEBUG = True
    CLICKHOUSE_HOST = "47.97.251.252"
    CLICKHOUSE_PW = "Baofang660"
    CLICKHOUSE_DATABASE = "stocks"

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://bf:Baofang660@8.136.152.203:3306/asset_manager'
    SQLALCHEMY_BINDS = {
        "stock": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/stock",
        "adj": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/stock",
        "stock_day": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/stock_ths_day",
        "stock_m5": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/stock_ths_m5",
        'users': 'mysql+pymysql://bf:Baofang660@8.136.152.203:3306/bftrader',
        "future": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/future",
        "future_m1": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/future_1min",
        "future_m5": "mysql+pymysql://bf:Baofang660@8.136.152.203:3306/future_5min",
    }
    SPARK_MASTER_API = "http://master:18080"
    # 查询时显示原始语句
    SQLALCHEMY_ECHO = False
    # mongodb
    MONGOALCHEMY_DATABASE = "Close"
    MONGOALCHEMY_SERVER = "dds-bp1cdc60acd24ff41221-pub.mongodb.rds.aliyuncs.com:3717"
    MONGOALCHEMY_USER = "root"
    MONGOALCHEMY_PASSWORD = "Baofang660"

    # 定时任务
    JOBS = [
        # 测试模板
        {
            'id': 'fang_test',
            'func': 'app.bf_scheduler.fund:count_real_time_profit_time',
            'args': None,
            'trigger': {
                'type': 'cron',
                'day_of_week': "mon-fri",
                'hour': '9-16',
                'minute': '*/5',
                'second': '0'
            },
        }, {
            'id': 'error2wechat',
            'func': 'app.bf_scheduler.fund:send_error_msg',
            'args': None,
            'trigger': {
                'type': 'cron',
                'day_of_week': "mon-fri",
                'hour': '16',
                'minute': '52',
            },
        }
    ]
    # 缓存
    REDIS_URL = "redis://:@192.168.4.5:6379/0"
    # REDIS_URL = "redis://:Baofang20190730@r-bp1fiid0fim62yfcx5pd.redis.rds.aliyuncs.com:6379/0"


config = {
    'production': ProductionConfig,
    'default': FangConfig,
    'fang': FangConfig,
}
