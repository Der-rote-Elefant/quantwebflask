import json
import threading
import time
import os
from flask import render_template, jsonify, request
from . import main
from ..db_util import data_from_source, check_table_is_exist
from ..extensions import cache, clickhouse_client, for_config
from ..permission import filter_res
from ..models import SparkTask, db, to_list
from ..scheduler.scheduler import firstMatchSpark2DB
import requests
from datetime import datetime


@main.route('/', methods=['GET', 'POST'])
def index(name="BF"):
    return render_template('index.html', name=name)


@main.route('/cheng/create_table', methods=['GET'])
@filter_res()
def create_table():
    db.create_all()
    return "success"


@main.route('/cheng/cul/redisDataList', methods=['GET', "POST"])
@filter_res()
def cul_redis_data_list():
    if "name" in request.json:
        name = request.json["name"]
        cache_map = "looptest.report_{name}*".format(name=name)
    else:
        cache_map = "looptest.report_*"
    keys = list(map(lambda x: {"key": x, "name": "-".join(x.split("_")[1:])},
                    map(lambda x: x.decode("utf-8"), cache.keys(cache_map))))
    return keys


@main.route('/cheng/cul/redisData', methods=['GET', 'POST'])
@filter_res()
def cul_redis_data():
    value = "{" + cache.get(request.json["key"]).decode("utf-8") + "}"
    a = value.replace("\r\n", "").replace(" ", "").replace(",]}", "]}").replace(',}', '}')
    return json.loads(a)


@main.route("/templates_echart", methods=['GET'])
def templates_test():
    return render_template('test.html')


@main.route('/cheng/getLoopResCodes', methods=["GET", 'POST'])
@filter_res()
def get_loop_res_codes():
    if "Id" not in request.args:
        raise Exception("输入Id")
    s = SparkTask.query.get(request.args["Id"])
    codes_sql = "select * from (select code from {table} group by code);".format(table=s.Name)
    return [i[0] for i in clickhouse_client.engine.execute(codes_sql)]


@main.route('/cheng/loop_res_profit', methods=["GET", 'POST'])
@filter_res()
def get_loop_res_profit():
    if "Id" not in request.args:
        raise Exception("输入Id")
    task = SparkTask.query.get(request.args.get("Id"))
    sql = "select * from spark_looptest where application_id='{application_id}'".format(
        application_id=task.ApplicationsId)
    loop_df = data_from_source("spark_looptest", sql)
    loop_df.sort_values(by="date", inplace=True)
    response = {
        "x": list(loop_df["date"].apply(lambda x: x.strftime("%Y-%m-%d"))),
        "y": {
            "profit": list(loop_df["profit"].expanding().sum())
        }
    }
    return response


# todo 以后写 获取具体股票的交易点位
@main.route('/cheng/loop_res_data_by_code', methods=["GET", 'POST'])
def get_loop_res_data_by_code():
    if "Id" not in request.json:
        raise Exception("输入Id")
    task = SparkTask.query.get(request.json["ID"])
    table_name = check_table_is_exist(task)
    if not table_name:
        return {
            "httpCode": -1,
            "msg": "表不存在"
        }
    # todo 暂时直接返回结果
    if "codes" in request.json:
        codes = request.json["codes"]
        sql = "select * from {table_name} where code in ['{codes}'}]".format(table_name=table_name,
                                                                             codes="','".join(codes))
    else:
        sql = "select * from {table_name}".format(table_name=table_name)
    loop_df = data_from_source(table_name, sql)
    # for code,data in
    min_date = loop_df["date"].min()
    max_date = loop_df["close"].max()
    # todo 周期
    sql = "select * from "
    return


def get_code_up_dir():
    dirpath = os.path.join(main.root_path, 'upload_codes')
    if not os.path.exists(dirpath):
        os.mkdir(dirpath)
    return dirpath


@main.route('/cheng/taskList', methods=['GET'])
@filter_res()
def getTask():
    task_list = SparkTask.query.all()
    firstMatchSpark2DB()
    return to_list(task_list)


# 获取回测结果
@main.route('/cheng/taskLoopResTable', methods=['GET'])
@filter_res()
def getTaskLoopResTable():
    if "Id" not in request.args:
        raise Exception("Id不存在")
    id = request.args.get("Id")
    s = SparkTask.query.get(id)
    keys = cache.keys("spark_looptest_summary_{0}".format(s.ApplicationsId))
    if len(keys) > 0:
        result = cache.get(keys[0])
        if result:
            return json.loads(result)
    return []


# 用于创建任务
@main.route('/cheng/createTask', methods=['POST'])
@filter_res()
def createTask():
    file = request.files["file"]
    task_name = request.form.get("Name")
    parameter = request.form.get("Parameter")
    # 暂时加上参数为空的保护
    if parameter.strip() == "":
        parameter = '--packages "com.github.housepower:clickhouse-native-jdbc:2.3-stable"'
    task_description = request.form.get("Description")
    task_type = request.form.get("TaskType")
    if file and file.filename.endswith(".py"):
        filename = str(time.time()) + file.filename
        file_path = os.path.join(get_code_up_dir(), filename)
        file.save(file_path)
        spark_task = SparkTask(Name=task_name, Description=task_description, Parameter=parameter, TaskType=task_type,
                               Status="ready", CreateTime=int(time.time()))
        db.session.add(spark_task)
        db.session.commit()
        # todo 运行代码 待测
        command = 'spark-submit {0} --name "{1}" ' \
                  '--master "spark://master:7077" {2}' \
                  ' task_name="{3}" task_description="{4}" task_type="{5}"' \
            .format(parameter, task_name, file_path, task_name, task_description, task_type)
        threading.Thread(target=lambda c: os.system(c), args=(command,)).start()
        time.sleep(2)
        firstMatchSpark2DB()
    else:
        raise Exception("上传文件不符合规范")
    return "success"


# 用于删除任务
@main.route('/cheng/deleteTask', methods=["GET", 'POST'])
@filter_res()
def delete_task():
    if "Id" not in request.args:
        raise Exception("Id不存在")
    task_id = request.args["Id"]
    spark_task = SparkTask.query.get(task_id)
    db.session.delete(spark_task)
    db.session.commit()
    return "success"


# 用于主动提交状态
@main.route('/cheng/submit_state', methods=["GET", 'POST'])
@filter_res()
def submit_state():
    # todo 待测
    req = request.form
    if "application_id" not in req:
        raise Exception("application_id not in request")
    if "status" not in req:
        raise Exception("status not in request")
    app_db = SparkTask.query.filter_by(ApplicationsId=req["application_id"]).first()
    # fixme 暂时先只更新app_id status
    if app_db is None:
        app_db = SparkTask(ApplicationsId=req["application_id"], Status=req["status"])
        db.session.add(app_db)
    else:
        app_db.Status = req["status"]
    url = for_config.get_config().SPARK_MASTER_API + "/api/v1/applications"
    resp = requests.get(url).json()
    app = list(filter(lambda x: x["id"] == app_db.ApplicationsId, resp))
    if len(app) > 0:
        if app[0]["attempts"][0]["completed"]:
            start_time = datetime.strptime(app[0]["attempts"][0]["startTime"][:19], "%Y-%m-%dT%H:%M:%S")
            end_time = datetime.strptime(app[0]["attempts"][0]["endTime"][:19], "%Y-%m-%dT%H:%M:%S")
            app_db.SpendTime = (end_time - start_time).seconds
    db.session.commit()
    return


##########################################################################################
# todo cheng
@main.route('/cheng/reports/summary', methods=['GET'])
def summary():
    application_id = request.args.get("applicationId")
    if application_id:
        keys = cache.keys("spark_looptest_summary_{0}".format(application_id))
        if len(keys) > 0:
            result = cache.get(keys[0])
            if result:
                return jsonify({
                    "httpCode": 200,
                    "responseEntity": json.loads(result)
                })

        return jsonify({
            "httpCode": -1,
            "errorMsg": "请求的任务不存在"
        })

    else:
        res = []
        keys = cache.keys("spark_looptest_task_*")
        for key in keys:
            result = cache.get(key)
            if result:
                res.append(json.loads(result))
        return jsonify({
            "httpCode": 200,
            "responseEntity": res
        })


@main.route('/cheng/tasks/looptest', methods=['POST'])
def tasks():
    file = request.files["file"]
    parameter = request.form.get("parameter")
    task_name = request.form.get("taskName")
    task_description = request.form.get("taskDescription")
    task_type = request.form.get("taskType")
    if file and file.filename.endswith(".py"):
        filename = str(time.time()) + file.filename
        file_path = os.path.join(get_code_up_dir(), filename)
        file.save(file_path)

        command = 'spark-submit {0} --name "{1}" ' \
                  '--master "spark://master:7077" {2}' \
                  ' task_name="{3}" task_description="{4}"' \
                  ' task_type="{5}"' \
            .format(parameter, task_name, file_path, task_name, task_description, task_type)

        # todo 线程出错了也不知道
        # todo getApplicationId
        threading.Thread(target=lambda c: os.system(c), args=(command,)).start()
    else:
        return jsonify({
            "httpCode": -1,
            "errorMsg": "文件格式不对"
        })
    return jsonify({
        "httpCode": 200,
        "responseEntity": {
            "taskName": task_name
        }
    })
