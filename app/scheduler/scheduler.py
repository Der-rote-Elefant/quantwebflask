from ..extensions import for_config
import requests
from ..models import SparkTask, db
from sqlalchemy import and_
from datetime import datetime
import time


# 用于匹配数据库中的信息与spark中的结果
# 1. 匹配 刚入库的信息
def firstMatchSpark2DB():
    url = for_config.get_config().SPARK_MASTER_API + "/api/v1/applications"
    response = requests.get(url).json()
    sparks = SparkTask.query.filter(and_(SparkTask.ApplicationsId.is_(None), SparkTask.Status == "ready")).order_by(
        SparkTask.CreateTime.desc()).all()
    for s in sparks:
        app = list(filter(lambda x: x["name"] == s.Name, response))
        if len(app) == 0:
            continue
        # 这里可能会有问题
        for a in app:
            a_start = datetime.strptime(a["attempts"][0]["startTime"][:16], "%Y-%m-%dT%H:%M").timestamp()
            if abs(a_start - s.CreateTime) < 30000:
                s.ApplicationsId = app[0]["id"]
                s.Status = "running"
                break
        else:
            now = int(time.time())
            if now - s.CreateTime > 60000:
                s.Status = "error"
    spark_runs = SparkTask.query.filter(SparkTask.Status == "running").all()
    for s in spark_runs:
        app = list(filter(lambda x: x["id"] == s.ApplicationsId, response))
        if len(app) > 0:
            if app[0]["attempts"][0]["completed"]:
                start_time = datetime.strptime(app[0]["attempts"][0]["startTime"][:19], "%Y-%m-%dT%H:%M:%S")
                end_time = datetime.strptime(app[0]["attempts"][0]["endTime"][:19], "%Y-%m-%dT%H:%M:%S")
                s.Status = "success"
                s.SpendTime = (end_time - start_time).seconds
    db.session.commit()
    return
