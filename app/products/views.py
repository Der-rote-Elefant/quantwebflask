# -*- coding: UTF-8 -*-
from flask import jsonify, request, session, send_from_directory, current_app
from sqlalchemy import and_, or_
import datetime
from . import main
from ..util import redis_message
from ..models_user import RoleFund
from ..models import *
import time
import json
import os
from ..util import common
from ..util import index_calculator
from ..util import profit_calculator
from dateutil.relativedelta import relativedelta
from ..extensions import cache
from ..permission import filter
import pandas as pd
import numpy as np
from ..extensions import clickhouse_client

role_type = "product"


def secure_filename(filename):
    return str(int(time.time())) + filename


# 显示首页用户所有的产品
@main.route('/bf/product/list', methods=['GET', 'POST'])
@filter(role_type)
def get_product_list():
    user = session["user"]
    ps = RoleFund.query.filter_by(role_id=user["role_id"]).all()
    if len(ps) == 0:
        return {
            "code": 0,
            "data": "无产品数据",
        }
    ps = [i.fund_id for i in ps]
    funds = Fund.query.filter(and_(Fund.FundType == 0, Fund.Id.in_(ps))).all()
    fund_list = to_list(funds)
    return {
        "code": 1,
        "data": fund_list,
    }


# 显示产品方案
@main.route('/bf/product/strategy', methods=['GET', 'POST'])
@filter(role_type)
def get_product_strategy():
    id = request.args.get("id")
    if id is None:
        return {
            "code": 0,
            "msg": "id为空"
        }
    fund_id = int(id)
    loop_test_info = FundLooptestInfo.query.filter(FundLooptestInfo.FundId == fund_id).order_by(
        FundLooptestInfo.Date.desc()).first()
    loop_test_info_dict = to_dict(loop_test_info)
    return {
        "code": 1,
        "data": [loop_test_info_dict],
    }


@main.route('/bf/product/historyLoopTest', methods=['GET', 'POST'])
@filter(role_type)
def get_history_loop_test():
    fund_id = int(request.args.get("id"))
    loop_test_infos = FundLooptestInfo.query.filter(FundLooptestInfo.FundId == fund_id).order_by(
        FundLooptestInfo.Date.asc())
    loop_test_infos = to_list(loop_test_infos)
    return {
        "code": 1,
        "data": loop_test_infos,
    }


# @main.route('/bf/product/chart', methods=['GET', 'POST'])
# @filter(role_type)
# def get_product_chart():
#     fund_id = int(request.args.get("id"))
#     fund = Fund.query.filter(Fund.Id == fund_id).first()
#     balances = Balance.query.filter(Balance.FundId == fund_id).order_by(Balance.Date).all()
#     min_date = Balance.min if len(balances) <= 0 else balances[0].Date
#     max_date = datetime.datetime.min if len(balances) <= 0 else balances[-1].Date
#
#     base_code = fund.BaseCode.split('.')[0]
#     dict_base_profit = profit_calculator.get_base_profit_dict(base_code, min_date)
#     dict_loop_test_profit = dict()
#     dict_profit = dict()
#
#     list_date_time = list()
#     list_base_profit = list()
#     list_profit = list()
#     list_loop_test_profit = list()
#
#     # 回测收益
#     loop_test_balances = Balance.query.filter(and_(Balance.FundId == fund.BaseFund, Balance.Date >= min_date,
#                                                    Balance.Date <= max_date)).order_by(Balance.Date).all()
#     if loop_test_balances is not None:
#         for loop_test_balance in loop_test_balances:
#             dict_loop_test_profit[loop_test_balance.Date] = float(loop_test_balance.Profit)
#
#     # 真实收益
#     sum_loop_test_profit = 0
#     sum_base_profit = 0
#     sum_profit = 0
#     for balance in balances:
#         list_date_time.append(balance.Date.strftime('%Y-%m-%d'))
#         # 累加真实收益
#         sum_profit = sum_profit + float(balance.Profit)
#         dict_profit[balance.Date] = sum_profit
#         list_profit.append(round(dict_profit[balance.Date], 2))
#
#         # 累加回测收益
#         if balance.Date not in dict_loop_test_profit.keys():
#             dict_loop_test_profit[balance.Date] = sum_loop_test_profit
#         else:
#             sum_loop_test_profit = sum_loop_test_profit + float(dict_loop_test_profit[balance.Date])
#             dict_loop_test_profit[balance.Date] = sum_loop_test_profit
#         list_loop_test_profit.append(round(dict_loop_test_profit[balance.Date], 2))
#
#         # 累加基准收益
#         if balance.Date not in dict_base_profit.keys():
#             dict_base_profit[balance.Date] = sum_base_profit
#         else:
#             sum_base_profit = sum_base_profit + dict_base_profit[balance.Date]
#             dict_base_profit[balance.Date] = sum_base_profit
#         list_base_profit.append(round(dict_base_profit[balance.Date], 2))
#
#     target = []
#     list_datetime = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in list_date_time]
#     res_loop = index_calculator.get_index_target(list_datetime, list_loop_test_profit)
#     res_profit = index_calculator.get_index_target(list_datetime, list_profit)
#     target.append(
#         {"name": "净值", "loop_test": round(list_loop_test_profit[-1] * 0.01 + 1, 4),
#          "profit": round(list_profit[-1] * 0.01 + 1, 4)})
#     for i in range(len(res_loop)):
#         target.append({"name": res_loop[i][0], "loop_test": float(res_loop[i][1]), "profit": float(res_profit[i][1])})
#     return {
#         "code": 20000,
#         "data": {
#             "BaseProfit": list_base_profit,
#             "Datetime": list_date_time,
#             "BaseFundIdProfit": list_loop_test_profit,
#             "Profit": list_profit,
#             "target": target
#         },
#     }


@main.route('/bf/product/newchart', methods=['GET', 'POST'])
@filter(role_type)
def get_product_new_chart():
    fund_id = int(request.args.get("id"))
    fund = Fund.query.filter(Fund.Id == fund_id).first()

    # 真实收益
    dict_profit = dict()
    balances = Balance.query.filter(Balance.FundId == fund_id).order_by(Balance.Date).all()
    if balances is not None:
        for balance in balances:
            dict_profit[balance.Date] = float(balance.Profit)

    min_date = datetime.datetime.min if len(balances) <= 0 else balances[0].Date
    max_date = datetime.datetime.min if len(balances) <= 0 else balances[-1].Date

    base_code = fund.BaseCode.split('.')[0]
    dict_base_profit = profit_calculator.get_base_profit_dict(base_code, min_date)
    dict_loop_test_profit = dict()
    dict_strategy_profit = dict()

    list_date_time = list()
    list_base_profit = list()
    list_profit = list()
    list_loop_test_profit = list()
    list_strategy_profit = list()

    # 冲击收益
    loop_test_balances = Balance.query.filter(and_(Balance.FundId == fund.BaseFund, Balance.Date >= min_date,
                                                   Balance.Date <= max_date)).order_by(Balance.Date).all()
    if loop_test_balances is not None:
        for loop_test_balance in loop_test_balances:
            dict_loop_test_profit[loop_test_balance.Date] = float(loop_test_balance.Profit)

    # 回测收益
    fund_strategy = FundStrategyRelation.query.filter(FundStrategyRelation.FundId == fund_id).first()
    # 空值校验
    if fund_strategy is not None:
        bind_strategy_id = fund_strategy.StrategyId
        strategy = Strategy.query.filter(Strategy.Id == bind_strategy_id)
        strategy_balances = StrategyBalance.query.filter(
            and_(StrategyBalance.StrategyId == bind_strategy_id, StrategyBalance.Date >= min_date,
                 StrategyBalance.Date <= max_date)).order_by(StrategyBalance.Date).all()
        if strategy_balances is not None:
            for strategy_balance in strategy_balances:
                dict_strategy_profit[strategy_balance.Date] = float(strategy_balance.Profit)

    # 真实收益
    sum_loop_test_profit = 0
    sum_strategy_profit = 0
    sum_base_profit = 0
    sum_profit = 0

    loop_test_with_real_index = 0  # 回测曲线与真实曲线对齐点
    first_loop_test = True
    for balance in balances:
        # print(balance.Date)
        list_date_time.append(balance.Date.strftime('%Y-%m-%d'))

        # 累加冲击收益
        if balance.Date not in dict_loop_test_profit.keys():
            dict_loop_test_profit[balance.Date] = sum_loop_test_profit
        else:
            sum_loop_test_profit = sum_loop_test_profit + float(dict_loop_test_profit[balance.Date])
            dict_loop_test_profit[balance.Date] = sum_loop_test_profit
        list_loop_test_profit.append(round(dict_loop_test_profit[balance.Date], 2))

        # 累加回测收益
        if balance.Date not in dict_strategy_profit.keys():
            # dict_strategy_profit[balance.Date] = sum_strategy_profit
            dict_strategy_profit[balance.Date] = dict_profit[balance.Date]  # 回测不存在的数据用真实收益代替
        else:
            loop_test_with_real_index = loop_test_with_real_index + 1
            if loop_test_with_real_index < 5:
                dict_strategy_profit[balance.Date] = dict_profit[balance.Date]  # 前N个仍然用真实收益代替,相当于调仓

        sum_strategy_profit = sum_strategy_profit + float(dict_strategy_profit[balance.Date])
        dict_strategy_profit[balance.Date] = sum_strategy_profit
        list_strategy_profit.append(round(dict_strategy_profit[balance.Date], 2))

        # 累加真实收益
        sum_profit = sum_profit + float(balance.Profit)
        dict_profit[balance.Date] = sum_profit
        list_profit.append(round(dict_profit[balance.Date], 2))

        # 累加基准收益
        if balance.Date not in dict_base_profit.keys():
            dict_base_profit[balance.Date] = sum_base_profit
        else:
            sum_base_profit = sum_base_profit + dict_base_profit[balance.Date]
            dict_base_profit[balance.Date] = sum_base_profit
        list_base_profit.append(round(dict_base_profit[balance.Date], 2))

    target = []
    list_datetime = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in list_date_time]
    res_loop = index_calculator.get_index_target(list_datetime, list_loop_test_profit)
    res_strategy = index_calculator.get_index_target(list_datetime, list_strategy_profit)
    res_profit = index_calculator.get_index_target(list_datetime, list_profit)
    target.append(
        {"name": "净值",
         "loop_test": round(list_loop_test_profit[-1] * 0.01 + 1, 4) if len(list_loop_test_profit) > 0 else 0,
         "profit": round(list_profit[-1] * 0.01 + 1, 4) if len(list_profit) > 0 else 0,
         "strategy": round(list_strategy_profit[-1] * 0.01 + 1, 4) if len(list_strategy_profit) > 0 else 0})
    for i in range(len(res_loop)):
        target.append({"name": res_loop[i][0], "loop_test": float(res_loop[i][1]), "profit": float(res_profit[i][1]),
                       "strategy": float(res_strategy[i][1])})
    return {
        "code": 20000,
        "data": {
            "BaseProfit": list_base_profit,
            "Datetime": list_date_time,
            # "BaseFundIdProfit": list_loop_test_profit,
            "StrategyProfit": list_strategy_profit,
            "Profit": list_profit,
            "target": target
        },
    }


@main.route('/bf/product/looptestCharts', methods=['GET', 'POST'])
@filter(role_type)
def get_loop_test_charts():
    req = request.json
    list_loop_test_id = req["ids"]
    if "type" in req and req["type"] in common.LT:
        time_format = common.LT[req["type"]]
    else:
        time_format = "%Y-%m-%d"
    dfs = []
    columns = []
    for loop_test_id in list_loop_test_id:
        column = "id-" + str(loop_test_id)
        columns.append(column)
        df_new = []
        ltb = LooptestBalanceStrategy.query.filter(
            LooptestBalanceStrategy.LoopTestId == loop_test_id).order_by(LooptestBalanceStrategy.Date).all()
        if not ltb or len(ltb) == 0:
            continue
        df = pd.DataFrame(to_list(ltb))
        df = df.loc[:, ["Date", "Profit"]]
        df["Date"] = df["Date"].apply(
            lambda x: datetime.datetime.strptime(x, common.MODEL_FORMAT).strftime(time_format))
        if time_format != "%Y-%m-%d":
            df["Profit"] = df["Profit"].diff()
        df.ix[0, "Profit"] = 0
        df.columns = ["Date", column]
        for d, g in df.groupby("Date"):
            df_new.append({"Date": d, column: g[column].sum()})
        df_new = pd.DataFrame(df_new)
        df_new.set_index("Date", inplace=True)
        dfs.append(df_new)
    if len(dfs) == 0:
        return {
            "code": 0,
            "msg": "请先上传数据",
        }
    dfs = pd.concat(dfs, axis=1, join='outer')
    dict_result = {}
    dict_result["x"] = list(dfs.index)
    dict_result["y"] = {}
    targets = {}
    for c in columns:
        dict_result["y"][c] = list(dfs[c])
        if time_format == "%Y-%m-%d":
            list_datetime = [datetime.datetime.strptime(d, time_format) for d in dict_result["x"]]
            commons = index_calculator.get_common_index_target(list_datetime, dict_result["y"][c])
            for com in commons:
                if com[0] not in targets:
                    targets[com[0]] = {"name": com[0]}
                targets[com[0]][c] = com[1]
    if time_format == "%Y-%m-%d":
        dict_result["targets"] = list(targets.values())
    return {
        "code": 1,
        "data": dict_result,
    }


# @main.route('/bf/product/realTimeProfitTest', methods=['GET', 'POST'])
# def count_real_time_profit():
#     fund_id = int(request.args.get("id"))
#     history_balance = Balance.query.filter(
#         and_(Balance.FundId == fund_id, Balance.Date < datetime.datetime.strptime(datetime.datetime.today().strftime(common.TRADEFLOW_FORMAT),common.TRADEFLOW_FORMAT))).all()
#
#     today_db_balance = Balance.query.filter(Balance.FundId == fund_id).order_by(Balance.Date.desc()).first()
#     last_position_day = Position.query.filter(Position.FundId == fund_id).order_by(Position.Date.desc()).first().Date
#     today_db_position = Position.query.filter(
#         and_(Position.FundId == fund_id, Position.Date == last_position_day)).all()
#     temp_long_market_value, temp_short_market_value = profit_calculator.get_market_value(today_db_position,
#                                                                                          ask_last=True)
#     profit_dict = profit_calculator.get_temp_position_profit(fund_id)
#     long_market_value_change = 0
#     short_market_value_change = 0
#     for profit_key in profit_dict:
#         profit_info = profit_dict[profit_key]
#         if profit_info.direction == 1:
#             long_market_value_change = long_market_value_change + profit_info.sum_profit()
#         else:
#             short_market_value_change = short_market_value_change + profit_info.sum_profit()
#
#     # print(fund_id, long_market_value_change, short_market_value_change)
#     # 上次流水计算完成后(cash变动完成)与当前价格的市值变化
#     long_market_value_diff = temp_long_market_value - float(today_db_balance.MarketValue)
#     short_market_value_diff = float(today_db_balance.ShortMarketValue) - temp_short_market_value
#     today_profit = float(today_db_balance.Profit) if today_db_balance.Date.strftime(
#         common.LT["day"]) == datetime.datetime.today().strftime(common.LT["day"]) else 0
#     temp_profit = today_profit + (short_market_value_diff + long_market_value_diff) / float(
#         today_db_balance.InitialFunds) * 100
#     net_profit = 0
#     for balance in history_balance:
#         net_profit = net_profit + float(balance.Profit)
#     net_profit = (net_profit + temp_profit + 100) / 100
#
#     stock_0 = {"Code": "SK总计", "DaySumProfit": 0, "MarketValue": 0, "PositionProfit": 0}
#     future_0 = {"Code": "FU总计", "DaySumProfit": 0, "MarketValue": 0, "PositionProfit": 0}
#     stock_list = []
#     future_list = []
#     for profit_key in profit_dict:
#         p = profit_dict[profit_key].to_dict()
#         if "SK" in p["Code"]:
#             stock_list.append(p)
#             stock_0["DaySumProfit"] += p["DaySumProfit"]
#             stock_0["MarketValue"] += p["MarketValue"]
#             stock_0["PositionProfit"] += p["PositionProfit"]
#         else:
#             future_list.append(p)
#             future_0["DaySumProfit"] += p["DaySumProfit"]
#             future_0["MarketValue"] += p["MarketValue"]
#             future_0["PositionProfit"] += p["PositionProfit"]
#     stock_0["DaySumProfit"] = round(stock_0["DaySumProfit"], 2)
#     stock_0["MarketValue"] = round(stock_0["MarketValue"], 2)
#     stock_0["PositionProfit"] = round(stock_0["PositionProfit"], 2)
#     future_0["PositionProfit"] = round(future_0["PositionProfit"], 2)
#     future_0["PositionProfit"] = round(future_0["PositionProfit"], 2)
#     future_0["PositionProfit"] = round(future_0["PositionProfit"], 2)
#     stock_list = [stock_0] + stock_list
#     future_list = [future_0] + future_list
#     print(today_db_balance.InitialFunds)
#     result = {
#         "Time": last_position_day.strftime("%Y-%m-%d"),
#         "NetProfit": round(net_profit, 4),
#         "NetProfitChange": round(temp_profit, 3),
#         "StockMarketValue": round(temp_long_market_value, 2),
#         "FutureMarketValue": round(temp_short_market_value, 2),
#         "StockMarketValueChange": round(long_market_value_change, 2),
#         "FutureMarketValueChange": round(short_market_value_change, 2),
#         "ProfitInfo": [stock_list, future_list],
#         "allMoney": round(float(today_db_balance.InitialFunds) * net_profit, 4)
#     }
#     return result


def count_real_time_profit(fund_id):
    history_balance = Balance.query.filter(
        and_(Balance.FundId == fund_id,
             Balance.Date < datetime.datetime.strptime(datetime.datetime.today().strftime(common.TRADEFLOW_FORMAT),
                                                       common.TRADEFLOW_FORMAT))).all()

    today_db_balance = Balance.query.filter(Balance.FundId == fund_id).order_by(Balance.Date.desc()).first()
    last_position = Position.query.filter(Position.FundId == fund_id).order_by(Position.Date.desc()).first()
    accrual_flows = AccrualFlow.query.filter(AccrualFlow.FundId == fund_id).all()
    accrual_money = 0
    if accrual_flows is not None:
        for accrual_flow in accrual_flows:
            accrual_money = accrual_money + float(accrual_flow.Vol) * float(accrual_flow.Price)
    if last_position is None or today_db_balance is None:
        return {
            "Time": "",
            "NetProfit": "",
            "NetProfitChange": "",
            "StockMarketValue": "",
            "FutureMarketValue": "",
            "StockMarketValueChange": "",
            "FutureMarketValueChange": "",
            "ProfitInfo": [[], []],
            "allMoney": "",
            "accrualMoney": ""
        }
    last_position_day = last_position.Date
    today_db_position = Position.query.filter(
        and_(Position.FundId == fund_id, Position.Date == last_position_day)).all()
    temp_long_market_value, temp_short_market_value, adjust_cash = profit_calculator.get_market_value(today_db_position,
                                                                                                      ask_last=True)
    profit_dict = profit_calculator.get_temp_position_profit(fund_id)
    long_market_value_change = 0
    short_market_value_change = 0
    for profit_key in profit_dict:
        profit_info = profit_dict[profit_key]
        if profit_info.direction == 1:
            long_market_value_change = long_market_value_change + profit_info.sum_profit()
        else:
            short_market_value_change = short_market_value_change + profit_info.sum_profit()

    # print(fund_id, long_market_value_change, short_market_value_change)
    # 上次流水计算完成后(cash变动完成)与当前价格的市值变化
    long_market_value_diff = temp_long_market_value - float(today_db_balance.MarketValue)
    short_market_value_diff = float(today_db_balance.ShortMarketValue) - temp_short_market_value
    today_profit = float(today_db_balance.Profit) if today_db_balance.Date.strftime(
        common.LT["day"]) == datetime.datetime.today().strftime(common.LT["day"]) else 0
    temp_profit = today_profit + (short_market_value_diff + long_market_value_diff) / float(
        today_db_balance.InitialFunds) * 100
    net_profit = 0
    for balance in history_balance:
        net_profit = net_profit + float(balance.Profit)
    net_profit = (net_profit + temp_profit + 100) / 100

    stock_0 = {"Code": "SK总计", "DaySumProfit": 0, "MarketValue": 0, "PositionProfit": 0}
    future_0 = {"Code": "FU总计", "DaySumProfit": 0, "MarketValue": 0, "PositionProfit": 0}
    stock_list = []
    future_list = []
    for profit_key in profit_dict:
        p = profit_dict[profit_key].to_dict()
        if "SK" in p["Code"]:
            stock_list.append(p)
            stock_0["DaySumProfit"] += p["DaySumProfit"]
            stock_0["MarketValue"] += p["MarketValue"]
            stock_0["PositionProfit"] += p["PositionProfit"]
        else:
            future_list.append(p)
            future_0["DaySumProfit"] += p["DaySumProfit"]
            future_0["MarketValue"] += p["MarketValue"]
            future_0["PositionProfit"] += p["PositionProfit"]
    stock_0["DaySumProfit"] = round(stock_0["DaySumProfit"], 2)
    stock_0["MarketValue"] = round(stock_0["MarketValue"], 2)
    stock_0["PositionProfit"] = round(stock_0["PositionProfit"], 2)
    future_0["PositionProfit"] = round(future_0["PositionProfit"], 2)
    future_0["PositionProfit"] = round(future_0["PositionProfit"], 2)
    future_0["PositionProfit"] = round(future_0["PositionProfit"], 2)
    stock_list = [stock_0] + stock_list
    future_list = [future_0] + future_list
    print(today_db_balance.InitialFunds)
    result = {
        "Time": last_position_day.strftime("%Y-%m-%d"),
        "NetProfit": round(net_profit, 4),
        "NetProfitChange": round(temp_profit, 3),
        "StockMarketValue": round(temp_long_market_value, 2),
        "FutureMarketValue": round(temp_short_market_value, 2),
        "StockMarketValueChange": round(long_market_value_change, 2),
        "FutureMarketValueChange": round(short_market_value_change, 2),
        "ProfitInfo": [stock_list, future_list],
        "allMoney": round(float(today_db_balance.InitialFunds) * net_profit, 4),
        "accrualMoney": round(accrual_money, 2)
    }
    return result


@main.route('/bf/product/realTimeProfitTest', methods=['GET', 'POST'])
@filter(role_type)
def get_real_time_profit_fun_test():
    fund_id = int(request.args.get("id"))

    result = count_real_time_profit(fund_id)
    return {"code": 1, "data": result}


@main.route('/bf/product/realTimeProfit', methods=['GET', 'POST'])
@filter(role_type)
def get_real_time_profit():
    if request.args.get("id") is None:
        return {"code": 0, "mag": "Id不存在"}
    fund_id = int(request.args.get("id"))
    cache_key = "realTimeProfit-" + request.args.get("id")
    try:
        cache_value = cache.get(cache_key)
    except Exception as e:
        print(e)
        cache_value = None
    if cache_value:
        return {"code": 1, "data": json.loads(cache_value.decode('utf-8'))}
    result = count_real_time_profit(fund_id)
    cache.set(cache_key, json.dumps(result))
    print(result)
    return {"code": 1, "data": result}


@main.route('/bf/product/refresh', methods=['GET', 'POST'])
@filter(role_type)
def refresh_product():
    ids = [f.Id for f in Fund.query.filter_by(FundType=0).all()]
    for i in ids:
        result = count_real_time_profit(i)
        cache_key = "realTimeProfit-" + str(i)
        cache.set(cache_key, json.dumps(result))
    return {
        "code": 1,
        "data": "请刷新页面",
    }


dict_fund_type = {
    "Real": 0,
    "Test": 1
}


def create_fund(fund_name, base_code, initial_funds, create_date, fund_type, base_fund):
    product = Fund(FundName=fund_name, BaseCode=base_code, InitialFunds=initial_funds, CreateDate=create_date,
                   FundType=fund_type, BaseFund=base_fund)
    db.session.add(product)
    db.session.commit()
    accounts = []
    stock_list = request.args.get('StockList').split(',')
    for stock in stock_list:
        account_number = stock if fund_type == dict_fund_type["Real"] else "{0}_test".format(stock)
        accounts.append(Account(Type='1', AccountNumber=account_number))
    future_list = request.args.get('FutureList').split(',')
    for future in future_list:
        account_number = future if fund_type == dict_fund_type["Real"] else "{0}_test".format(future)
        accounts.append(Account(Type='0', AccountNumber=account_number))
    db.session.add_all(accounts)
    db.session.commit()
    fund_account_relations = []
    for account in accounts:
        fund_account_relations.append(FundAccountRelation(AccountId=account.Id, FundId=product.Id))
    db.session.add_all(fund_account_relations)
    db.session.commit()
    return product


@main.route('/bf/product/add', methods=['GET', 'POST'])
@filter("edit")
def set_product_add():
    fund_name = request.args.get('FundName')
    base_code = request.args.get('BaseCode')
    initial_funds = request.args.get('InitialFunds')
    create_date = request.args.get('CreateDate')
    funds = Fund.query.filter(Fund.FundName == fund_name).all()
    fund_list = to_list(funds)
    if len(fund_list) < 1:
        loop_test_fund = create_fund(fund_name, base_code, initial_funds, create_date, 1, None)
        real_fund = create_fund(fund_name, base_code, initial_funds, create_date, 0, loop_test_fund.Id)

        solution = request.args.get("solution")
        exposure = request.args.get("exposure")
        expect = request.args.get("expect")
        loop_test = FundLooptestInfo(FundId=real_fund.Id, FundSolution=solution, FundExposure=exposure, Expect=expect,
                                     Date=datetime.datetime.today())
        db.session.add(loop_test)
        db.session.commit()
        return {"code": 1, "msg": "新建产品成功！", "FundId": real_fund.Id}
    else:
        return {"code": 0, "msg": "产品名已存在！", "FundId": 'NULL'}


@main.route('/bf/product/remark', methods=['GET', 'POST'])
@filter(role_type)
def set_product_remark():
    fund_id = request.args.get('id')
    remark = FundRemark.query.filter(FundRemark.FundId == fund_id).order_by(FundRemark.Date.desc()).first()
    remark_list = to_list(remark)
    return {
        "code": 1,
        "data": remark_list,
    }


@main.route('/bf/product/looptest_info', methods=['GET', 'POST'])
@filter(role_type)
def get_product_loop_test_info():
    fund_id = request.args.get('id')
    info = FundLooptestInfo.query.filter(FundLooptestInfo.FundId == fund_id).order_by(
        FundLooptestInfo.Date.desc()).first()
    info_list = to_list(info)
    return {
        "code": 1,
        "data": info_list,
    }


@main.route('/bf/product/add_loop_test_solution', methods=['GET', 'POST'])
@filter("edit")
def add_product_loop_test():
    dict_data = request.json
    solution = dict_data["solution"]
    exposure = dict_data["exposure"]
    expect = dict_data["expect"]
    fund_id = dict_data["id"]
    loop_test = FundLooptestInfo(FundId=fund_id, FundSolution=solution, FundExposure=exposure, Expect=expect,
                                 Date=datetime.datetime.today())
    db.session.add(loop_test)
    db.session.commit()
    if loop_test.Id > 0:
        return {
            "code": 1,
            "data": "新增回测方案成功",
        }
    else:
        return {
            "code": 0,
            "msg": "新增回测方案失败",
        }


@main.route('/bf/product/position', methods=['GET', 'POST'])
@filter(role_type)
def get_product_last_position():
    fund_id = request.args.get('id')
    positions_sk = []
    positions_fu = []
    market_value_sk = 0
    market_value_fu = 0
    redis = False
    position_id = 3
    profit_by_day_sk_all = 0
    profit_by_day_fu_all = 0
    cost_price_sk = 0
    cost_price_fu = 0
    new_price_fu = 0
    new_price_sk = 0
    # 不与redis通信时，显示数据库的持仓
    if redis is False:
        m1_close_dic = dict()
        position_max_day = db.session.query(Position.Date).filter(Position.FundId == fund_id).order_by(
            Position.Date.desc()).limit(1).first()[0]
        # position_max_day = Position.query.filter(Position.FundId == fund_id).order_by(Position.Date.desc()).first()
        if position_max_day is not None:
            # 获取所有的持仓
            position_max_day = position_max_day.strftime(common.MODEL_FORMAT)
            position_list = Position.query.filter(Position.FundId == fund_id,
                                                  Position.Date == position_max_day).order_by(Position.Code).all()
            position_list = to_list(position_list)
            # 获取当前1MIN的股票Close
            stock_day_m1_list = StockDayM1.query.all()
            # 将股票名和close一一对应
            for stock_day_m1 in stock_day_m1_list:
                m1_close_dic[stock_day_m1.Code] = float(stock_day_m1.Close)

            # 组合持仓信息
            for position in position_list:
                code = position['Code']
                # product = code.split('.')[0]
                # exchange = code.split('.')[1]
                product, exchange = code.split('.')
                vol = position['Vol']
                price = position['Price']
                close_now = -9999
                market_value_open = vol * price
                market_value_now = -9999
                profit_by_day_1 = -9999
                profit_by_day_2 = -9999
                if exchange == 'SK':
                    # 持仓为股票时
                    if code in m1_close_dic:
                        close_now = m1_close_dic[code]
                        market_value_now = vol * close_now
                        profit_by_day_1 = round((close_now - price) * vol, 2)
                        profit_by_day_2 = round((close_now - price) / price * 100, 3)
                        market_value_sk = market_value_sk + market_value_now
                        profit_by_day_sk_all += profit_by_day_1
                        cost_price_sk += price * vol
                        new_price_sk += close_now * vol
                else:
                    # 持仓为期货时
                    future_tick_dic = FutureTick.query.with_entities(FutureTick.Price).filter(
                        FutureTick.InstrumentID == product).first()
                    if future_tick_dic is None:
                        continue
                    else:
                        close_now = float(future_tick_dic[0])
                        market_value_open = market_value_open * 200
                        market_value_now = vol * close_now * 200
                        profit_by_day_1 = round((close_now - price) * vol * 200, 2)
                        profit_by_day_2 = round((close_now - price) / price * 100, 3)
                        market_value_fu = market_value_fu + market_value_now
                        profit_by_day_fu_all += profit_by_day_1
                        cost_price_fu += price * vol
                        new_price_fu += close_now * vol
                # 组合为一条持仓
                position_code = {
                    'id': position_id,
                    'Code': code,
                    # 'Date': position['Date'],
                    'Direction': position['Direction'],
                    'OpenPrice': price,
                    'CloseNow': close_now,
                    'Vol': vol,
                    'MarketValueOpen': market_value_open,
                    'MarketValueNow': round(market_value_now, 3),
                    'ProfitByDay1': profit_by_day_1,
                    'ProfitByDay2': profit_by_day_2,
                }
                if exchange == 'SK':
                    positions_sk.append(position_code)
                else:
                    positions_fu.append(position_code)
                position_id = position_id + 1
    positions_sk_title = {
        'id': 1,
        'Code': "SK",
        'Date': "",
        'OpenPrice': "",
        'CloseNow': "",
        'Vol': "",
        'MarketValueOpen': '',
        'MarketValueNow': round(market_value_sk, 3),
        'ProfitByDay1': round(profit_by_day_sk_all, 3),
        'ProfitByDay2': round((new_price_sk - cost_price_sk) / cost_price_sk * 100, 2) if cost_price_sk > 0 else 0,
        'children': positions_sk,
    }
    positions_fu_title = {
        'id': 2,
        'Code': "FU",
        'Date': "",
        'OpenPrice': "",
        'CloseNow': "",
        'Vol': "",
        'MarketValueOpen': '',
        'MarketValueNow': round(market_value_fu, 3),
        'ProfitByDay1': round(profit_by_day_fu_all, 3),
        'ProfitByDay2': round((new_price_fu - cost_price_fu) / cost_price_fu * 100, 2) if cost_price_fu > 0 else 0,
        'children': positions_fu,
    }
    positions = [positions_sk_title, positions_fu_title]
    return {
        "code": 1,
        "data": positions,
    }


@main.route('/bf/product/loop_test_upload', methods=['GET', 'POST'])
def loop_test_upload():
    if request.method == 'POST':
        file = request.files['file']
        id = request.form.get("id")
        if file and common.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            file.save(f)
            df = pd.read_csv(f, encoding='gbk')
            df = df.loc[0:, ["dates", "yList"]]
            df.columns = ["Date", "Profit"]
            df["Profit"] = df["Profit"].apply(lambda x: (x - 1) * 100)
            for index, row in df.iterrows():
                d = LooptestBalanceStrategy.query.filter_by(Date=row["Date"], LoopTestId=id).first()
                if d is None:
                    db.session.add(
                        LooptestBalanceStrategy(Date=row["Date"], Profit=row["Profit"], LoopTestId=id,
                                                RefreshDate=datetime.datetime.now()))
                else:
                    d.Profit = row["Profit"]
                    d.RefreshDate = datetime.datetime.now()
            db.session.commit()
            return jsonify({"code": 20000, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})
    return jsonify({"code": 0, "msg": "请求失败"})


@main.route('/bf/product/delete', methods=['GET', 'POST'])
@filter("edit")
def loop_test_delete():
    list_loop_test_id = request.json["ids"]
    list_result = []
    for loop_test_id in list_loop_test_id:
        try:
            db.session.query(LooptestBalanceStrategy).filter(
                LooptestBalanceStrategy.LoopTestId == loop_test_id).delete()
            db.session.query(FundLooptestInfo).filter(FundLooptestInfo.Id == loop_test_id).delete()
            db.session.commit()
        except Exception as e:
            list_result.append("id:{0}删除失败,失败原因".format(loop_test_id))
        else:
            list_result.append("id:{0}删除成功".format(loop_test_id))

    return {
        "code": 1,
        "data": "删除成功",
    }


@main.route('/bf/product/drawdownChart', methods=['GET', 'POST'])
@filter(role_type)
def product_test_draw_down_chart():
    list_loop_test_id = request.json["ids"]
    dfs = []
    columns = []
    for loop_test_id in list_loop_test_id:
        column = "id-" + str(loop_test_id)
        columns.append(column)

        sql_exec = str(
            LooptestBalanceStrategy.query.filter(
                LooptestBalanceStrategy.LoopTestId == loop_test_id).order_by(
                LooptestBalanceStrategy.Date).statement.compile(
                compile_kwargs={"literal_binds": True})).replace('"', "")
        df = pd.read_sql(sql_exec, con=db.engine)
        if df.shape[0] == 0:
            continue
        # df = pd.DataFrame(to_list(LooptestBalanceStrategy.query.filter(
        #     LooptestBalanceStrategy.LoopTestId == loop_test_id).order_by(LooptestBalanceStrategy.Date).all()))
        df = df.loc[:, ["Date", "Profit"]]
        list_day_draw_down = index_calculator.get_day_draw_downs(list(df["Date"]), list(df["Profit"]))
        df["draw_down"] = pd.DataFrame(list_day_draw_down, columns=["draw_down"])["draw_down"]
        df.columns = ["Date", "Profit", column]
        df["Date"] = df["Date"].apply(
            lambda x: datetime.datetime.strptime(x, common.MODEL_FORMAT).strftime(common.LT["day"]))
        df.set_index("Date", inplace=True)
        dfs.append(df)
    if len(dfs) == 0:
        return {"code": 0,
                "msg": "数据未上传"}
    dfs = pd.concat(dfs, axis=1, join='outer')
    dict_result = dict()
    dict_result["x"] = list(dfs.index)
    dict_result["y"] = dict()
    targets = dict()
    for c in columns:
        dict_result["y"][c] = list(dfs[c])
        list_datetime = [datetime.datetime.strptime(d, common.LT["day"]) for d in dict_result["x"]]
        commons = index_calculator.get_draw_down_index_target(list_datetime, dict_result["y"][c])
        for com in commons:
            if com[0] not in targets:
                targets[com[0]] = {"name": com[0]}
            targets[com[0]][c] = com[1]
        dict_result["targets"] = list(targets.values())
    return {
        "code": 1,
        "data": dict_result,
    }


@main.route('/bf/product/deleteProApi', methods=['GET', 'POST'])
@filter("edit")
def delete_pro_api():
    id = request.args.get("id")
    product = Fund.query.filter_by(Id=id).first()
    if not product:
        return {"code": 0, "msg": "数据不存在"}
    # 开启事务
    # db.session.begin(subtransactions=True)
    products = [product.Id]
    if product.BaseFund is not None:
        products.append(product.BaseFund)

    Balance.query.filter(Balance.FundId.in_(products)).delete(synchronize_session=False)
    Position.query.filter(Position.FundId.in_(products)).delete(synchronize_session=False)
    FundRemark.query.filter(FundRemark.FundId.in_(products)).delete(synchronize_session=False)

    # account
    fars = FundAccountRelation.query.filter(FundAccountRelation.FundId.in_(products)).all()
    accountIds = [a.AccountId for a in fars]
    Tradeflow.query.filter(Tradeflow.AccountId.in_(accountIds)).delete(synchronize_session=False)
    FundAccountRelation.query.filter(FundAccountRelation.FundId.in_(products)).delete(synchronize_session=False)
    Account.query.filter(Account.Id.in_(accountIds)).delete(synchronize_session=False)

    ltbs = LooptestBalance.query.filter(LooptestBalance.FundId.in_(products)).all()
    ltbs_ids = [l.Id for l in ltbs]
    LooptestBalanceStrategy.query.filter(LooptestBalanceStrategy.LoopTestId.in_(ltbs_ids)).delete(
        synchronize_session=False)
    LooptestBalance.query.filter(LooptestBalance.FundId.in_(products)).delete(synchronize_session=False)

    FundLooptestInfo.query.filter_by(FundId=product.Id).delete(synchronize_session=False)
    Fund.query.filter_by(Id=id).delete(synchronize_session=False)
    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return {"code": 0, "msg": "删除失败"}
    return {"code": 1, "msg": "删除成功"}


@main.route('/bf/product/updatePro', methods=['GET', 'POST'])
@filter("edit")
def update_pro():
    req = request.json
    fund = FundLooptestInfo.query.get(req["Id"])
    fund.Expect = req["Expect"]
    fund.FundExposure = req["FundExposure"]
    fund.FundSolution = req["FundSolution"]
    db.session.add(fund)
    db.session.commit()
    return {
        "code": 1
    }


@main.route('/bf/product/pb_upload', methods=['GET', 'POST'])
def init_position_upload():
    if request.method == 'POST':
        balance_file = request.files['balance']
        position_file = request.files['position']
        id = request.form.get("id")
        fund = Fund.query.filter(Fund.Id == id).first()
        if balance_file and common.allowed_file(balance_file.filename) and position_file and common.allowed_file(
                position_file.filename):
            # db.session.begin(subtransactions=True)

            filename = secure_filename(balance_file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            balance_file.save(f)
            df = pd.read_csv(f, encoding='utf8', header=0, names=["Date", "MarketValue", "Profit", "InitialFunds",
                                                                  "Cash", "ShortMarketValue", "Base"])
            df = df.drop(columns=["Base", ])
            first_profit = df["Profit"][0]
            df["FundId"] = df["Date"].apply(lambda x: fund.Id)
            df["Profit"] = df["Profit"].diff()
            df["Profit"][0] = first_profit
            df.fillna(0, inplace=True)
            df.to_sql(name="balance", con=db.engine, index=False, if_exists='append')
            df["FundId"] = df["Date"].apply(lambda x: fund.BaseFund)
            df.to_sql(name="balance", con=db.engine, index=False, if_exists='append')

            filename = secure_filename(position_file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            position_file.save(f)
            df = pd.read_csv(f, encoding='gbk', header=0, names=["Code", "Vol", "Direction", "Date", "Price"])
            df["FundId"] = df["Code"].apply(lambda x: fund.Id)
            df["Code"] = df["Code"].apply(lambda x: x.replace("SH", "SK").replace("SZ", "SK"))
            df["Code"] = df["Code"].apply(lambda x: x if x.endswith('.SK') else x.replace(x.split('.')[1], "FU"))
            df.to_sql(name="position", con=db.engine, index=False, if_exists='append')
            df["FundId"] = df["Code"].apply(lambda x: fund.BaseFund)
            df.to_sql(name="position", con=db.engine, index=False, if_exists='append')
            try:
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                return jsonify({"code": 0, "msg": "删除失败"})
            return jsonify({"code": 20000, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})
    return jsonify({"code": 0, "msg": "请求失败"})


@main.route('/bf/product/tradeFlow_upload', methods=['GET', 'POST'])
def upload_trade_flow():
    if request.method == 'POST':
        file = request.files['file']
        fund_id = int(request.form.get("id"))
        if file and common.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            if not os.path.exists(common.UPLOAD_FOLDER):
                os.makedirs(common.UPLOAD_FOLDER)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            file.save(f)
            dict_type = {"买入": 0, "卖出": 1}
            df = pd.read_csv(f, header=0, encoding='gbk',
                             names=["Code", "Vol", "TradeDate", "Price", "InstrumentType", "Type", "Direction",
                                    "AccountId"])
            df["Type"] = df["Type"].apply(lambda x: dict_type[x])
            df.drop(columns=["InstrumentType", ], inplace=True)
            df["TradeDate_Format"] = df["TradeDate"].apply(lambda x: datetime.datetime.strptime(x, "%Y/%m/%d %H:%M:%S"))
            df["TradeDate"] = df["TradeDate"].apply(
                lambda x: datetime.datetime.strptime(x[:10], "%Y/%m/%d"))
            df["Code"] = df["Code"].apply(lambda x: x.replace("SH", "SK").replace("SZ", "SK"))
            account_numbers = set(df["AccountId"])
            account_dict = dict()
            for account_number in account_numbers:
                account = Account.query.filter(Account.AccountNumber == str(account_number)).first()
                account_dict[account_number] = account.Id
                if fund_id < 0:
                    relation = FundAccountRelation.query.filter(FundAccountRelation.AccountId == account.Id).first()
                    fund_id = relation.FundId
            df["AccountId"] = df["AccountId"].apply(lambda x: account_dict[x])
            if fund_id > 0:
                profit_calculator.deal_trade_flow(fund_id, df)
                return jsonify({"code": 20000, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})
    return jsonify({"code": 0, "msg": "请求失败"})


@main.route('/bf/product/computeToday', methods=['GET', 'POST'])
def compute_today():
    fund_id = int(request.args.get("id"))
    if fund_id > 0:
        fund = Fund.query.filter(Fund.Id == fund_id).first()
        if fund is not None:
            profit_calculator.calculator_today(fund.Id)
            if fund.BaseFund is not None:
                profit_calculator.calculator_today(fund.BaseFund)
    else:
        funds = Fund.query.filter(Fund.FundType == dict_fund_type["Real"]).all()
        for fund in funds:
            profit_calculator.calculator_today(fund.Id)
            if fund.BaseFund is not None:
                profit_calculator.calculator_today(fund.BaseFund)
    return jsonify({"code": 1, "data": "success"})


@main.route('/bf/product/clear_fund_info', methods=['GET', 'POST'])
@filter("edit")
def clear_fund_info():
    fund_id = request.args.get("id")
    fund = Fund.query.filter(Fund.Id == fund_id).first()
    if not fund:
        return {"code": 0, "msg": "fund不存在"}
    relations = FundAccountRelation.query.join(Account, Account.Id == FundAccountRelation.AccountId).filter(
        or_(FundAccountRelation.FundId == fund_id, FundAccountRelation.FundId == fund.BaseFund)).all()
    account_id_list = [relation.AccountId for relation in relations]
    # db.session.begin(subtransactions=True)
    Tradeflow.query.filter(Tradeflow.AccountId.in_(account_id_list)).delete(synchronize_session=False)
    Position.query.filter(or_(Position.FundId == fund_id, Position.FundId == fund.BaseFund)).delete()
    Balance.query.filter(or_(Balance.FundId == fund_id, Balance.FundId == fund.BaseFund)).delete()
    LooptestBalance.query.filter(LooptestBalance.FundId == fund_id).delete()
    db.session.commit()
    return {"code": 20000, "data": "success"}


DAY_FORMAT = '%Y-%m-%d'


@main.route('/bf/product/getFixTime', methods=['GET', 'POST'])
@filter(role_type)
def get_fix_time():
    fund_id = request.args.get("FundId")
    relations = db.session.query(FundAccountRelation.AccountId).join(Account,
                                                                     Account.Id == FundAccountRelation.AccountId).filter(
        FundAccountRelation.FundId == fund_id).all()
    relations = [i[0] for i in relations]
    sql_res = db.session.query(Tradeflow.TradeDate).filter(Tradeflow.AccountId.in_(relations)).group_by(
        Tradeflow.TradeDate).order_by(
        Tradeflow.TradeDate.desc()).all()
    datalist = list(set([r[0].strftime(DAY_FORMAT) for r in sql_res]))
    datalist.sort()
    datalist.reverse()
    return {"code": 1, "data": [{"TradeDate": d} for d in datalist]}


@main.route('/bf/product/getFixTableData', methods=['GET', 'POST'])
@filter(role_type)
def get_fix_table_data():
    update_day = request.args.get("TradeDate")
    if not update_day:
        return {"code": 0, "msg": "无数据"}
    fund_id = request.args.get("FundId")
    relations = FundAccountRelation.query.join(Account, Account.Id == FundAccountRelation.AccountId).filter(
        FundAccountRelation.FundId == fund_id).all()
    if not relations:
        return {"code": 0, "msg": "无数据"}
    account_id_list = [relation.AccountId for relation in relations]
    trade_flow_list = Tradeflow.query.filter(
        and_(Tradeflow.AccountId.in_(account_id_list), Tradeflow.TradeDate.like(update_day + "%"))).all()
    return {"code": 1, "data": to_list(trade_flow_list)}


@main.route('/bf/product/saveFixData', methods=['POST'])
# @filter(role_type)
def save_fix_data():
    req = request.json
    print(req)
    tf = Tradeflow(**req["row"])
    fund_id = int(req["FundId"])
    if fund_id < 0:
        account_id = Account.query.filter(Account.AccountNumber == tf.AccountId).first().Id
        tf.AccountId = account_id
        fund_id = FundAccountRelation.query.filter(FundAccountRelation.AccountId == account_id).first().FundId
    profit_calculator.update_one_trade_flow(fund_id, tf, req["type"])
    return jsonify({"code": 1, "data": "success"})


@main.route('/bf/product/saveFixAccrualData', methods=['POST'])
# @filter(role_type)
def save_fix_accrual_data():
    req = request.json
    print(req)
    af = AccrualFlow(**req["row"])
    fund_id = int(af.FundId)
    profit_calculator.update_one_accrual_flow(fund_id, af, req["type"])
    return jsonify({"code": 1, "data": "success"})


# @main.route('/bf/product/create_table', methods=['POST'])
def create_table():
    db.create_all()
    return jsonify({"code": 1, "data": "success"})


@main.route('/bf/product/getAccrualData', methods=['GET'])
@filter(role_type)
def get_accrual_data():
    foud_id = request.args.get("foud_id")
    res = to_list(AccrualFlow.query.filter_by(FundId=foud_id).all())
    return res


@main.route('/bf/product/upload', methods=['GET', 'POST'])
def product_upload():
    if request.method == 'POST':
        file = request.files['file']
        need_count = request.form.get("count")
        id = request.form.get("id")
        if file and common.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            file.save(f)
            df = pd.read_csv(f, encoding='gbk')
            df = df.loc[0:, ["dates", "yList"]]
            df.columns = ["Date", "Profit"]
            df["Profit"] = df["Profit"].apply(lambda x: (x - 1) * 100)
            for index, row in df.iterrows():
                d = LooptestBalance.query.filter_by(Date=row["Date"], FundId=id).first()
                if d is None:
                    db.session.add(
                        LooptestBalance(Date=row["Date"], Profit=row["Profit"], FundId=id,
                                        RefreshDate=datetime.datetime.now()))
                else:
                    d.Profit = row["Profit"]
                    d.RefreshDate = datetime.datetime.now()
            db.session.commit()
            # 是否要计算
            if need_count:
                df = pd.read_sql('select * from looptest_balance where FundId=%s' % id, con=db.engine).sort_index(
                    by=["Date"])
                d_time = list(df["Date"])
                profit = list(df["Profit"])
                sharp = float(index_calculator.calculate_sharp(d_time, profit))
                profit_rate = float(index_calculator.calculate_year_profit_rate(d_time, profit))
                max_draw_down = float(index_calculator.calculate_max_draw_down(d_time, profit))
                text = "%s:%.2f\n%s:%.2f\n%s:%.2f\n" % ("夏普值", sharp, "年化收益%", profit_rate, "最大回撤%", max_draw_down)
                tlt = FundLooptestInfo.query.filter_by(Id=id).first()
                tlt.Expect = text
                db.session.commit()
            return jsonify({"code": 20000, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})
    return jsonify({"code": 0, "msg": "请求失败"})


@main.route('/bf/product/getAccountId', methods=['GET', 'POST'])
@filter(role_type)
def get_account_id():
    id = request.args.get("id")
    if id is None:
        return {"code": 0, "msg": "Id不存在"}
    sql_res = db.session.execute(
        "SELECT AccountNumber from fund_account_relation as far inner join account as a on "
        "a.Id=far.AccountId and a.Type=1 and far.FundId=%s;" % id).fetchone()[0]
    return {"code": 1, "data": sql_res}


# 从sql中抽取数据用于计算
def get_tradeflow_df(accounts, last_date, future_date):
    sql = 'select * from tradeflow where AccountId in (%d,%d) and TradeDate > "%s" and TradeDate < "%s"' % (
        accounts[0].Id, accounts[1].Id, last_date, future_date)
    df = pd.read_sql(sql, con=db.engine).drop(columns=["RefDate", "AccountId", "Id", "TradeDate"],
                                              inplace=False)
    if df.shape[0] == 0:
        raise Exception("数据未上传")

    df["value"] = df["Price"] * df["Vol"]
    df_group = pd.DataFrame(
        [{"Code": name[0], "Type": -1 if name[1] else 1, "Vol": group["Vol"].sum(),
          "Price": (group["value"].sum() / group["Vol"].sum()).round(2),
          "TypeStr": ('平' if name[1] else '开') + ('多' if name[2] else '空')} for name, group in
         df.groupby(by=["Code", "Type", "Direction"])])
    return df_group


@main.route('/bf/product/checkDate', methods=['GET', 'POST'])
@filter(role_type)
def check_date():
    # 从数据库中查出数据
    fund_real_id = request.args.get("fundId")
    fund_test_id = Fund.query.get(fund_real_id).BaseFund

    date = request.args.get("date")
    last_date, future_date = common.get_time_section(date)
    account_real = Account.query.join(FundAccountRelation, and_(FundAccountRelation.FundId == fund_real_id,
                                                                FundAccountRelation.AccountId == Account.Id)).all()
    account_test = Account.query.join(FundAccountRelation, and_(FundAccountRelation.FundId == fund_test_id,
                                                                FundAccountRelation.AccountId == Account.Id)).all()
    try:
        real_df = get_tradeflow_df(account_real, last_date, future_date)
        test_df = get_tradeflow_df(account_test, last_date, future_date)
    except Exception as e:
        return {"code": 0, "msg": str(e)}
    res_df = pd.merge(real_df, test_df, on=["Code", "Vol", "Type", "TypeStr"], how='outer', suffixes=('_real', '_test'))
    # 滑点
    res_df["SlipPage"] = res_df["Type"] * (res_df["Price_real"] - res_df["Price_test"])
    # 滑点百分比
    res_df["SlippageP"] = ((res_df["SlipPage"] / res_df["Price_real"]) * 1000).round(5)
    # 价差
    res_df["Spread"] = res_df["SlipPage"] * res_df["Vol"]
    res_df["absSlippageP"] = res_df["SlippageP"].abs()
    res_df.sort_values(by='absSlippageP', inplace=True, ascending=False)
    # 算出平均值
    slip_page_ave = round(res_df["SlipPage"].mean(), 2)
    slip_page_p_ave = round(res_df["SlippageP"].mean(), 2)
    spread_ave = round(res_df["Spread"].mean(), 2)

    res_json = [{"Code": "平均值", "SlipPage": slip_page_ave, "SlippageP": slip_page_p_ave, "Spread": spread_ave}] + list(
        json.loads(res_df.to_json(orient='index')).values())
    res_log = to_list(
        FundRemark.query.filter(FundRemark.FundId == fund_real_id, FundRemark.Date > last_date,
                                FundRemark.Date < future_date).order_by(FundRemark.Date.desc()).all())

    return {"code": 1, "data": {"stockData": res_json},
            "logData": res_log}


def date_sort_by_type(sql, time_type):
    real_df = pd.read_sql(sql, con=db.engine)
    real_df["trans_date"] = real_df["Date"].apply(
        lambda x: x.strftime(common.LT[time_type]))
    result = []
    for name, group in real_df.groupby(by=["trans_date"]):
        result.append({"Date": name, "Profit": round(group["Profit"].sum(), 4)})
    return result


def sort_char_bar_res(parms):
    res = {}
    for p in parms:
        if parms[p] is None:
            continue
        res[p] = pd.DataFrame(parms[p]).set_index('Date').rename(columns={"Profit": p})
    res_df = pd.concat(list(res.values()), axis=1, join="outer")
    res_df = res_df.fillna(0)
    # res_df.sort_values(by='Date',inplace=True)
    # res_df.reset_index(inplace=True)
    res_json = {}
    res_json["x"] = list(res_df.index)
    res_json["y"] = {k: list(res_df[k]) for k in parms.keys()}
    return res_json


@main.route('/bf/product/chartBar', methods=['GET', 'POST'])
@filter(role_type)
def get_char_bar():
    fund_id = request.args.get("fundId")
    time_type = request.args.get("timeType")
    fund = Fund.query.filter(Fund.Id == fund_id).first()
    fsr = FundStrategyRelation.query.filter_by(FundId=fund_id).first()
    if fund is None or time_type not in common.LT:
        return {
            "code": 0,
            "msg": "参数错误"
        }
    # 真实流水
    sql_real = "select Date,Profit from balance where FundId=%s" % fund_id
    res_real = date_sort_by_type(sql_real, time_type)
    # 冲击流水
    sql_loop = "select Date,Profit from balance where FundId=%s" % fund.BaseFund
    res_loop = date_sort_by_type(sql_loop, time_type)
    res_test = None
    # 回测流水
    if fsr is not None:
        sql_test = "select Date,Profit from strategy_balance where StrategyId=%s" % fsr.StrategyId
        res_test = date_sort_by_type(sql_test, time_type)
    return {"code": 1, "data": sort_char_bar_res({"产品": res_real, "冲击": res_loop, "回测": res_test})}


@main.route('/bf/product/stockDiff', methods=['GET', 'POST'])
@filter(role_type)
def stock_diff():
    fund_name = request.args.get("name")
    funds = Fund.query.filter_by(FundName=fund_name).all()
    test_data = None
    real_data = None
    for fund in funds:
        if fund.FundType:
            test_data = get_tradeflow_by_fund(fund)
        else:
            real_data = get_tradeflow_by_fund(fund)
    if test_data is None or real_data is None:
        return {
            "code": 0,
            "msg": "个股贡献表，数据不存在"
        }
    x = list(set(list(test_data.keys()) + list(real_data.keys())))
    test_list = []
    real_list = []
    for i in x:
        if i in test_data:
            test_list.append(round(test_data[i], 5))
        else:
            test_list.append(0)

        if i in real_data:
            real_list.append(round(real_data[i], 5))
        else:
            real_list.append(0)
    result = {"x": x, "y": {"回测": test_list, "真实": real_list}}
    # print(result)
    return {
        "code": 1,
        "data": result
    }


def get_tradeflow_by_fund(fund):
    stock_pmap = {}
    sql_res = db.session.execute(
        "SELECT a.Id from fund_account_relation as far inner join account as a on "
        "a.Id=far.AccountId and a.Type=1 and far.FundId=%s;" % fund.Id).fetchone()[0]
    # 时间限制
    last_month = (datetime.datetime.today() - relativedelta(months=12)).strftime("%Y-%m-%d %H:%M:%S")
    # sql_exec = str(
    #     Tradeflow.query.filter(Tradeflow.AccountId == sql_res, Tradeflow.TradeDate > last_month).statement.compile(
    #         compile_kwargs={"literal_binds": True})).replace('"', "")
    sql_exec = str(
        Tradeflow.query.filter(Tradeflow.AccountId == sql_res).statement.compile(
            compile_kwargs={"literal_binds": True})).replace('"', "")
    df = pd.read_sql(
        sql_exec,
        con=db.engine)
    df.drop(columns=["Id", "AccountId"], inplace=True)
    df["Profit"] = df["Price"] * df["Vol"] * df["Type"].apply(lambda x: x if x == 1 else -1)
    for name, stock_p in df.groupby(by=["Code"]):
        stock_p["Vol2"] = df["Vol"] * df["Type"].apply(lambda x: 1 if x == 1 else -1)
        if stock_p["Vol2"].sum() == 0:
            stock_pmap[name] = (stock_p["Price"] * stock_p["Vol2"]).sum()
        else:
            s = stock_p.groupby(by=["Type"])
            if 0 not in s.groups or 1 not in s.groups:
                continue
            s1 = s.get_group(0)
            s1s = s1["Vol"].sum()
            s2 = s.get_group(1)
            s2s = s2["Vol"].sum()
            if s1s > s2s:
                if not handle_stock_p(s1, s1s - s2s):
                    continue
            else:
                if not handle_stock_p(s2, s2s - s1s):
                    continue
            stock_pmap[name] = (s1["Price"] * s1["Vol"] * s1["Type"].apply(lambda x: x if x == 1 else -1)).sum() + \
                               (s2["Price"] * s2["Vol"] * s2["Type"].apply(lambda x: x if x == 1 else -1)).sum()
    return stock_pmap


def handle_stock_p(stock_p, yu):
    i = len(stock_p) - 1
    stock_p.reset_index(drop=True, inplace=True)
    while True:
        if stock_p.loc[i, "Vol"] - yu < 0:
            yu = yu - stock_p.loc[i, "Vol"]
            stock_p.loc[i, "Vol"] = 0
            i -= 1
            if i < 0:
                return False
        else:
            stock_p.loc[i, "Vol"] = stock_p.loc[i, "Vol"] - yu
            return True


@main.after_request
def releaseDB(response):
    db.session.close()
    return response


# 选定时间的拼凑
def get_data_m5_vwap_compose(x):
    vwapStartTime = request.json["vwapStartTime"]
    vwapEndTime = request.json["vwapEndTime"]

    code, the_date = x[["Code", "fromTime"]]
    sql_table = the_date.strftime("%Y_%m")
    day = the_date.strftime("%Y-%m-%d")
    start_select_time = "{day} {date}:00".format(day=day, date=vwapStartTime)
    end_select_time = "{day} {date}:00".format(day=day, date=vwapEndTime)
    select_redis_time = "{day}X{vwapStartTime}T{vwapEndTime}".format(day=day, vwapStartTime=vwapStartTime,
                                                                     vwapEndTime=vwapEndTime)
    cache_key = "m5data_{code}_{date}".format(code=code, date=select_redis_time)
    cache_value = cache.get(cache_key)
    if cache_value:
        return float(cache_value)
    sql = r"select * from `{sql_table}` where Date >= '{start_select_time}' and Date<'{end_select_time}' and Code = '{code}';".format(
        sql_table=sql_table, start_select_time=start_select_time, end_select_time=end_select_time, code=code)
    df = pd.read_sql(sql, con=db.get_engine(bind="m5"))
    if len(df) == 0:
        return np.nan
    vwap = df["OpenInterest"].sum() / df["Volume"].sum() / 100
    cache.set(cache_key, vwap)
    return vwap


# 5分钟的拼凑
def get_data_m5_vwap(x):
    # t1 = time.time()
    code, the_date = x[["Code", "fromTime"]]
    select_redis_time = the_date.strftime("%Y-%m-%dT%H:%M:%S")
    cache_key = "m5vwap_{code}_{date}".format(code=code, date=select_redis_time)
    cache_value = cache.get(cache_key)
    if cache_value:
        # t2 = time.time()
        # print("查询一条耗时:", t2 - t1)
        return float(cache_value)
    print("vwap", cache_key, "未命中")

    sql_table = the_date.strftime("%Y_%m")
    select_time = the_date.strftime("%Y-%m-%d %H:%M:%S")
    sql = r"select OpenInterest/Volume/100 from `{sql_table}` where Date = '{select_time}' and Code = '{code}';".format(
        sql_table=sql_table, select_time=select_time, code=code)
    vwaps = db.get_engine(bind="m5").execute(sql).fetchone()
    if vwaps and len(vwaps) > 0:
        vwap = float(vwaps[0])
        cache.set(cache_key, vwap)
        return vwap
    return np.nan


def get_data_m5_twap(x):
    # t1 = time.time()
    code, the_date = x[["Code", "fromTime"]]
    startTime = the_date.strftime("%Y%m%d%H%M%S")
    cache_key = "m5twap_{code}_{date}".format(code=code, date=startTime)
    cache_value = cache.get(cache_key)
    if cache_value:
        # t2 = time.time()
        # print("查询一条耗时:", t2 - t1)
        return float(cache_value)
    print("twap", cache_key, "未命中")

    endTime = (the_date + relativedelta(minutes=5)).strftime("%Y%m%d%H%M%S")
    sql = "select avg(Close) from m1_code where Code='{code}' and toYYYYMMDDhhmmss(Date)>{startTime} and toYYYYMMDDhhmmss(Date)<{endTime}".format(
        code=code, startTime=startTime, endTime=endTime)
    res = clickhouse_client.engine.execute(sql)
    if len(res) > 0:
        twap = res[0][0]
        cache.set(cache_key, twap)
        return twap
    return np.nan


def get_from_time(x):
    if x.hour == 15:
        x -= relativedelta(minutes=3)
    if x.hour == 9 and x.minute < 30:
        return datetime.datetime(x.year, x.month, x.day, 9, 30)
    return datetime.datetime(x.year, x.month, x.day, x.hour, x.minute // 5 * 5)


# 显示首页用户所有的产品
@main.route('/bf/product/vwap', methods=['GET', 'POST'])
# @filter(role_type)
@filter()
def get_product_vwap():
    response = {"DateTime": [], "vwapDiff": [], "twapDiff": [], "twapDiffMean": 0, "vwapDiffMean": 0}
    get_type = request.json["getType"]
    product_id = request.json["productId"]

    sql = "select account.AccountNumber from fund_account_relation,account where account.Type=1 and fund_account_relation.AccountId=account.Id and fund_account_relation.FundId={product_id};".format(
        product_id=product_id)
    accounts = db.session.execute(sql).fetchone()
    if len(accounts) == 0:
        raise Exception("账号异常")

    account = accounts[0]
    sql = "select *,date_format(TheDate,'%%H:%%i') as mintue from trade_trade where TradeTag like '%%{account}'".format(
        account=account)
    source_format = "%Y-%m-%dT%H:%M:%S.%fZ"
    target_format = "%Y-%m-%d %H:%M:%S"
    if "startTime" in request.json:
        start_time = datetime.datetime.strptime(request.json["startTime"], source_format).strftime(target_format)
        sql += "and TheDate >= '{start_time}'".format(start_time=start_time)
    if "endTime" in request.json:
        end_time = datetime.datetime.strptime(request.json["endTime"], source_format).strftime(target_format)
        sql += " and TheDate <= '{end_time}'".format(end_time=end_time)
    if get_type == "open":
        sql += " and Direction=0"
    elif get_type == "close":
        sql += " and Direction=1"
    else:
        # todo 开平仓
        pass

    if "vwapStartTime" in request.json or "vwapEndTime" in request.json:
        if request.json["vwapStartTime"] and request.json["vwapEndTime"]:
            sql = "select * from ({sql}) as mintue_trade where mintue>='{vwapStartTime}' and mintue<='{vwapEndTime}'".format(
                sql=sql,
                vwapStartTime=request.json["vwapStartTime"], vwapEndTime=request.json["vwapEndTime"])
    print(sql)
    t0 = time.time()
    df = pd.read_sql(sql, con=db.get_engine(bind="users"))
    if len(df) == 0:
        return response

    df["fromTime"] = df["TheDate"].apply(get_from_time)
    t1 = time.time()
    print("sql耗时:", t1 - t0, "数据大小", len(df))
    df["twap"] = df.apply(get_data_m5_twap, axis=1)
    t2 = time.time()
    print("Twap耗时:", t2 - t1)

    df["vwap"] = df.apply(get_data_m5_vwap, axis=1)
    t3 = time.time()
    print("Vwap耗时:", t3 - t2)

    df["DateTime"] = df["TheDate"].apply(lambda x: x.strftime("%Y-%m-%d"))
    df["TwapMoney"] = df["twap"] * df["Volume"]
    df["VwapMoney"] = df["vwap"] * df["Volume"]
    df = df[(~df["vwap"].isna()) & (~df["twap"].isna())]

    if get_type == "open":
        df["vwapDiff"] = (df["vwap"] - df["Price"]) * df["Volume"]
        df["twapDiff"] = (df["twap"] - df["Price"]) * df["Volume"]
    if get_type == "close":
        df["vwapDiff"] = (df["Price"] - df["vwap"]) * df["Volume"]
        df["twapDiff"] = (df["Price"] - df["twap"]) * df["Volume"]

    res_list = []
    for day, data in df.groupby(by="DateTime"):
        res_list.append({"DateTime": day,
                         "vwapDiff": data["vwapDiff"].sum() / data["VwapMoney"].sum() * 10000,
                         "twapDiff": data["twapDiff"].sum() / data["TwapMoney"].sum() * 10000})
    res_df = pd.DataFrame(res_list)
    if len(res_df) == 0:
        return response
    res_df.sort_values(by="DateTime", inplace=True)
    response["DateTime"] = list(res_df["DateTime"])
    response["vwapDiff"] = list(res_df["vwapDiff"])
    response["vwapDiffMean"] = round(res_df["vwapDiff"].mean(), 3)

    response["twapDiff"] = list(res_df["twapDiff"])
    response["twapDiffMean"] = round(res_df["twapDiff"].mean(), 3)
    t4 = time.time()
    print("最后耗时", t4 - t3)

    return response


# 显示卡方公式
@main.route('/bf/product/kafang_vwap', methods=['GET', 'POST'])
@filter()
def get_kafang_vwap():
    response = {"DateTime": [], "vwapDiff": [], "twapDiff": [], "twapDiffMean": 0, "vwapDiffMean": 0}
    # 开平仓将没有类型
    # get_type = request.json["getType"]
    product_id = request.json["productId"]

    sql = "select account.AccountNumber from fund_account_relation,account where account.Type=1 and fund_account_relation.AccountId=account.Id and fund_account_relation.FundId={product_id};".format(
        product_id=product_id)
    accounts = db.session.execute(sql).fetchone()
    if len(accounts) == 0:
        raise Exception("账号异常")

    account = accounts[0]
    sql = "select *,date_format(TheDate,'%%H:%%i') as mintue from trade_trade where TradeTag like '%%{account}'".format(
        account=account)

    source_format = "%Y-%m-%dT%H:%M:%S.%fZ"
    target_format = "%Y-%m-%d %H:%M:%S"
    if "startTime" in request.json:
        start_time = datetime.datetime.strptime(request.json["startTime"], source_format).strftime(target_format)
        sql += "and TheDate >= '{start_time}'".format(start_time=start_time)
    if "endTime" in request.json:
        end_time = datetime.datetime.strptime(request.json["endTime"], source_format).strftime(target_format)
        sql += " and TheDate <= '{end_time}'".format(end_time=end_time)

    if "vwapStartTime" in request.json or "vwapEndTime" in request.json:
        if request.json["vwapStartTime"] and request.json["vwapEndTime"]:
            sql = "select * from ({sql}) as mintue_trade where mintue>='{vwapStartTime}' and mintue<='{vwapEndTime}'".format(
                sql=sql,
                vwapStartTime=request.json["vwapStartTime"], vwapEndTime=request.json["vwapEndTime"])
    print(sql)
    df = pd.read_sql(sql, con=db.get_engine(bind="users"))
    if len(df) == 0:
        return response
    df["DateTime"] = df["TheDate"].apply(lambda x: x.strftime("%Y-%m-%d"))
    df["Money"] = df["Price"] * df["Volume"]
    res = []

    for day, data in df.groupby("DateTime"):
        day_res = []
        startTime = datetime.datetime.strptime(day + request.json["vwapStartTime"], "%Y-%m-%d%H:%M")
        endTime = datetime.datetime.strptime(day + request.json["vwapEndTime"], "%Y-%m-%d%H:%M")
        for code, ddd in data.groupby("Code"):
            vwap = kafang_vwap(code, startTime, endTime)

            open_df = ddd[ddd["Direction"] == 0]
            if len(open_df) > 0 and True:
                avg_price = (open_df["Price"] * open_df["Volume"]).sum() / open_df["Volume"].sum()
                day_res.append(
                    {"Code": code, "diff": (1 - avg_price / vwap) * open_df["Money"].sum(), "Direction": "open",
                     "Money": open_df["Money"].sum()})

            close_df = ddd[ddd["Direction"] == 1]
            if len(close_df) > 0 and True:
                avg_price = (close_df["Price"] * close_df["Volume"]).sum() / close_df["Volume"].sum()
                day_res.append(
                    {"Code": code, "diff": (avg_price / vwap - 1) * close_df["Money"].sum(), "Direction": "close",
                     "Money": close_df["Money"].sum()})

        diff_df = pd.DataFrame(day_res)
        diff = diff_df["diff"].sum() * 10000 / diff_df["Money"].sum()
        res.append({"Day": day, "vwapDiff": diff})
    df = pd.DataFrame(res)
    df.sort_values("Day", inplace=True)

    file_name = "%d.csv" % int(time.time())
    df.to_csv(os.path.join(get_kafang_dir(), file_name), index=False)
    response["file_name"] = file_name

    response["DateTime"] = list(df["Day"])
    response["vwapDiff"] = list(df["vwapDiff"])
    response["vwapDiffMean"] = round(df["vwapDiff"].mean(), 3)
    return response


def kafang_vwap(code, start_time, end_time):
    table_name = start_time.strftime("%Y_%m")
    if (end_time - start_time).seconds < 300:
        start_time = end_time - datetime.timedelta(seconds=300)
    sql = "select * from `{table_name}` where Code='{code}' and Date>='{start_time}' and Date <'{end_time}'".format(
        code=code, table_name=table_name, start_time=start_time.strftime("%Y-%m-%d %H:%M:%S"),
        end_time=end_time.strftime("%Y-%m-%d %H:%M:%S"))
    print(sql)
    df = pd.read_sql(sql=sql, con=db.get_engine(bind="m5"))
    # print(df)
    return df["OpenInterest"].sum() / df["Volume"].sum() / 100


def get_whh_data_1217():
    vwapStartTime = "10:00"
    vwapEndTime = "10:30"
    file = r"D:\fym\whhxy_1201-1223.csv"
    df = pd.read_csv(file, encoding='gb18030')
    df = df[df["备注"] == "证券卖出"]
    df["Direction"] = 1
    df["TheDate"] = df.apply(lambda x: datetime.datetime.strptime("%08d%s" % (x["成交日期"], x["成交时间"]), "%Y%m%d%H:%M:%S"),
                             axis=1)

    df["Minute"] = df["TheDate"].apply(lambda x: x.strftime("%H:%M"))
    df = df[(df["Minute"] >= vwapStartTime) & (df["Minute"] <= vwapEndTime)]

    df = df[["成交均价", "证券代码", "成交数量", "TheDate", "Direction"]]
    df.rename(columns={
        "成交均价": "Price",
        "成交数量": "Volume",
        "证券代码": "Code"
    }
        , inplace=True)
    df["Code"] = df["Code"].apply(lambda x: "%06d.SK" % x)
    df["Volume"] = df["Volume"].abs()
    return df, vwapStartTime, vwapEndTime


# whhxy_1201-1223.csv

def get_cheng_data_1125_1():
    vwapStartTime = "09:45"
    vwapEndTime = "10:30"
    file = r"D:\fym\20201218——222.csv"
    df = pd.read_csv(file, encoding='gb18030')
    df = df[df["买卖标志"] == "证券卖出"]
    df["Direction"] = 1
    df["TheDate"] = df.apply(lambda x: datetime.datetime.strptime("%08d%06d" % (x["成交日期"], x["签署时间"]), "%Y%m%d%H%M%S"),
                             axis=1)
    df = df[["成交价格", "证券代码", "成交数量", "TheDate", "Direction"]]
    df.rename(columns={
        "成交价格": "Price",
        "成交数量": "Volume",
        "证券代码": "Code"
    }
        , inplace=True)
    df["Code"] = df["Code"].apply(lambda x: "%06d.SK" % x)
    df = df[df["Code"].apply(lambda x: x.startswith("0") or x.startswith("3") or x.startswith("6"))]
    df["Volume"] = df["Volume"].abs()
    return df, vwapStartTime, vwapEndTime


def get_cheng_data_1125():
    vwapStartTime = "09:45"
    vwapEndTime = "10:30"
    file = r"D:\fym\20201125.csv"
    df = pd.read_csv(file, encoding='gb18030')
    df = df[df["买卖标志"] == "证券卖出"]
    df["Direction"] = 1
    df["TheDate"] = df.apply(lambda x: datetime.datetime.strptime("%08d%s" % (x["成交日期"], x["成交时间"]), "%Y%m%d%H:%M:%S"),
                             axis=1)
    df = df[["成交价格", "证券代码", "成交数量", "TheDate", "Direction"]]
    df.rename(columns={
        "成交价格": "Price",
        "成交数量": "Volume",
        "证券代码": "Code"
    }
        , inplace=True)
    df["Code"] = df["Code"].apply(lambda x: "%06d.SK" % x)
    df["Volume"] = df["Volume"].abs()
    return df, vwapStartTime, vwapEndTime


def get_kafang_dir():
    dirpath = os.path.join(main.root_path, 'kafangVwapUpload')
    if not os.path.exists(dirpath):
        os.mkdir(dirpath)
    return dirpath


@main.route('/bf/product/run_cheng', methods=['GET', 'POST'])
@filter()
def run_cheng():
    response = {"DateTime": [], "vwapDiff": [], "twapDiff": [], "twapDiffMean": 0, "vwapDiffMean": 0}

    # df, vwapStartTime, vwapEndTime = get_cheng_data()
    # df, vwapStartTime, vwapEndTime = get_cheng_data_1027()
    df, vwapStartTime, vwapEndTime = get_whh_data_1217()

    df["DateTime"] = df["TheDate"].apply(lambda x: x.strftime("%Y-%m-%d"))
    df["Money"] = df["Price"] * df["Volume"]
    res = []
    detail_list = []
    for day, data in df.groupby("DateTime"):
        day_res = []
        startTime = datetime.datetime.strptime(day + vwapStartTime, "%Y-%m-%d%H:%M")
        endTime = datetime.datetime.strptime(day + vwapEndTime, "%Y-%m-%d%H:%M")
        for code, ddd in data.groupby("Code"):
            vwap = kafang_vwap(code, startTime, endTime)

            open_df = ddd[ddd["Direction"] == 0]
            if len(open_df) > 0 and True:
                avg_price = (open_df["Price"] * open_df["Volume"]).sum() / open_df["Volume"].sum()
                day_res.append(
                    {"Code": code, "diff": (1 - avg_price / vwap) * open_df["Money"].sum(), "Direction": "open",
                     "Money": open_df["Money"].sum(), "avgPx": avg_price, "vwap": vwap, "qty": open_df["Volume"].sum()})

            close_df = ddd[ddd["Direction"] == 1]
            if len(close_df) > 0 and True:
                avg_price = (close_df["Price"] * close_df["Volume"]).sum() / close_df["Volume"].sum()
                day_res.append(
                    {"Code": code, "diff": (avg_price / vwap - 1) * close_df["Money"].sum(), "Direction": "close",
                     "Money": close_df["Money"].sum(), "avgPx": avg_price, "vwap": vwap,
                     "qty": open_df["Volume"].sum()})

        diff_df = pd.DataFrame(day_res)
        detail_list.append(diff_df)

        diff = diff_df["diff"].sum() * 10000 / diff_df["Money"].sum()
        res.append({"Day": day, "vwapDiff": diff, "minDealDate": data["TheDate"].min().strftime("%H%M")})
    df = pd.DataFrame(res)
    df.sort_values("Day", inplace=True)

    file_name = "%d.csv" % int(time.time())
    df.to_csv(os.path.join(get_kafang_dir(), file_name), index=False)
    pd.concat(detail_list).to_csv(os.path.join(get_kafang_dir(), "detail_" + file_name), index=False)

    response["DateTime"] = list(df["Day"])
    response["vwapDiff"] = list(df["vwapDiff"])
    response["vwapDiffMean"] = round(df["vwapDiff"].mean(), 3)
    response["file_name"] = file_name
    return response


@main.route('/bf/product/run_xuntou2', methods=['GET', 'POST'])
@filter()
def run_xuntou2():
    '''
    后续用于计算迅投
    上传文件
    通过文件计算
    '''
    # todo 若有修改迅投也要改
    response = {"DateTime": [], "vwapDiff": [], "twapDiff": [], "twapDiffMean": 0, "vwapDiffMean": 0}
    # file = r"D:\fym\flows\txh_绩效统计.csv"
    file = request.files['file']
    df = pd.read_csv(file, encoding='gb18030')
    now_day = int(datetime.datetime.now().strftime("%Y%m%d"))
    df = df[df["日期"] != now_day]

    df["数量"] = df["数量"].apply(lambda x: int(x.replace(",", "")))
    df["成交均价"] = df["成交均价"].astype(float)
    df["Money"] = df["数量"] * df["成交均价"]

    df["TheDateStart"] = df.apply(
        lambda x: datetime.datetime.strptime("%dT%s" % (x["日期"], x["开始时间"]), "%Y%m%dT%H:%M:%S"), axis=1)
    df["TheDateEnd"] = df.apply(lambda x: datetime.datetime.strptime("%dT%s" % (x["日期"], x["结束时间"]), "%Y%m%dT%H:%M:%S"),
                                axis=1)
    df["Code"] = df["证券代码"].apply(lambda x: "%06d.SK" % x)
    df["VwapOur"] = df.apply(lambda x: kafang_vwap(x["Code"], x["TheDateStart"], x["TheDateEnd"]), axis=1)
    df["diff_vwap"] = ((df["成交均价"] / df["VwapOur"] - 1) * df["Money"]) * df["方向"].apply(
        lambda x: -1 if x == "买入" else 1)
    # df[["Code", "TheDateStart", "TheDateEnd", "VWAP", "VwapOur", "diff_vwap"]].to_csv(r"D:\fym\data\fannn.csv",
    #                                                                                   index=False)
    day_list = []
    for day, data in df.groupby("日期"):
        res = {}
        res["DateTime"] = day
        res["bp_buy_sell"] = data["diff_vwap"].sum() * 10000 / data["Money"].sum()

        buy_df = data[data["方向"] == "买入"]
        if len(buy_df) > 0:
            res["bp_buy"] = buy_df["diff_vwap"].sum() * 10000 / buy_df["Money"].sum()

        sell_df = data[data["方向"] == "卖出"]
        if len(sell_df) > 0:
            res["bp_sell"] = sell_df["diff_vwap"].sum() * 10000 / sell_df["Money"].sum()

        day_list.append(res)

    day_df = pd.DataFrame(day_list)
    file_name = "%d.csv" % int(time.time())
    day_df.to_csv(os.path.join(get_kafang_dir(), file_name), index=False)

    response["file_name"] = file_name
    response["DateTime"] = list(day_df["DateTime"])
    response["vwapDiff"] = list(day_df["bp_buy_sell"])
    response["vwapDiffMean"] = round(day_df["bp_buy_sell"].mean(), 3)
    print(response)
    return response


@main.route('/bf/product/run_kafang2', methods=['GET', 'POST'])
@filter()
def run_kafang():
    response = {"DateTime": [], "vwapDiff": [], "twapDiff": [], "twapDiffMean": 0, "vwapDiffMean": 0}
    file = request.files['file']
    df = pd.read_csv(file, encoding='gb18030')
    df.columns = map(lambda x: x[0].upper() + x[1:], df.columns)

    df["Money"] = df["FilledQty"] * df["AvgPx"]
    df["StartTime"] = df["StartTime"] // 100000
    df["EndTime"] = df["EndTime"] // 100000
    df["TheDateStart"] = df.apply(
        lambda x: datetime.datetime.strptime("%sT%04d" % (x["Date"], x["StartTime"]), "%Y/%m/%dT%H%M"), axis=1)
    df["TheDateEnd"] = df.apply(
        lambda x: datetime.datetime.strptime("%sT%04d" % (x["Date"], x["EndTime"]), "%Y/%m/%dT%H%M"), axis=1)
    df["Code"] = df["Symbol"].apply(lambda x: "%s.SK" % x[:6])
    df["VwapOur"] = df.apply(lambda x: kafang_vwap(x["Code"], x["TheDateStart"], x["TheDateEnd"]), axis=1)
    df["diff_vwap"] = (df["AvgPx"] / df["VwapOur"] - 1) * df["Money"] * df["Side"].apply(
        lambda x: 1 if x == "S" else -1)
    day_list = []
    for day, data in df.groupby("Date"):
        res = {}
        res["DateTime"] = day
        res["bp_buy_sell"] = data["diff_vwap"].sum() * 10000 / data["Money"].sum()

        buy_df = data[data["Side"] == "B"]
        if len(buy_df) > 0:
            res["bp_buy"] = buy_df["diff_vwap"].sum() * 10000 / buy_df["Money"].sum()

        sell_df = data[data["Side"] == "S"]
        if len(sell_df) > 0:
            res["bp_sell"] = sell_df["diff_vwap"].sum() * 10000 / sell_df["Money"].sum()

        day_list.append(res)

    day_df = pd.DataFrame(day_list)
    file_name = "%d.csv" % int(time.time())
    day_df.to_csv(os.path.join(get_kafang_dir(), file_name), index=False)

    response["file_name"] = file_name
    response["DateTime"] = list(day_df["DateTime"])
    response["vwapDiff"] = list(day_df["bp_buy_sell"])
    response["vwapDiffMean"] = round(day_df["bp_buy_sell"].mean(), 3)
    print(response)
    return response


@main.route('/bf/product/rng', methods=['GET', 'POST'])
def eeee():
    print("fangg")
    return "faff"


@main.route('/bf/product/get_kafang_vwap_file', methods=['POST'])
def kafang_vwap_file():
    file_name = request.json["file_name"]
    response = send_from_directory(get_kafang_dir(), file_name, as_attachment=True)
    response.headers["Access-Control-Expose-Headers"] = "Content-disposition"
    return response
