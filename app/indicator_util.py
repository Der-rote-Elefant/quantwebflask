from .models import MarketIndicator


def cal_indicator(df, indicator, parameters):
    formula = indicator.Formula
    try:
        formula = formula.format(*parameters)
    except Exception as e:
        default_parameters = indicator.Parameters.split(",")
        formula = formula.format(*default_parameters)
    return eval(formula)


def ma(df, column="close", n=5):
    ma_column = f"ma_{column}_{n}"
    print(df)
    df[ma_column] = df[column].rolling(window=n, min_periods=1).mean()
    return df[ma_column]


def mid(df):
    df["mid"] = (df["close"] * 3 + df["low"] + df["open"] + df["high"]) / 6
    return df["mid"]


def ref(df, column="close", n=1):
    ref_column = f"ref_{column}_{n}"
    df[ref_column] = df[column].shift(n)
    return df[ref_column]


def dkx(df):
    df["mid"] = mid(df)
    df["dkx"] = 20 * df["mid"]
    for i in range(19, 1, -1):
        df["dkx"] = df["dkx"] + i * ref(df, "mid", 20 - i)
    df["dkx"] = (df["dkx"] + ref(df, "mid", 20)) / 210
    return df["dkx"]


def madkx(df, n=5):
    madkx_column = f"madxk_{n}"
    df["dkx"] = dkx(df)
    df[madkx_column] = ma(df, column="dkx", n=n)
    return df[madkx_column]
