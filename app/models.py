from .extensions import db
import decimal
import datetime
from dateutil.relativedelta import relativedelta
from sqlalchemy.orm import class_mapper
import pandas as pd
import pymysql

DATE_FORMAT = "%Y-%m-%d"
MINUTE_FORMAT = "%Y-%m-%d %H:%M"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
KLINE_START_DATE = "2010-01-01"
DEFAULT_LENGTH = 240
DICT_PERIOD_DEFAULT_DAY = {
    "Day": 240,
    "M5": 5,
    "M1": 2
}


class StockInfo(object):
    def __init__(self, obj_dict):
        self.code = obj_dict["code"]
        if "name" in obj_dict:
            self.name = obj_dict["name"]
        else:
            self.name = None
        if "isIndex" in obj_dict:
            self.is_index = obj_dict["isIndex"]
        else:
            self.is_index = None
        if "stockType" in obj_dict:
            self.stock_type = obj_dict["stockType"]
            if self.stock_type == "stock":
                if "SK" not in self.code:
                    self.code += ".SK"
        else:
            self.stock_type = None

    def to_dict(self):
        return {
            "code": self.code,
            "name": self.name,
            "stockType": self.stock_type,
            "isIndex": self.is_index
        }

    def get_db_engine(self, period):
        stock_db_dict = {
            "stock_Day": db.get_engine(bind="stock_day"),
            "stock_M5": db.get_engine(bind="stock_m5"),
            "future_Day": db.get_engine(bind="future"),
            "future_M1": db.get_engine(bind="future_m1"),
            "future_M5": db.get_engine(bind="future_m5"),
        }
        return stock_db_dict["{0}_{1}".format(self.stock_type, period)]

    def __get_stock_df(self, period, start, end, is_adj=False):
        dfs = list()
        if period == "Day" or is_adj:
            start_time = pd.datetime.strptime(start, DATE_FORMAT)
            end_time = pd.datetime.strptime(end, DATE_FORMAT)
            while True:
                if end_time >= start_time:
                    sql = "select productid as code,date,open,high,low,close,volume," \
                          "openInterest as amount, adjust_price from day_{0} where productid='{1}' and date>='{2}' " \
                          "and date<='{3}' order by date asc".format(start_time.year, self.code, start, end)
                    df = pd.read_sql(sql=sql, con=self.get_db_engine("Day"))
                    dfs.append(df)
                    start_time = start_time + relativedelta(years=1)
                else:
                    break
            df_day = pd.concat(dfs, ignore_index=True, sort=True)
            df_day["adj_rate"] = df_day["adjust_price"] / df_day["close"]
            df_day["day"] = df_day["date"].apply(lambda x: x.strftime(DATE_FORMAT))
            df_day["date"] = df_day["date"].apply(lambda x: x.strftime(DATE_FORMAT))
            if period == "Day":
                df_res = df_day
        dfs.clear()
        if period == "M5":
            start_time = pd.datetime.strptime(start, DATE_FORMAT)
            end_time = pd.datetime.strptime(end, DATE_FORMAT)
            while True:
                if end_time >= start_time:
                    sql = "select code,date,open,high,low,close,volume,openInterest as amount " \
                          "from {0} where code='{1}' and date>='{2}' and date<='{3}'" \
                          "order by date asc".format(start_time.strftime("%Y_%m"), self.code, start, end)
                    df = pd.read_sql(sql=sql, con=self.get_db_engine(period))
                    dfs.append(df)
                    start_time = start_time + relativedelta(months=1)
                else:
                    break
            df_m5 = pd.concat(dfs, ignore_index=True, sort=True)
            df_m5["day"] = df_m5["date"].apply(lambda x: x.strftime(DATE_FORMAT))
            df_m5["date"] = df_m5["date"].apply(lambda x: x.strftime(MINUTE_FORMAT))
            if is_adj:
                df_adj = df_day.loc[:, ["code", "day", "adj_rate"]]
                df_m5 = pd.merge(left=df_m5, right=df_adj, on=["code", "day"], how="outer")
            df_res = df_m5
        if period not in ["Day", "M5"]:
            df_res = pd.DataFrame(
                columns=["code", "date", "open", "high", "low", "close", "volume", "amount", "adj_rate"])
            # raise RuntimeError("暂不支持其他周期")

        if is_adj:
            df_res["open"] = df_res["open"] * df_res["adj_rate"]
            df_res["high"] = df_res["high"] * df_res["adj_rate"]
            df_res["low"] = df_res["low"] * df_res["adj_rate"]
            df_res["close"] = df_res["close"] * df_res["adj_rate"]
        return df_res

    def __get_future_df(self, period, start, end):
        code = self.code.replace("999", "000")
        dfs = list()
        start_time = pd.datetime.strptime(start, DATE_FORMAT)
        end_time = pd.datetime.strptime(end, DATE_FORMAT)
        if period == "Day":
            while True:
                if end_time >= start_time:
                    sql = "select InstrumentID as code,date,open,high,low,close,volume," \
                          "openInterest as amount from day_{0} where InstrumentID='{1}' and date>='{2}' " \
                          "and date<='{3}' order by date asc".format(start_time.year, code, start, end)
                    df = pd.read_sql(sql=sql, con=self.get_db_engine(period))
                    dfs.append(df)
                    start_time = start_time + relativedelta(years=1)
                else:
                    break
            df_future = pd.concat(dfs, ignore_index=True, sort=True)
            df_future["date"] = df_future["date"].apply(lambda x: x.strftime(DATE_FORMAT))
            df_future["code"] = self.code
            return df_future
        elif period == "M5" or period == "M1":
            while True:
                if end_time >= start_time:
                    sql = "select InstrumentID as code,date,open,high,low,close,volume,openInterest as amount " \
                          "from {0} where InstrumentID='{1}' and date>='{2}' and date<='{3}' " \
                          "order by date asc".format(start_time.strftime("%Y_%m"), code, start, end)
                    df = pd.read_sql(sql=sql, con=self.get_db_engine(period))
                    dfs.append(df)
                    start_time = start_time + relativedelta(months=1)
                else:
                    break
            df_future = pd.concat(dfs, ignore_index=True, sort=True)
        else:
            df_future = pd.DataFrame(columns=["code", "date", "open", "high", "low", "close", "volume", "amount"])
            # raise RuntimeError("暂不支持其他周期")

        df_future["date"] = df_future["date"].apply(lambda x: x.strftime(MINUTE_FORMAT))
        df_future["code"] = self.code
        return df_future

    def get_df(self, period, start, end, is_adj=False):
        if self.stock_type == "stock":
            return self.__get_stock_df(period, start, end, is_adj)
        else:
            return self.__get_future_df(period, start, end)

    def get_date_range(self):
        if self.stock_type == "stock":
            return self.__get_stock_date_range()
        else:
            return self.__get_future_date_range()

    @classmethod
    def get_default_date(cls, period, max_date):
        start_date = pd.datetime.strptime(max_date, DATE_FORMAT) - relativedelta(days=DICT_PERIOD_DEFAULT_DAY[period])
        return start_date.strftime(DATE_FORMAT), max_date

    def __get_stock_date_range(self):
        dfs = list()
        start_time = pd.datetime.strptime(KLINE_START_DATE, DATE_FORMAT)
        end_time = pd.datetime.today()
        start = KLINE_START_DATE
        end = end_time.strftime(DATE_FORMAT)
        while True:
            if end_time >= start_time:
                sql = "select max(date) as max_date, min(date) as min_date " \
                      "from day_{0} " \
                      "where productid='{1}' and date>='{2}' and date<='{3}' order by date asc" \
                    .format(start_time.year, self.code, start, end)
                df = pd.read_sql(sql=sql, con=self.get_db_engine("Day"))
                dfs.append(df)
                start_time = start_time + relativedelta(years=1)
            else:
                break
        df_res = pd.concat(dfs, ignore_index=True, sort=True)
        if len(df_res) <= 0:
            return start, end
        min_date = df_res["min_date"].min().strftime(DATE_FORMAT)
        max_date = df_res["max_date"].max().strftime(DATE_FORMAT)
        return min_date, max_date

    def __get_future_date_range(self):
        dfs = list()
        start_time = pd.datetime.strptime(KLINE_START_DATE, DATE_FORMAT)
        end_time = pd.datetime.today()
        start = KLINE_START_DATE
        end = end_time.strftime(DATE_FORMAT)
        code = self.code.replace("999", "000")
        while True:
            if end_time >= start_time:
                sql = "select max(date) as max_date, min(date) as min_date " \
                      "from day_{0} where InstrumentID='{1}' and date>='{2}' and date<='{3}' " \
                      "order by date asc".format(start_time.year, code, start, end)
                df = pd.read_sql(sql=sql, con=self.get_db_engine("Day"))
                dfs.append(df)
                start_time = start_time + relativedelta(years=1)
            else:
                break
        df_future = pd.concat(dfs, ignore_index=True, sort=True)
        if len(df_future) <= 0:
            return start, end
        min_date = df_future["min_date"].min().strftime(DATE_FORMAT)
        max_date = df_future["max_date"].max().strftime(DATE_FORMAT)
        return min_date, max_date


def to_dict(obj, date_format=True):
    result = []
    for col in class_mapper(obj.__class__).mapped_table.c:
        v = getattr(obj, col.name)
        if isinstance(v, decimal.Decimal):
            v = float(v)
        if date_format and isinstance(v, datetime.datetime):
            v = v.strftime("%Y-%m-%d %H:%M:%S")
        result.append((col.name, v))
    return dict(result)


def to_list(list, date_format=True):
    return [to_dict(i, date_format) for i in list]


class SparkTask(db.Model):
    __tablename__ = 'spark_task'
    Id = db.Column(db.Integer, primary_key=True)
    ApplicationsId = db.Column(db.String(63))
    Name = db.Column(db.String(63))
    Description = db.Column(db.String(255))
    Parameter = db.Column(db.String(255))
    TaskType = db.Column(db.String(63))
    Status = db.Column(db.String(63))
    CreateTime = db.Column(db.Integer)
    SpendTime = db.Column(db.Integer)


class MarketIndicator(db.Model):
    __tablename__ = 'market_indicator'

    Id = db.Column(db.Integer, primary_key=True)
    Category = db.Column(db.String(63), info='指标类别')
    Description = db.Column(db.String(255), info='指标描述')
    Parameters = db.Column(db.String(255), info='指标默认参数，多个值用逗号隔开')
    Formula = db.Column(db.String(255), info='指标公式')
    GraphType = db.Column(db.Integer, info='0-主图，1-副图')


class Account(db.Model):
    __tablename__ = 'account'

    Id = db.Column(db.Integer, primary_key=True)
    Type = db.Column(db.Integer)
    AccountNumber = db.Column(db.String(255))


class AlgoTradeReport(db.Model):
    __tablename__ = 'algo_trade_report'

    Id = db.Column(db.Integer, primary_key=True)
    UserId = db.Column(db.String(255))
    Code = db.Column(db.String(20))
    Date = db.Column(db.DateTime)
    Direction = db.Column(db.Integer)
    BusinessBalance = db.Column(db.Numeric(18, 2))
    PendingBalance = db.Column(db.Numeric(18, 2))
    ChasingBalance = db.Column(db.Numeric(18, 2))
    EntrustTime = db.Column(db.Integer)
    PendingTime = db.Column(db.Integer)
    ChasingTime = db.Column(db.Integer)
    AverageSlip = db.Column(db.Numeric(10, 4))
    AverageLash = db.Column(db.Numeric(10, 4))
    WithdrawRate = db.Column(db.Numeric(10, 4))
    PendingRate = db.Column(db.Numeric(10, 4))


class Balance(db.Model):
    __tablename__ = 'balance'

    Id = db.Column(db.Integer, primary_key=True)
    Date = db.Column(db.DateTime)
    MarketValue = db.Column(db.Numeric(10, 2))
    Profit = db.Column(db.Float(14, True))
    FundId = db.Column(db.Integer)
    InitialFunds = db.Column(db.Float(9, True))
    Cash = db.Column(db.Float(11, True))
    ShortMarketValue = db.Column(db.Numeric(10, 2))


class Fund(db.Model):
    __tablename__ = 'fund'

    Id = db.Column(db.Integer, primary_key=True)
    BaseCode = db.Column(db.String(255))
    FundName = db.Column(db.String(255))
    InitialFunds = db.Column(db.Float(9, True))
    CreateDate = db.Column(db.DateTime)
    FundType = db.Column(db.Integer)
    BaseFund = db.Column(db.Integer)


class FundAccountRelation(db.Model):
    __tablename__ = 'fund_account_relation'

    Id = db.Column(db.Integer, primary_key=True)
    AccountId = db.Column(db.Integer)
    FundId = db.Column(db.Integer)


class FundLooptestInfo(db.Model):
    __tablename__ = 'fund_looptest_info'

    Id = db.Column(db.Integer, primary_key=True)
    FundId = db.Column(db.Integer)
    Date = db.Column(db.DateTime)
    FundSolution = db.Column(db.Text)
    FundExposure = db.Column(db.Text)
    Expect = db.Column(db.Text)


class FundRemark(db.Model):
    __tablename__ = 'fund_remark'

    Id = db.Column(db.Integer, primary_key=True)
    FundId = db.Column(db.Integer)
    Remark = db.Column(db.Text)
    Date = db.Column(db.DateTime)


class FutureTick(db.Model):
    __tablename__ = 'future_tick'

    Id = db.Column(db.Integer, primary_key=True)
    InstrumentID = db.Column(db.String(10))
    TradingDay = db.Column(db.String(10))
    ActionDay = db.Column(db.String(10))
    UpdateTime = db.Column(db.String(10))
    UpdateMillisec = db.Column(db.Integer)
    Price = db.Column(db.Numeric(18, 2))
    Volume = db.Column(db.Numeric(18, 2))
    HighestPrice = db.Column(db.Numeric(18, 2))
    LowestPrice = db.Column(db.Numeric(18, 2))
    OpenPrice = db.Column(db.Numeric(18, 2))
    ClosePrice = db.Column(db.Numeric(18, 2))
    AveragePrice = db.Column(db.Numeric(18, 2))
    AskPrice = db.Column(db.Numeric(18, 2))
    AskVolume = db.Column(db.Numeric(18, 2))
    BidPrice = db.Column(db.Numeric(18, 2))
    BidVolume = db.Column(db.Numeric(18, 2))
    UpperLimitPrice = db.Column(db.Numeric(18, 2))
    LowerLimitPrice = db.Column(db.Numeric(18, 2))
    OpenInterest = db.Column(db.Numeric(18, 2))
    TurnOver = db.Column(db.Numeric(18, 2))
    PreClosePrice = db.Column(db.Numeric(18, 2))
    PreOpenInterest = db.Column(db.Numeric(18, 2))
    PreSettlementPrice = db.Column(db.Numeric(18, 2))


class LooptestBalance(db.Model):
    __tablename__ = 'looptest_balance'

    Id = db.Column(db.Integer, primary_key=True)
    Date = db.Column(db.DateTime)
    Profit = db.Column(db.Float(6, True))
    FundId = db.Column(db.Integer)
    RefreshDate = db.Column(db.DateTime)


class LooptestBalanceStrategy(db.Model):
    __tablename__ = 'looptest_balance_strategy'

    Id = db.Column(db.Integer, primary_key=True)
    LoopTestId = db.Column(db.Integer)
    Date = db.Column(db.DateTime)
    Profit = db.Column(db.Float(6, True))
    RefreshDate = db.Column(db.DateTime)


class LooptestBalanceTag(db.Model):
    __tablename__ = 'looptest_balance_tag'

    Id = db.Column(db.Integer, primary_key=True)
    TagId = db.Column(db.Integer)
    Date = db.Column(db.DateTime)
    Profit = db.Column(db.Float(6, True))
    RefreshDate = db.Column(db.DateTime)


class Position(db.Model):
    __tablename__ = 'position'

    Id = db.Column(db.Integer, primary_key=True)
    FundId = db.Column(db.Integer)
    Code = db.Column(db.String(255))
    Vol = db.Column(db.Integer)
    Direction = db.Column(db.Integer)
    Date = db.Column(db.DateTime)
    Price = db.Column(db.Float(8, True))


class StockDayM1(db.Model):
    __tablename__ = 'stock_day_m1'

    Id = db.Column(db.Integer, primary_key=True)
    Code = db.Column(db.String(20), unique=True)
    Date = db.Column(db.DateTime, index=True)
    Open = db.Column(db.Numeric(18, 2))
    High = db.Column(db.Numeric(18, 2))
    Low = db.Column(db.Numeric(18, 2))
    Close = db.Column(db.Numeric(18, 2))
    Volume = db.Column(db.Numeric(18, 2))
    OpenInsterest = db.Column(db.Numeric(18, 0))
    RefreshDate = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())


class TagLooptestInfo(db.Model):
    __tablename__ = 'tag_looptest_info'

    Id = db.Column(db.Integer, primary_key=True)
    Date = db.Column(db.DateTime)
    Solution = db.Column(db.Text)
    Exposure = db.Column(db.Text)
    Expect = db.Column(db.Text)
    Category = db.Column(db.String)


class Tradeflow(db.Model):
    __tablename__ = 'tradeflow'

    Id = db.Column(db.Integer, primary_key=True)
    AccountId = db.Column(db.Integer)
    Code = db.Column(db.String(255))
    Price = db.Column(db.Float(8, True))
    Vol = db.Column(db.Integer)
    TradeDate = db.Column(db.DateTime)
    Direction = db.Column(db.Integer)
    Type = db.Column(db.Integer)


class Strategy(db.Model):
    __tablename__ = 'strategy'

    Id = db.Column(db.Integer, primary_key=True)
    Tag = db.Column(db.String(255))
    Describe = db.Column(db.Text)
    Type = db.Column(db.Integer)
    InitialFund = db.Column(db.Float(9, True))
    BaseCode = db.Column(db.String(255))


class StrategyBalance(db.Model):
    __tablename__ = 'strategy_balance'

    Id = db.Column(db.Integer, primary_key=True)
    Date = db.Column(db.DateTime)
    MarketValue = db.Column(db.Numeric(10, 2))
    Profit = db.Column(db.Float(6, True))
    StrategyId = db.Column(db.Integer)
    InitialFunds = db.Column(db.Float(9, True))
    Cash = db.Column(db.Float(11, True))
    ShortMarketValue = db.Column(db.Numeric(10, 2))


class StrategyFlow(db.Model):
    __tablename__ = 'strategy_flow'

    Id = db.Column(db.Integer, primary_key=True)
    StrategyId = db.Column(db.Integer)
    Code = db.Column(db.String(255))
    Price = db.Column(db.Float(8, True))
    Vol = db.Column(db.Integer)
    TradeDate = db.Column(db.DateTime)
    Direction = db.Column(db.Integer)
    Type = db.Column(db.Integer)


class StrategyPosition(db.Model):
    __tablename__ = 'strategy_position'

    Id = db.Column(db.Integer, primary_key=True)
    StrategyId = db.Column(db.Integer)
    Code = db.Column(db.String(255))
    Vol = db.Column(db.Integer)
    Direction = db.Column(db.Integer)
    Date = db.Column(db.DateTime)
    Price = db.Column(db.Float(8, True))


class FundStrategyRelation(db.Model):
    __tablename__ = 'fund_strategy_relation'

    Id = db.Column(db.Integer, primary_key=True)
    StrategyId = db.Column(db.Integer)
    FundId = db.Column(db.Integer)


class Function(db.Model):
    __tablename__ = 'function'

    Id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60))
    detail_func = db.Column(db.TEXT)
    info = db.Column(db.TEXT)


class AccrualFlow(db.Model):
    __tablename__ = 'accrual_flow'

    Id = db.Column(db.Integer, primary_key=True)
    FundId = db.Column(db.Integer)
    Price = db.Column(db.Float(8, True))
    Vol = db.Column(db.Integer)
    AccrualDate = db.Column(db.DateTime)
    Direction = db.Column(db.Integer)


class GeneInfo(db.Model):
    __tablename__ = 'gene_info'

    Id = db.Column(db.Integer, primary_key=True)
    Code = db.Column(db.String(255), info='因子名')
    Formula = db.Column(db.String(500), info='公式')
    Description = db.Column(db.String(255), info='因子描述')
    ScriptPath = db.Column(db.String(255), info='脚本文件路径')
    Writer = db.Column(db.String(255), info='脚本文件路径')


class GeneTag(db.Model):
    __tablename__ = 'gene_tag'

    Id = db.Column(db.Integer, primary_key=True)
    Description = db.Column(db.String(500), info='标签描述')
    IsMandatory = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue(), info='是否必须')


class GeneTagRelation(db.Model):
    __tablename__ = 'gene_tag_relation'

    Id = db.Column(db.Integer, primary_key=True)
    GeneId = db.Column(db.Integer, info='因子Id')
    TagId = db.Column(db.Integer, info='因子标签Id')
    TagValueId = db.Column(db.Integer, info='标签值Id')


class GeneTagValue(db.Model):
    __tablename__ = 'gene_tag_values'

    Id = db.Column(db.Integer, primary_key=True)
    TagId = db.Column(db.Integer, info='对应的GeneTagId')
    Value = db.Column(db.String(255), info='因子值')
    Description = db.Column(db.String(255), info='描述')
