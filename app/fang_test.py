# /usr/bin/python3
# coding=utf-8

import sys
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import functions as fn
from pyspark.sql.window import Window
import json
import redis
import requests

looptest_codes = ",".join(["'{0}'".format(c) for c in ["000001.SK", "000002.SK"]])


def get_summary(df):
    indicator_list = list()
    # 净利润
    # todo take running
    net_profit = df.groupby().sum("profit").take(1)[0].asDict()["sum(profit)"]
    # 总盈利
    sum_profit = df.where(df.profit > 0).groupby().sum("profit").take(1)[0].asDict()["sum(profit)"]
    # 总亏损
    sum_loss = df.where(df.profit < 0).groupby().sum("profit").take(1)[0].asDict()["sum(profit)"]
    # 总盈利/总亏损
    sum_profit_rate = sum_profit / sum_loss

    indicator_list.append({
        "key": "net_profit",
        "name": "净利润",
        "value": round(net_profit, 2)
    })
    indicator_list.append({
        "key": "sum_profit",
        "name": "总盈利",
        "value": round(sum_profit, 2)
    })
    indicator_list.append({
        "key": "sum_loss",
        "name": "总亏损",
        "value": round(sum_loss, 2)
    })
    indicator_list.append({
        "key": "sum_profit_rate",
        "name": "总盈利/总亏损",
        "value": round(sum_profit_rate, 2)
    })

    # 交易手数
    trade_nums = df.groupby().sum("qty").take(1)[0].asDict()["sum(qty)"]
    # 盈利手数
    profit_nums = df.where(df.profit > 0).groupby().sum("qty").take(1)[0].asDict()["sum(qty)"]
    # 亏损手数
    loss_nums = df.where(df.profit < 0).groupby().sum("qty").take(1)[0].asDict()["sum(qty)"]
    # 持平手数
    equal_nums = df.where(df.profit == 0).groupby().sum("qty").take(1)[0].asDict()["sum(qty)"]

    indicator_list.append({
        "key": "trade_nums",
        "name": "交易股数",
        "value": trade_nums
    })
    indicator_list.append({
        "key": "profit_nums",
        "name": "盈利股数",
        "value": profit_nums
    })
    indicator_list.append({
        "key": "loss_nums",
        "name": "亏损股数",
        "value": loss_nums
    })
    indicator_list.append({
        "key": "equal_nums",
        "name": "持平股数",
        "value": equal_nums
    })

    # 平均利润
    avg_all_profit = df.groupby().avg("profit").take(1)[0].asDict()["avg(profit)"]
    # 平均盈利
    avg_profit = df.where(df.profit > 0).groupby().avg("profit").take(1)[0].asDict()["avg(profit)"]
    # 平均亏损
    avg_loss = df.where(df.profit < 0).groupby().avg("profit").take(1)[0].asDict()["avg(profit)"]
    # 平均盈利/平均亏损
    avg_profit_rate = avg_profit / avg_loss

    indicator_list.append({
        "key": "avg_all_profit",
        "name": "平均利润",
        "value": round(avg_all_profit, 2)
    })
    indicator_list.append({
        "key": "avg_profit",
        "name": "平均盈利",
        "value": round(avg_profit, 2)
    })
    indicator_list.append({
        "key": "avg_loss",
        "name": "平均亏损",
        "value": round(avg_loss, 2)
    })
    indicator_list.append({
        "key": "avg_profit_rate",
        "name": "平均盈利/平均亏损",
        "value": round(avg_profit_rate, 2)
    })

    # 最大盈利
    max_profit = df.groupby().max("profit").take(1)[0].asDict()["max(profit)"]
    # 最大亏损
    max_loss = df.groupby().min("profit").take(1)[0].asDict()["min(profit)"]
    # 最大盈利/总盈利
    max_profit_rate = max_profit / sum_profit
    # 最大亏损/总亏损
    max_loss_rate = max_loss / sum_loss
    # 净利润/最大亏损
    net_profit_loss_rate = net_profit / max_loss

    indicator_list.append({
        "key": "max_profit",
        "name": "最大盈利",
        "value": round(max_profit, 2)
    })
    indicator_list.append({
        "key": "max_loss",
        "name": "最大亏损",
        "value": round(max_loss, 2)
    })
    indicator_list.append({
        "key": "max_profit_rate",
        "name": "最大盈利/总盈利",
        "value": round(max_profit_rate, 2)
    })
    indicator_list.append({
        "key": "max_loss_rate",
        "name": "最大亏损/总亏损",
        "value": round(max_loss_rate, 2)
    })
    indicator_list.append({
        "key": "net_profit_loss_rate",
        "name": "净利润/最大亏损",
        "value": round(net_profit_loss_rate, 2)
    })
    return indicator_list


def run_demo(task_name):
    spark_master = "spark://master:7077"  # 指定spark standalone模式运行
    spark_conf = SparkConf().setMaster(spark_master).setAppName(task_name)
    # 设置clickhouse依赖,这些参数也可以在提交任务界面指定
    spark_conf.set("spark.jars.packages", "com.github.housepower:clickhouse-native-jdbc:2.3-stable")
    spark_conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    # 指定计算节点只用的内存大小
    # todo 我总感觉是参数错了
    spark_conf.set("spark.driver.memory", "2g")
    spark_conf.set("spark.executor.memory", "4g")
    # 执行的executors数量
    spark_conf.set("num-executors", "2")
    # 每个Executor的核的数量。此核非彼核，它不是机器的CPU核，可以理解为Executor的一个线程
    spark_conf.set("spark.executor.cores", 4)
    # spark.cores.max：为一个application分配的最大cpu核心数，如果没有设置这个值默认为spark.deploy.defaultCores
    spark_conf.set('spark.cores.max', 20)
    # 启用任务事件记录器，开启后记录到hdfs文件系统中，访问master:18080可以查看历史任务运行情况
    spark_conf.set("spark.eventLog.enabled", "true")
    spark_conf.set("spark.eventLog.dir", "hdfs://master:9000/history-log")

    spark = SparkSession.builder.config(conf=spark_conf).getOrCreate()
    spark.sparkContext.setLogLevel('WARN')
    application_id = spark.sparkContext.applicationId
    clickhouse_driver = "com.github.housepower.jdbc.ClickHouseDriver"

    df_m5 = spark.read.format("jdbc") \
        .option("driver", clickhouse_driver) \
        .option("url", "jdbc:clickhouse://192.168.1.126:9000/stocks") \
        .option("user", "default") \
        .option("password", "Baofang660") \
        .option("query", "select * from m5_code where code in ({0})".format(looptest_codes)) \
        .load()

    df_day = spark.read.format("jdbc") \
        .option("driver", clickhouse_driver) \
        .option("url", "jdbc:clickhouse://192.168.1.126:9000/stocks") \
        .option("user", "default") \
        .option("password", "Baofang660") \
        .option("query", "select * from day where code in ({0})".format(looptest_codes)) \
        .load()

    df_day = df_day.withColumn("adj_rate", df_day.adjust_price / df_day.close)
    df_day = df_day.select("code", "date", "adj_rate")
    df_m5 = df_m5.withColumn("day", fn.date_trunc("day", df_m5.date))
    df = df_m5.join(df_day, [df_m5.code == df_day.code, df_m5.day == df_day.date], "inner") \
        .select(df_m5.code, df_m5.date, df_m5.close, (df_m5.close * df_day.adj_rate).alias('adj_close'))
    # df.show()
    # 计算 close 5日均值
    days_window = Window.partitionBy("code").orderBy("date").rowsBetween(-(48 * 5) + 1, 0)
    df = df.withColumn("mean_length", fn.count("*").over(days_window))
    df.select("code", "date", "mean_length").show()
    df = df.withColumn("mean_close240",
                       fn.when(df.mean_length >= 240, fn.mean("close").over(days_window)).otherwise(None))
    # 上突破5日均线买入
    df = df.withColumn("up_break", df.close > df.mean_close240)
    df = df.withColumn("down_break", df.close < df.mean_close240)
    shift_window = Window.partitionBy("code").orderBy("date")
    df = df.withColumn("up_break_1",
                       fn.lag(df.up_break, count=1, default=None).over(shift_window))
    df = df.withColumn("is_open", (~df.up_break_1) & df.up_break)

    # 单次交易金额60000
    SINGLE_MONEY = 60000
    # 设置开仓价格
    df = df.withColumn("open_price", fn.when(df.is_open, df.close).otherwise(0))
    # 设置开仓数量
    df = df.withColumn("qty", fn.when(df.is_open, fn.floor(SINGLE_MONEY / df.close / 100) * 100).otherwise(0))
    # 设置平仓日期 ，持仓2日
    df = df.withColumn("close_date", fn.lead(df.date, count=48 * 2, default=None).over(shift_window))
    # 设置平仓价格
    df = df.withColumn("close_price", fn.lead(df.close, count=48 * 2, default=None).over(shift_window))
    # 筛选有成交的日期
    df = df.where(df.is_open).select("code", "date", "close_date", "open_price", "close_price", "qty")
    # 计算收益
    df = df.withColumn("profit", (df["close_price"] - df["open_price"]) * df["qty"])
    df = df.withColumn("application_id", fn.when(df.profit > 0, application_id).otherwise(application_id))
    df = df.dropna()
    # 写入clickhouse
    df_record = df.select("code", "date", "qty", "open_price", "close_date", "close_price", "profit", "application_id")
    # 这里发生计算
    df_record.write.format("jdbc").mode("append") \
        .option("driver", clickhouse_driver) \
        .option("url", "jdbc:clickhouse://192.168.1.126:9000/stocks") \
        .option("user", "default") \
        .option("password", "Baofang660") \
        .option("isolationLevel", "NONE") \
        .option("numPartitions", "1") \
        .option("dbtable", "spark_looptest") \
        .save()
    all_list = get_summary(df)
    long_list = all_list
    result = {
        "allTrades": all_list,
        "longTrades": long_list,
        "short_list": list(),
        "allSummary": list(),
        "assetWithdraw": list(),
        "assetWithdrawRate": list()
    }

    requests.post("http://192.168.1.161:5000/cheng/submit_state",
                  data={"application_id": application_id, "status": "success"}
                  )
    # 保存结果到redis中
    cache = redis.StrictRedis(host='192.168.1.126', port=6379, db=0)
    cache.set("spark_looptest_summary_{0}".format(application_id), json.dumps(result))
    spark.sparkContext.stop()


if __name__ == '__main__':
    dict_arg = dict(map(lambda x: x.split("="), sys.argv[1:]))
    if "task_name" in dict_arg:
        run_demo(dict_arg["task_name"])
