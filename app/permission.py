# -*- coding: UTF-8 -*-
from sqlalchemy.orm import class_mapper
from flask import session, jsonify
from .models_user import *
from sqlalchemy import and_, or_
from .util.log_util import logger
from functools import wraps


def success(data):
    return {"code": 1, "data": data}


def error(msg):
    return {"code": 0, "msg": msg}


def filter_res(role=None):
    def role_decorate(func):
        @wraps(func)
        def role_wrap(*args, **keys):
            if role is None:
                try:
                    return jsonify({
                        "httpCode": 200,
                        "responseEntity": func(*args, **keys)}
                    )
                except Exception as e:
                    logger.exception(e)
                    return jsonify({
                        "httpCode": -1,
                        "msg": str(e)
                    })

        return role_wrap

    return role_decorate


class RoleList:
    INIT = 0
    ROLE_LIST = []

    def __init__(self):
        pass

    def __init_list(self):
        if not self.INIT:
            roles = Role.query.all()
            self.ROLE_LIST = [(r.code, r.name) for r in roles]
            self.INIT = 1

    def get_roles(self, ur):
        self.__init_list()
        roles = []
        for r in self.ROLE_LIST:
            if r[0] & ur:
                roles.append(r[1])
        return roles


ROLE = RoleList()


def to_dict(obj):
    result = []
    for col in class_mapper(obj.__class__).mapped_table.c:
        v = getattr(obj, col.name)
        result.append((col.name, v))
    return dict(result)


def success(data):
    return {"code": 1, "data": data}


def error(msg):
    return {"code": 0, "msg": msg}


def filter(role=None):
    def role_decorate(func):
        @wraps(func)
        def role_wrap(*args, **keys):
            if role is None:
                return jsonify(func(*args, **keys))

            if "user" not in session:
                return jsonify(error("未登录"))
            user = session["user"]
            if user:
                if role in user["roles"]:
                    return jsonify(func(*args, **keys))
                else:
                    return jsonify(error("没有权限"))
            else:
                return jsonify(error("未登录"))

        return role_wrap

    return role_decorate


# 入参：orm对象 返回：处理后的字典
def save_user(user):
    u = to_dict(user)
    # 删除敏感信息
    del u["password"]
    del u["openid"]
    # 添加权限
    # u["roles"] = ROLE.get_roles(u["role"])
    u["roles"] = [t.name for t in
                  Tag.query.join(RoleTag, and_(RoleTag.tag_id == Tag.id, RoleTag.role_id == user.role_id)).all()]
    u["token"] = "token"
    session["user"] = u
    return u


def get_user(parm="token"):
    return session["user"]


def del_user(parm="token"):
    session.pop("user")
