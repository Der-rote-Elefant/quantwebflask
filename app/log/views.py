from . import main
from flask import request, jsonify
from ..models import FundRemark, to_list, db
from datetime import datetime
from ..permission import filter
from ..extensions import cache


@main.route('/bf/log/get', methods=['GET'])
@filter()
def get_log_table():
    fund_id = request.args.get("FundId")
    res = to_list(FundRemark.query.filter_by(FundId=fund_id).order_by(FundRemark.Date.desc()).all())
    return {"code": 1, "data": res}


@main.route('/bf/log/add', methods=['POST'])
@filter()
def add_log():
    if "Date" not in request.json or request.json["Date"] == "":
        date = datetime.today()
    else:
        date = request.json["Date"]
    db.session.add(FundRemark(Date=date, Remark=request.json["Remark"], FundId=request.json["FundId"]))
    db.session.commit()
    return {"code": 1, "data": "success"}


@main.after_request
def releaseDB(response):
    db.session.close()
    return response


@main.route('/bf/log/error_data', methods=['Get'])
# @filter()
def get_error_data():
    cache_key = "error2wechat-*"
    msg = ""
    cache.set("error2wechat-test", "this time")
    for key in cache.keys(cache_key):
        msg += str(cache.get(key))
    return msg
