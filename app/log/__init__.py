from flask import Blueprint

main = Blueprint('compound', __name__)

from . import views, errors