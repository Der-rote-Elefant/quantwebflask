# -*- coding: UTF-8 -*-
# 扩展放入这里
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_mongoalchemy import MongoAlchemy
from flask_apscheduler import APScheduler
from flask_redis import FlaskRedis
from flask_compress import Compress
# from flask_cache import Cache
from flask_cors import CORS
from clickhouse_driver import Client
import warnings
import pymysql

mongo = MongoAlchemy()
bootstrap = Bootstrap()
db = SQLAlchemy()
sche = APScheduler()
cache = FlaskRedis()
compress = Compress()
cors = CORS()


class ClickHouseClient:
    def __init__(self):
        self.engine = None

    def init_app(self, app):
        if "CLICKHOUSE_HOST" not in app.config or "CLICKHOUSE_PW" not in app.config or "CLICKHOUSE_DATABASE" not in app.config:
            warnings.warn("clickhouse配置未设置")
        host = app.config["CLICKHOUSE_HOST"]
        password = app.config["CLICKHOUSE_PW"]
        database = app.config["CLICKHOUSE_DATABASE"]
        self.engine = Client(host=host, password=password, database=database)


class ForConfig:
    def __init__(self):
        self.config = None

    def set_config(self, config):
        self.config = config
        return self

    def get_config(self):
        return self.config


clickhouse_client = ClickHouseClient()

for_config = ForConfig()
