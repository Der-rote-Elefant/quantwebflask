# -*- coding: UTF-8 -*-
from flask import jsonify, request
from . import main
from ..models_user import User, db
from ..permission import *
from ..models import Fund, to_list


@main.route('/bf/user/login', methods=['POST'])
def login():
    dict_data = request.json
    username = dict_data["username"]
    password = dict_data["password"]
    user = User.query.filter_by(username=username, password=password).first()
    if not user:
        return jsonify({"code": 0, "msg": "角色不存在||密码错误"})
    u = save_user(user)
    return jsonify({"code": 1, "data": {
        "token": u["token"]
    }})


@main.route('/bf/user/info', methods=['GET', 'POST'])
@filter()
def info():
    return {"code": 1, "data": get_user()}


@main.route('/bf/user/logout', methods=['POST'])
@filter()
def logout():
    del_user()
    return {"code": 1, "data": "等出成功"}


@main.route('/bf/user/getAllInfo', methods=['POST'])
@filter("root")
def get_all_info():
    funds = [(f.Id, f.FundName) for f in Fund.query.filter(Fund.FundType == 0).all()]
    tags = [(r.id, r.name_zh) for r in Tag.query.all()]
    return {"code": 1, "data": {
        "funds": funds,
        "tags": tags
    }}


@main.route('/bf/user/getAllRole', methods=['POST'])
@filter("root")
def get_all_role():
    result = []
    roles = Role.query.all()
    for r in roles:
        res = to_dict(r)
        res["tags"] = to_list(Tag.query.join(RoleTag, and_(RoleTag.role_id == r.id, RoleTag.tag_id == Tag.id)).all())
        res["funds"] = to_list(
            Fund.query.filter(Fund.Id.in_([r.fund_id for r in RoleFund.query.filter_by(role_id=r.id).all()])).all())
        result.append(res)
    return {"code": 1, "data": result}


@main.route('/bf/user/saveRole', methods=['POST'])
@filter("root")
def update_role():
    req = request.json
    if "id" in req:
        role = {"id": req["id"]}
        RoleTag.query.filter_by(role_id=req["id"]).delete()
        RoleFund.query.filter_by(role_id=req["id"]).delete()

    else:
        role = Role(name=req["name"])
        db.session.add(role)
        db.session.commit()
        role = {"id": role.id}
    if "funds" in req:
        for f in req["funds"]:
            db.session.add(RoleFund(role_id=role["id"], fund_id=f))
    if "tags" in req:
        for t in req["tags"]:
            db.session.add(RoleTag(role_id=role["id"], tag_id=t))
    db.session.commit()
    return {"code": 1, "data": "成功"}


# todo 计算权益值
@main.route('/bf/user/saveUser', methods=['POST'])
def update_user():
    req = request.json
    if "roleName" in req:
        del req["roleName"]
    if "weixin" in req:
        del req["weixin"]
    if "id" in req:
        db.session.query(User).filter(User.id == req["id"]).update(req)
    else:
        db.session.add(User(**req))
    db.session.commit()
    return jsonify({"code": 1, "data": "成功"})


@main.route('/bf/user/getAllUser', methods=['POST'])
@filter("root")
def get_all_user():
    result = []
    for u, r in db.session.query(User, Role).filter(User.role_id == Role.id).all():
        res = to_dict(u)
        res["roleName"] = r.name
        res["weixin"] = 1 if res["openid"] else 0
        result.append(res)
    return {"code": 1, "data": result}


@main.route('/bf/user/getSimpleUser', methods=['POST'])
@filter("root")
def get_simple_user():
    role = Role.query.filter_by(name=request.json["name"]).first()
    return {"code": 1, "data": [{"id": u[0], "name": u[1]} for u in
                                db.session.query(User.id, User.name).filter(User.role_id == role.id).all()]}


# todo 待测
@main.route('/bf/user/deleteTask', methods=['DELETE'])
def delete_task():
    print(request.json)
    return jsonify({"code": 1, "data": ""})


# todo 待测
@main.route('/bf/user/saveTask', methods=['POST'])
def save_task():
    req = request.json
    print(req)
    joiners = req["joiners"]
    del req["joiners"]
    if "id" in req:
        id = req["id"]
        db.session.query(Task).filter(Task.id == req["id"]).update(req)
    else:
        task = Task(**req)
        db.session.add(task)
        db.session.commit()
        id = task.id
    Joiner.query.filter_by(task_id=id).delete()
    for j in joiners:
        db.session.add(Joiner(joiner_id=j["joiner_id"], joiner_name=j["joiner_name"], task_id=id))
    db.session.commit()
    return jsonify({"code": 1, "data": "成功"})


# todo 待测
@main.route('/bf/user/getAllTask', methods=['GET'])
def get_all_task():
    res = to_list(Task.query.order_by(Task.content.desc()).all())
    for r in res:
        r["joiner_names"] = [j.joiner_name for j in Joiner.query.filter_by(task_id=r["id"]).all()]
    return jsonify({"code": 1, "data": res})


@main.after_request
def releaseDB(response):
    db.session.close()
    return response
