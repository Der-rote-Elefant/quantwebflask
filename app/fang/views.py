from . import main
from flask import request, jsonify


@main.route('/cheng/user/login', methods=['GET', "POST"])
def fang():
    print(request.json)
    return jsonify({
        "code": 20000,
        "data": {
            "token": 'admin-token'
        }
    })


@main.route('/cheng/user/info', methods=['GET', "POST"])
def fang1():
    return jsonify({
        "code": 20000,
        "data": {
            "roles": ['admin'],
            "introduction": 'I am a super administrator',
            "avatar": 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
            "name": 'Super Admin'
        }
    })


@main.route('/cheng/user/logout', methods=['GET', "POST"])
def fang2():
    return jsonify({
        "code": 20000,
        "data": 'success'
    })
