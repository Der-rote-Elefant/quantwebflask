# coding: utf-8
from sqlalchemy import Column, Integer, String
from .extensions import db
import pymysql

class User(db.Model):
    __tablename__ = 'user'
    __bind_key__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(45), unique=True)
    password = db.Column(db.String(45))
    tag_count = db.Column(db.Integer)
    role_id = db.Column(db.Integer)
    openid = db.Column(db.String(45))
    name = db.Column(db.String(45))
    avatar = db.Column(db.String(100))


class RoleFund(db.Model):
    __tablename__ = 'role_fund'
    __bind_key__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    role_id = db.Column(db.Integer)
    fund_id = db.Column(db.Integer)


class Role(db.Model):
    __tablename__ = 'role'
    __bind_key__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45))


class RoleTag(db.Model):
    __tablename__ = 'role_tag'
    __bind_key__ = 'users'

    id = Column(db.Integer, primary_key=True)
    role_id = Column(db.Integer)
    tag_id = Column(db.Integer)


class Tag(db.Model):
    __tablename__ = 'tag'
    __bind_key__ = 'users'

    id = Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer)
    name = db.Column(db.String(45))
    name_zh = db.Column(db.String(64))


class Task(db.Model):
    __tablename__ = 'task'
    __bind_key__ = 'users'

    id = Column(db.Integer, primary_key=True)
    author_name = Column(String(64))
    author_id = Column(db.Integer)
    content = Column(db.Text)
    level = Column(db.Integer)
    deadline = Column(db.DateTime)
    progress = Column(db.Integer)
    estime = Column(db.DateTime)
    realTime = Column(db.DateTime)
    delay = Column(db.Integer)
    delayReason = Column(db.Text)


class Joiner(db.Model):
    __tablename__ = 'joiner'
    __bind_key__ = 'users'
    id = Column(db.Integer, primary_key=True)
    joiner_name = Column(db.String(255))
    joiner_id = Column(db.Integer)
    task_id = Column(db.Integer)
