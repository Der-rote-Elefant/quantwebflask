# /usr/bin/python3
# coding=utf-8

import sys
import json
import redis
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import functions as fn
from pyspark.sql.window import Window
import pandas as pd


# JAVA_HOME = "/usr/lib/jvm/default-java"
# PYSPARK_PYTHON = "/usr/bin/python3"

# os.environ["JAVA_HOME"] = JAVA_HOME
# os.environ["PYSPARK_PYTHON"] = PYSPARK_PYTHON
# os.environ["PYSPARK_DRIVER_PYTHON"] = PYSPARK_PYTHON

def get_summary(df):
    indicator_list = list()
    # 净利润
    # "code", "date", "qty", "open_price", "close_date", "close_price", "profit", "application_id"
    df_group = df.groupby().agg(
        fn.sum(df.profit).alias("sum_profit"), fn.max(df.profit).alias("max_profit"),
        fn.min(df.profit).alias("min_profit"), fn.sum(df.qty).alias("sum_qty"),
        fn.count(df.code).alias("sum_count"))

    df_profit_group = df.where(df.profit > 0).groupby().agg(fn.sum(df.profit).alias("sum_profit"),
                                                            fn.max(df.profit).alias("max_profit"),
                                                            fn.min(df.profit).alias("min_profit"),
                                                            fn.sum(df.qty).alias("sum_qty"),
                                                            fn.count(df.code).alias("sum_count"))

    df_loss_group = df.where(df.profit < 0).groupby().agg(fn.sum(df.profit).alias("sum_profit"),
                                                          fn.max(df.profit).alias("max_profit"),
                                                          fn.min(df.profit).alias("min_profit"),
                                                          fn.sum(df.qty).alias("sum_qty"),
                                                          fn.count(df.code).alias("sum_count"))

    all_dict = df_group.first().asDict()
    all_profit_dict = df_profit_group.first().asDict()
    all_loss_dict = df_loss_group.first().asDict()
    net_profit = all_dict["sum_profit"]
    # 总盈利
    sum_profit = all_profit_dict["sum_profit"]
    # 总亏损
    sum_loss = all_loss_dict["sum_profit"]
    # 总盈利/总亏损
    sum_profit_rate = sum_profit / sum_loss

    indicator_list.append({
        "key": "net_profit",
        "name": "净利润",
        "value": round(net_profit, 2)
    })
    indicator_list.append({
        "key": "sum_profit",
        "name": "总盈利",
        "value": round(sum_profit, 2)
    })
    indicator_list.append({
        "key": "sum_loss",
        "name": "总亏损",
        "value": round(sum_loss, 2)
    })
    indicator_list.append({
        "key": "sum_profit_rate",
        "name": "总盈利/总亏损",
        "value": round(sum_profit_rate, 2)
    })

    # 交易次数
    trade_nums = all_dict["sum_count"]
    # 盈利次数
    profit_nums = all_profit_dict["sum_count"]
    # 亏损次数
    loss_nums = all_loss_dict["sum_count"]
    # 持平次数
    equal_nums = trade_nums - profit_nums - loss_nums

    indicator_list.append({
        "key": "trade_nums",
        "name": "交易次数",
        "value": trade_nums
    })
    indicator_list.append({
        "key": "盈利次数",
        "name": "盈利股数",
        "value": profit_nums
    })
    indicator_list.append({
        "key": "loss_nums",
        "name": "亏损次数",
        "value": loss_nums
    })
    indicator_list.append({
        "key": "equal_nums",
        "name": "持平次数",
        "value": equal_nums
    })

    # 平均利润
    avg_all_profit = net_profit / trade_nums
    # 平均盈利
    avg_profit = sum_profit / profit_nums
    # 平均亏损
    avg_loss = sum_loss / loss_nums
    # 平均盈利/平均亏损
    avg_profit_rate = avg_profit / avg_loss

    indicator_list.append({
        "key": "avg_all_profit",
        "name": "平均利润",
        "value": round(avg_all_profit, 2)
    })
    indicator_list.append({
        "key": "avg_profit",
        "name": "平均盈利",
        "value": round(avg_profit, 2)
    })
    indicator_list.append({
        "key": "avg_loss",
        "name": "平均亏损",
        "value": round(avg_loss, 2)
    })
    indicator_list.append({
        "key": "avg_profit_rate",
        "name": "平均盈利/平均亏损",
        "value": round(avg_profit_rate, 2)
    })

    # 最大盈利
    max_profit = all_dict["max_profit"]
    # 最大亏损
    max_loss = all_dict["min_profit"]
    # 最大盈利/总盈利
    max_profit_rate = max_profit / sum_profit
    # 最大亏损/总亏损
    max_loss_rate = max_loss / sum_loss
    # 净利润/最大亏损
    net_profit_loss_rate = net_profit / max_loss

    indicator_list.append({
        "key": "max_profit",
        "name": "最大盈利",
        "value": round(max_profit, 2)
    })
    indicator_list.append({
        "key": "max_loss",
        "name": "最大亏损",
        "value": round(max_loss, 2)
    })
    indicator_list.append({
        "key": "max_profit_rate",
        "name": "最大盈利/总盈利",
        "value": round(max_profit_rate, 2)
    })
    indicator_list.append({
        "key": "max_loss_rate",
        "name": "最大亏损/总亏损",
        "value": round(max_loss_rate, 2)
    })
    indicator_list.append({
        "key": "net_profit_loss_rate",
        "name": "净利润/最大亏损",
        "value": round(net_profit_loss_rate, 2)
    })

    return indicator_list


def run_demo(task_name, task_description, task_type):
    spark_master = "spark://master:7077"  # 指定spark standalone模式运行
    spark_conf = SparkConf().setMaster(spark_master).setAppName(task_name)
    # 设置clickhouse依赖,这些参数也可以在提交任务界面指定
    spark_conf.set("spark.jars.packages", "com.github.housepower:clickhouse-native-jdbc:2.3-stable")
    spark_conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    # 指定计算节点只用的内存大小
    spark_conf.set("spark.driver.memory", "2g")
    spark_conf.set("spark.executor.memory", "8g")
    # 共要用多少个Executor进程来执行
    # spark_conf.set("spark.executor.instances", "18")
    # 每个Executor进程的CPU core数量
    spark_conf.set("spark.executor.cores", "4")
    spark_conf.set("spark.default.parallelism", "150")
    # 指定计算节点使用的最大cpu核数
    spark_conf.set("spark.cores.max", "90")
    # spark_conf.set("spark.dynamicAllocation.enabled", "true")
    # spark_conf.set("spark.shuffle.service.enabled", "true")
    # 启用任务事件记录器，开启后记录到hdfs文件系统中，访问master:18080可以查看历史任务运行情况
    spark_conf.set("spark.eventLog.enabled", "true")
    spark_conf.set("spark.eventLog.dir", "hdfs://master:9000/history-log")
    # spark_conf.set("spark.sql.execution.arrow.enabled", True)
    spark = SparkSession.builder.config(conf=spark_conf).getOrCreate()
    spark.sparkContext.setLogLevel('WARN')
    application_id = spark.sparkContext.applicationId
    # todo save task to mysql
    # cache = redis.StrictRedis(host='192.168.1.126', port=6379, db=0)
    # cache.set("spark_looptest_summary_{0}_{1}".format(task_name,), json.dumps(result))

    clickhouse_driver = "com.github.housepower.jdbc.ClickHouseDriver"
    # 读取clickhouse中日线的数据源，指定查询000001.SK全部历史数据
    df_m5 = spark.read.format("jdbc") \
        .option("driver", clickhouse_driver) \
        .option("url", "jdbc:clickhouse://192.168.1.126:9000/stocks") \
        .option("user", "default") \
        .option("password", "Baofang660") \
        .option("query", "select * from m5_code where code in ({0})".format(looptest_codes)) \
        .load()
    # df_m5_c = df_m5.cache()

    df_day = spark.read.format("jdbc") \
        .option("driver", clickhouse_driver) \
        .option("url", "jdbc:clickhouse://192.168.1.126:9000/stocks") \
        .option("user", "default") \
        .option("password", "Baofang660") \
        .option("query", "select * from day where code in ({0})".format(looptest_codes)) \
        .load()

    # df_day_c = df_day.cache()

    df_day = df_day.withColumn("adj_rate", df_day.adjust_price / df_day.close)
    df_day = df_day.select("code", "date", "adj_rate")
    df_m5 = df_m5.withColumn("day", fn.date_trunc("day", df_m5.date))
    df = df_m5.join(df_day, [df_m5.code == df_day.code, df_m5.day == df_day.date], "inner") \
        .select(df_m5.code, df_m5.date, df_m5.close, (df_m5.close * df_day.adj_rate).alias('adj_close'),
                fn.date_format(df_m5.date, "yyyy-MM").alias("year_month"))

    df.repartition(len(df_code), df.code)  # 重新分区
    df = df.cache()
    # df.show()
    # 计算 close 5日均值
    days_window = Window.partitionBy("code").orderBy("date").rowsBetween(-(48 * 5) + 1, 0)
    # df = df.withColumn("mean_close240", fn.mean("close").over(days_window))
    df = df.withColumn("mean_length", fn.count("*").over(days_window))
    df.show()
    df = df.withColumn("mean_close240",
                       fn.when(df.mean_length >= 240, fn.mean("close").over(days_window)).otherwise(None))

    # 上突破5日均线买入
    df = df.withColumn("up_break", df.close > df.mean_close240)
    df = df.withColumn("down_break", df.close < df.mean_close240)
    shift_window = Window.partitionBy("code").orderBy("date")
    df = df.withColumn("up_break_1",
                       fn.lag(df.up_break, count=1, default=None).over(shift_window))
    df = df.withColumn("is_open", (~df.up_break_1) & df.up_break)
    #
    # 单次交易金额60000
    SINGLE_MONEY = 60000
    # 设置开仓价格
    df = df.withColumn("open_price", fn.when(df.is_open, df.close).otherwise(0))
    # 设置开仓数量
    df = df.withColumn("qty", fn.when(df.is_open, fn.floor(SINGLE_MONEY / df.close / 100) * 100).otherwise(0))
    # 设置平仓日期 ，持仓2日
    df = df.withColumn("close_date", fn.lead(df.date, count=48 * 2, default=None).over(shift_window))
    # 设置平仓价格
    df = df.withColumn("close_price", fn.lead(df.close, count=48 * 2, default=None).over(shift_window))
    # 筛选有成交的日期
    df = df.where(df.is_open).select("code", "date", "close_date", "open_price", "close_price", "qty")
    # 计算收益
    df = df.withColumn("profit", (df["close_price"] - df["open_price"]) * df["qty"])
    df = df.withColumn("application_id", fn.when(df.profit > 0, application_id).otherwise(application_id))
    df = df.dropna()

    df_cached_record = df.cache()

    # 写入clickhouse
    df_record = df.select("code", "date", "qty", "open_price", "close_date", "close_price", "profit", "application_id")
    df_record.write.format("jdbc").mode("append") \
        .option("driver", clickhouse_driver) \
        .option("url", "jdbc:clickhouse://192.168.1.126:9000/stocks") \
        .option("user", "default") \
        .option("password", "Baofang660") \
        .option("isolationLevel", "NONE") \
        .option("numPartitions", "1") \
        .option("dbtable", "spark_looptest") \
        .save()

    # 计算回测任务的指标（暂时只计算一部分）
    all_list = get_summary(df_cached_record)
    long_list = all_list
    result = {
        "allTrades": all_list,
        "longTrades": long_list,
        "short_list": list(),
        "allSummary": list(),
        "assetWithdraw": list(),
        "assetWithdrawRate": list()
    }

    # 保存结果到redis中
    cache = redis.StrictRedis(host='192.168.1.126', port=6379, db=0)
    cache.set("spark_looptest_summary_{0}".format(application_id), json.dumps(result))

    df.unpersist()
    df_cached_record.unpersist()
    spark.sparkContext.stop()


# 1上证50， 2沪深300
df_code = pd.read_csv("http://www.wdl421.cn/api/get_stock_industry.php?industryid=2&day=0",
                      names=["code", "date", "weight"])
looptest_codes = ",".join(["'{0}'".format(c) for c in list(df_code["code"])])

if __name__ == '__main__':
    dict_arg = dict()
    for arg in sys.argv[1:]:
        print('-' * 50, arg, '-' * 50)
        dict_arg[arg.split("=")[0]] = arg.split("=")[1]

    if "task_name" in dict_arg and "task_description" in dict_arg and "task_type" in dict_arg:
        run_demo(dict_arg["task_name"], dict_arg["task_description"], dict_arg["task_type"])
    else:
        run_demo("task_name", "task_description", "looptest")
