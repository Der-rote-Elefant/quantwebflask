import pandas as pd
from . import market
from ..permission import filter_res
from ..models import SparkTask, db, to_list
from ..models import StockInfo, MarketIndicator, DATE_FORMAT
from flask import render_template, jsonify, request
from ..indicator_util import cal_indicator
from dateutil.relativedelta import relativedelta
import numpy as np
import json
from ..db_util import get_all_code
from datetime import datetime


@market.route('/cheng/market/keyboards', methods=['GET'])
@filter_res()
def get_market_keyboards():
    df_res = get_all_code()
    return df_res.to_dict(orient="records")


@market.route('/cheng/market/get_period_type', methods=['GET', 'POST'])
@filter_res()
def get_period_type():
    return [
        {
            "value": "Day",
            "label": "日线",
        },
        {
            "value": "M5",
            "label": "5分钟",
        },
        {
            "value": "M1",
            "label": "1分钟",
        },
    ]


@market.route('/cheng/market/bars', methods=['GET', 'POST'])
@filter_res()
def get_market_bars():
    stock_dicts = request.json["stocks"]
    period = request.json["period"]
    start = request.json["start"][:10]
    end = request.json["end"][:10]
    if end == "":
        end = datetime.now().strftime(DATE_FORMAT)
    is_adj = request.json["is_adj"]
    indicator_list = request.json["indicators"]
    if not end:
        end = pd.datetime.today().strftime(DATE_FORMAT)
    if "forward_times" in request.json:
        forward_times = request.json["forward_times"]
    else:
        forward_times = 1
    res = []
    # bar_columns = ["date", "open", "high", "low", "close", "volume", "amount"]
    bar_columns = ["date", "open", "high", "low", "close", "volume", "is_up"]
    for stock_dict in stock_dicts:
        stock_info = StockInfo(stock_dict)
        min_date, max_date = stock_info.get_date_range()
        if start is None or start == "":
            start, end = stock_info.get_default_date(period, max_date)
        start_date = pd.datetime.strptime(start, DATE_FORMAT)
        end_date = pd.datetime.strptime(end, DATE_FORMAT)
        forward_days = np.floor((end_date - start_date).days * (forward_times - 1))
        forward_start = (start_date - relativedelta(days=forward_days)).strftime(DATE_FORMAT)
        start = min_date if forward_start < min_date else forward_start
        end = max_date if end > max_date else end
        df = stock_info.get_df(period, start, end, is_adj)
        df["is_up"] = (df["close"] > df["open"]).apply(lambda x: 1 if x else -1)
        indicator_data = list()

        # todo 测试用 用于加入买卖平仓点位

        # indicator_data.append({
        #     "name": "测试点",
        #     "GraphType": 0,
        #     "data": df[["date", "close"]].tail(10).to_dict(orient='split')['data']
        # })

        for indicator_dict in indicator_list:
            for category, indicator_values in indicator_dict.items():
                parameters = indicator_values["Parameters"]
                indicator = db.session.query(MarketIndicator).filter(
                    MarketIndicator.Category == category).first()
                if indicator:
                    if "GraphType" in indicator_values and indicator.GraphType != indicator_values["GraphType"]:
                        indicator.GraphType = indicator_values["GraphType"]
                        db.session.commit()
                    indicator_column = cal_indicator(df, indicator, parameters)
                    indicator_display = indicator_column.replace(np.nan, 0)
                    indicator_data.append({
                        "name": indicator_display.name.upper(),
                        "GraphType": indicator.GraphType,
                        "data": indicator_display.values.tolist()
                    })
                else:
                    print("category:{0}指标不存在".format(category))
        res.append({
            "code": stock_info.code,
            "name": stock_info.name,
            "min_date": min_date,
            "max_date": max_date,
            "bars": df.loc[:, bar_columns].values.tolist(),
            "date": df["date"].values.tolist(),
            "indicator": indicator_data,
        })

    return res


@market.route('/cheng/market/indicators', methods=['GET', 'POST', 'PUT', 'DELETE'])
@filter_res()
def market_indicators():
    if request.method == "GET":  # 查询所有指标
        indicators = db.session.query(MarketIndicator).all()
        return to_list(indicators)
    elif request.method == "POST":  # 新增指标
        description = request.json["description"]
        formula = request.json["formula"]
        category = request.json["category"]
        parameters = request.json["parameters"]
        graph_type = int(request.json["graphType"])
        parameter_str = ",".join([str(p) for p in parameters])
        indicator = MarketIndicator(Description=description, Formula=formula,
                                    Parameters=parameter_str, Category=category, GraphType=graph_type)
        db.session.add(indicator)
        db.session.commit()
    elif request.method == "DELETE":  # 修改指标
        indicator_id = request.json["id"]
        db.session.query(MarketIndicator).filter(MarketIndicator.Id == indicator_id).delete()
        db.session.commit()
    elif request.method == "PUT":  # 新增指标
        formula = request.json["formula"]
        category = request.json["category"]
        parameters = request.json["parameters"]
        graph_type = request.json["graphType"]
        parameter_str = ",".join([str(p) for p in parameters])
        indicator_id = request.json["id"]
        indicator = db.session.query(MarketIndicator).filter(MarketIndicator.Id == indicator_id).first()
        indicator.category = category
        indicator.Description = request.json["description"]
        indicator.Formula = formula
        indicator.Parameters = parameter_str
        indicator.GraphType = graph_type
        db.session.add(indicator)
        db.session.commit()
    return "success"


@market.route('/cheng/market/treeMap', methods=['GET', 'POST'])
@filter_res()
def get_data():
    return [{
        "name": "银行",
        # 市值
        "value": "30",
        # 涨幅
        "rate": "1.3%",
        "children": [{
            "name": "工商银行",
            "value": "20",
            "rate": "2.1%",
        }, {
            "name": "建设银行",
            "value": "10",
            "rate": "1.2%",
        }]
    }, {
        "name": "食品饮料",
        # 市值
        "value": "70",
        # 涨幅
        "rate": "4.3%",
        "children": [{
            "name": "贵州茅台",
            "value": "30",
            "rate": "1.1%",
        }, {
            "name": "五粮液",
            "value": "40",
            "rate": "3.1%",
        }]
    }]
