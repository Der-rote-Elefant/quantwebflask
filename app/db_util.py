from .extensions import clickhouse_client
import pandas as pd
from .extensions import db


# todo
def check_table_is_exist(table_name):
    sql = "show tables like '{table_name}'".format(table_name=table_name)
    res = clickhouse_client.engine.execute(sql)
    if len(res) > 0:
        return table_name
    return None


def data_from_source(table_name, sql):
    column_sql = "desc table {table_name}".format(table_name=table_name)
    column = [column[0] for column in clickhouse_client.engine.execute(column_sql)]
    df = pd.DataFrame(clickhouse_client.engine.execute(sql), columns=column)
    return df


def get_all_code():
    # 1 获取股票全市场代码
    res_columns = ["code", "name", "stockType", "isIndex", "indexClass"]
    stock_sql = "select ProductID as code, ProductClass as indexClass, InstrumentName as name from stock"
    df_stock = pd.read_sql(sql=stock_sql, con=db.get_engine(bind="stock"))
    df_stock["stockType"] = "stock"
    df_stock["isIndex"] = df_stock["indexClass"].apply(lambda x: "SKIN" in x)
    df_stock = df_stock.loc[:, res_columns]

    # 2 获取期货全市场代码
    future_sql = "select InstrumentID as code, ProductClass as indexClass, InstrumentName as name from f_instrument"
    df_future = pd.read_sql(sql=future_sql, con=db.get_engine(bind="future"))
    df_future["stockType"] = "future"
    df_future["isIndex"] = df_future["code"].apply(lambda x: x.endswith("000"))
    df_future = df_future.loc[:, res_columns]

    # 3 获取期货999代码
    future_sql = "select distinct ProductID from f_instrument where ProductID!=''"
    df_future999 = pd.read_sql(sql=future_sql, con=db.get_engine(bind="future"))
    df_future999["code"] = df_future999["ProductID"].apply(lambda x: "{0}999".format(x))
    df_future999["name"] = df_future999["code"]
    df_future999["stockType"] = "future"
    df_future999["isIndex"] = False
    df_future999["indexClass"] = "FU"
    df_future999 = df_future999.loc[:, res_columns]
    df_res = pd.concat([df_stock, df_future, df_future999], ignore_index=True, sort=True)
    return df_res
