# -*- coding: UTF-8 -*-
from flask import Flask
from config import config
from app.extensions import db, bootstrap, mongo, sche, cache, compress, clickhouse_client, for_config
import pymysql

app = Flask(__name__)


def app_create(config_name):
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    for_config.set_config(config[config_name])
    # 插件在此添加
    db.init_app(app)
    # with app.test_request_context():
    #     db.create_all()

    bootstrap.init_app(app)
    # mongo.init_app(app)
    sche.init_app(app)
    # todo 定时器
    sche.start()
    cache.init_app(app)
    # cors.init_app(app)
    compress.init_app(app)
    clickhouse_client.init_app(app)

    # 注册蓝本，在此添加
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    from .market import market as market_blueprint
    app.register_blueprint(market_blueprint)
    from .products import main as product_blueprint
    app.register_blueprint(product_blueprint)
    from .fang import main as fang_blueprint
    app.register_blueprint(fang_blueprint)
    from .log import main as log_blueprint
    app.register_blueprint(log_blueprint)
    from .user import main as user_blueprint
    app.register_blueprint(user_blueprint)
    from.loop_test import main as loop_test_blueprint
    app.register_blueprint(loop_test_blueprint)

    # from .api import main as api_blueprint
    # app.register_blueprint(api_blueprint)
    # 路由和其他处理程序定义
    return app
