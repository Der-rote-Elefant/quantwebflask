# -*- coding: UTF-8 -*-
from flask import jsonify, request
from sqlalchemy import and_, or_
import datetime
from . import main
from ..extensions import db, cache
from ..models import *
import math
import requests
from ..util import common
import time
import os
import pandas as pd
import json
from ..util import index_calculator
from ..permission import filter
from ..util import profit_calculator
from dateutil.relativedelta import relativedelta
from ..util.rpc_request import rpc_client
from ..db_util import get_all_code
import re


def secure_filename(filename):
    return str(int(time.time())) + filename


role_type = "backTest"


@main.route('/bf/loop_test/add_tag_looptest', methods=['GET', 'POST'])
@filter("edit")
def add_tag_loop_test():
    dict_data = request.json
    solution = dict_data["Solution"]
    exposure = dict_data["Exposure"]
    expect = dict_data["Expect"]
    category = dict_data["Category"]
    if "Id" in dict_data:
        TagLooptestInfo.query.filter(TagLooptestInfo.Id == dict_data["Id"]).update(dict_data)
        db.session.commit()
        return {
            "code": 1,
            "data": "新增回测方案成功",
        }
    else:
        tag_loop_test = TagLooptestInfo(Solution=solution, Date=datetime.datetime.today(), Exposure=exposure,
                                        Expect=expect, Category=category)
        db.session.add(tag_loop_test)
        db.session.commit()
        if tag_loop_test.Id > 0:
            return {
                "code": 1,
                "data": "新增回测方案成功",
            }
        else:
            return {
                "code": 1,
                "data": "新增回测方案失败",
            }


@main.route('/bf/loop_test/get_tag_looptests', methods=['GET', 'POST'])
@filter(role_type)
def get_tag_loop_tests():
    tag_loop_test_infos = TagLooptestInfo.query.order_by(TagLooptestInfo.Date.desc()).all()
    list_infos = to_list(tag_loop_test_infos)
    return {
        "code": 1,
        "data": list_infos,
    }


LT = {"day": "%Y-%m-%d", "week": "%Y-%W", "month": "%Y-%m", "year": "%Y"}
model_format = "%Y-%m-%d %H:%M:%S"


@main.route('/bf/loop_test/charts', methods=['GET', 'POST'])
@filter(role_type)
def get_loop_test_charts():
    req = request.json
    list_loop_test_id = req["ids"]
    if "type" in req and req["type"] in LT:
        time_format = LT[req["type"]]
    else:
        time_format = "%Y-%m-%d"
    dfs = []
    columns = []
    for loop_test_id in list_loop_test_id:
        ltb = LooptestBalanceTag.query.filter(
            LooptestBalanceTag.TagId == loop_test_id).order_by(LooptestBalanceTag.Date).all()
        if not ltb or len(ltb) == 0:
            continue

        column = "id-" + str(loop_test_id)
        columns.append(column)
        df_new = []
        df = pd.DataFrame(to_list(ltb))
        df = df.loc[:, ["Date", "Profit"]]
        df["Date"] = df["Date"].apply(lambda x: datetime.datetime.strptime(x, model_format).strftime(time_format))
        if time_format != "%Y-%m-%d":
            df["Profit"] = df["Profit"].diff()
        df.ix[0, "Profit"] = 0
        df.columns = ["Date", column]
        for d, g in df.groupby("Date"):
            df_new.append({"Date": d, column: g[column].sum()})
        df_new = pd.DataFrame(df_new)
        df_new.set_index("Date", inplace=True)
        dfs.append(df_new)
    if len(dfs) == 0:
        return {
            "code": 0,
            "msg": "请先上传数据"
        }
    dfs = pd.concat(dfs, axis=1, join='outer')
    dict_result = {}
    dict_result["x"] = list(dfs.index)
    dict_result["y"] = {}
    targets = {}
    for c in columns:
        dfs[c].fillna(0, inplace=True)
        dict_result["y"][c] = list(dfs[c])
        if time_format == "%Y-%m-%d":
            list_datetime = [datetime.datetime.strptime(d, time_format) for d in dict_result["x"]]
            commons = index_calculator.get_index_target(list_datetime, dict_result["y"][c])
            for com in commons:
                if com[0] not in targets:
                    targets[com[0]] = {"name": com[0]}
                targets[com[0]][c] = com[1]
    if time_format == "%Y-%m-%d":
        dict_result["targets"] = list(targets.values())
    return {
        "code": 1,
        "data": dict_result,
    }


@main.route('/bf/loop_test/upload', methods=['GET', 'POST'])
def loop_test_upload():
    if request.method == 'POST':
        file = request.files['file']
        id = request.form.get("id")
        if file and common.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            file.save(f)
            df = pd.read_csv(f, encoding='gbk')
            df = df.loc[0:, ["dates", "yList"]]
            df.columns = ["Date", "Profit"]
            df["Profit"] = df["Profit"].apply(lambda x: (x - 1) * 100)
            # 删除原有数据
            LooptestBalanceTag.query.filter_by(TagId=id).delete()
            for index, row in df.iterrows():
                db.session.add(
                    LooptestBalanceTag(Date=row["Date"], Profit=row["Profit"], TagId=id,
                                       RefreshDate=datetime.datetime.now()))
            db.session.commit()
            # 更新预计收益参数
            df = pd.read_sql('select * from looptest_balance_tag where TagId=%s' % id, con=db.engine).sort_index(
                by=["Date"])
            d_time = list(df["Date"])
            profit = list(df["Profit"])
            sharp = float(index_calculator.calculate_sharp(d_time, profit))
            profit_rate = float(index_calculator.calculate_year_profit_rate(d_time, profit))
            max_draw_down = float(index_calculator.calculate_max_draw_down(d_time, profit))
            text = "%s:%.2f\n%s:%.2f\n%s:%.2f\n" % ("夏普值", sharp, "年化收益%", profit_rate, "最大回撤%", max_draw_down)
            tlt = TagLooptestInfo.query.filter_by(Id=id).first()
            tlt.Expect = text
            db.session.commit()

            return jsonify({"code": 1, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})
    return jsonify({"code": 0, "msg": "请求失败"})


# @main.route('/bf/loop_test/update', methods=['GET', 'POST'])
# def loop_test_update():
#     req = request.json
#     tag = TagLooptestInfo.query.get(req["Id"])
#     tag.Expect = req["Expect"]
#     tag.Exposure = req["Exposure"]
#     tag.Solution = req["Solution"]
#     db.session.commit()
#     return jsonify({
#         "code": 1
#     })


@main.route('/bf/loop_test/delete', methods=['GET', 'POST'])
@filter("edit")
def loop_test_delete():
    list_loop_test_id = request.json["ids"]
    list_result = []
    for loop_test_id in list_loop_test_id:
        try:
            db.session.query(TagLooptestInfo).filter(TagLooptestInfo.Id == loop_test_id).delete()
            db.session.query(LooptestBalanceTag).filter(LooptestBalanceTag.TagId == loop_test_id).delete()
            db.session.commit()
        except Exception as e:
            list_result.append("id:{0}删除失败,失败原因".format(loop_test_id))
        else:
            list_result.append("id:{0}删除成功".format(loop_test_id))

    return {
        "code": 1,
        "data": "删除成功",
    }


@main.route('/bf/loop_test/drawdownChart', methods=['GET', 'POST'])
@filter(role_type)
def loop_test_draw_down_chart():
    list_loop_test_id = request.json["ids"]
    dfs = []
    columns = []
    for loop_test_id in list_loop_test_id:
        lbts = to_list(LooptestBalanceTag.query.filter(
            LooptestBalanceTag.TagId == loop_test_id).order_by(LooptestBalanceTag.Date).all())
        if len(lbts) < 1:
            continue

        column = "id-" + str(loop_test_id)
        columns.append(column)
        df = pd.DataFrame(lbts)
        df = df.loc[:, ["Date", "Profit"]]
        list_day_draw_down = index_calculator.get_day_draw_downs(list(df["Date"]), list(df["Profit"]))
        df["draw_down"] = pd.DataFrame(list_day_draw_down, columns=["draw_down"])["draw_down"]
        df.columns = ["Date", "Profit", column]
        df["Date"] = df["Date"].apply(
            lambda x: datetime.datetime.strptime(x, common.MODEL_FORMAT).strftime(common.LT["day"]))
        df.set_index("Date", inplace=True)
        dfs.append(df)
    if len(dfs) == 0:
        return {
            "code": 0,
            "msg": "数据未上传"
        }
    dfs = pd.concat(dfs, axis=1, join='outer').fillna(0)
    dict_result = dict()
    dict_result["x"] = list(dfs.index)
    dict_result["y"] = dict()
    targets = dict()
    for c in columns:
        dict_result["y"][c] = list(dfs[c])
        list_datetime = [datetime.datetime.strptime(d, common.LT["day"]) for d in dict_result["x"]]
        commons = index_calculator.get_draw_down_index_target(list_datetime, dict_result["y"][c])
        for com in commons:
            if com[0] not in targets:
                targets[com[0]] = {"name": com[0]}
            targets[com[0]][c] = com[1]
        dict_result["targets"] = list(targets.values())
    return {
        "code": 1,
        "data": dict_result,
    }


# ----------------------- 流水回测 -----------------------------------
@main.route('/bf/loop_test/strategy/tradeFlow_upload', methods=['GET', 'POST'])
def loop_test_flow_upload():
    if request.method == 'POST':
        file = request.files['file']
        strategy_id = int(request.form.get("id"))
        if file and common.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            file.save(f)
            dict_type = {"买入": 0, "卖出": 1}
            df = pd.read_csv(f, header=0, encoding='gbk',
                             names=["Code", "Vol", "TradeDate", "Price", "InstrumentType", "Type", "Direction"])
            df["Type"] = df["Type"].apply(lambda x: dict_type[x])
            df.drop(columns=["InstrumentType", ], inplace=True)
            df["TradeDate_Format"] = df["TradeDate"].apply(lambda x: datetime.datetime.strptime(x, "%Y/%m/%d %H:%M:%S"))
            df["TradeDate"] = df["TradeDate"].apply(
                lambda x: datetime.datetime.strptime(x[:10], "%Y/%m/%d"))
            if strategy_id > 0:
                profit_calculator.deal_looptest_flow(strategy_id, df)
                return jsonify({"code": 20000, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})
    return jsonify({"code": 0, "msg": "请求失败"})


# ----------------------- 流水回测 -----------------------------------
@main.route('/bf/loop_test/strategy/tradeFlow_upload_account', methods=['GET', 'POST'])
def loop_test_flow_upload_account():
    if request.method == 'POST':
        strategy_id = -1
        fund_id = -1
        file = request.files['file']
        account_number = int(request.form.get("id"))
        # 是否为调整策略时段标志位，若为调整策略时段，则复制真实产品Balance和Positions覆盖当日回测状态
        adjust_strategy = bool(request.form.get("b_adjust"))
        account = Account.query.filter(
            Account.AccountNumber == account_number).first()
        if account is not None:
            account_id = account.Id
            fund = Fund.query.join(FundAccountRelation, Fund.Id == FundAccountRelation.FundId).filter(
                FundAccountRelation.AccountId == account_id).first()
            if fund is not None:
                fund_id = fund.Id
                strategy = Strategy.query.join(FundStrategyRelation,
                                               Strategy.Id == FundStrategyRelation.StrategyId).filter(
                    and_(FundStrategyRelation.FundId == fund_id, Strategy.Type == 1)).first()
                if strategy is not None:
                    strategy_id = strategy.Id

        if strategy_id < 0:
            return jsonify({"code": 0, "msg": "account_number错误"})

        if file and common.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            f = os.path.join(common.UPLOAD_FOLDER, filename)
            file.save(f)
            dict_type = {"买入": 0, "卖出": 1}
            df = pd.read_csv(f, header=0, encoding='gbk',
                             names=["Code", "Vol", "TradeDate", "Price", "InstrumentType", "Type", "Direction"])
            df["Type"] = df["Type"].apply(lambda x: dict_type[x])
            df.drop(columns=["InstrumentType", ], inplace=True)
            df["TradeDate_Format"] = df["TradeDate"].apply(
                lambda x: datetime.datetime.strptime(x, "%Y/%m/%d %H:%M:%S"))
            df["TradeDate"] = df["TradeDate"].apply(
                lambda x: datetime.datetime.strptime(x[:10], "%Y/%m/%d"))
            if strategy_id > 0:
                if not adjust_strategy:
                    profit_calculator.deal_looptest_flow(strategy_id, df)
                else:
                    # 策略调整，删除旧流水以及收益和持仓信息，读取真实仓位
                    # 后期优化方案: 比较旧策略昨日持仓与真实产品今日持仓差别，模拟生成当日流水(开仓价)进行仓位匹配，生成一笔红冲流水匹配净值状态
                    profit_calculator.deal_adjust_looptest_flow(fund_id, strategy_id, df)
                return jsonify({"code": 20000, "data": "success"})
        else:
            return jsonify({"code": 0, "msg": "文件格式不服"})

    return jsonify({"code": 0, "msg": "请求失败"})


@main.route('/bf/loop_test/strategy/allTable', methods=['GET', 'POST'])
@filter(role_type)
def get_strategy_all_table():
    list_infos = to_list(Strategy.query.all())
    return {"code": 1, "data": list_infos}


@main.route('/bf/loop_test/strategy/update', methods=['POST'])
@filter(role_type)
def update_info():
    u_type = request.json["type"]
    u_data = request.json["data"]
    if u_type == "add":
        db.session.add(Strategy(**u_data))
    elif u_type == "update":
        strategy = Strategy.query.get(u_data["Id"])
        strategy.Tag = u_data["Tag"]
        strategy.Describe = u_data["Describe"]
        strategy.Type = u_data["Type"]
        strategy.InitialFund = u_data["InitialFund"]
        strategy.BaseCode = u_data["BaseCode"]
    elif u_type == "delete":
        for u in u_data:
            if Strategy.query.get(u) == None:
                continue
            StrategyBalance.query.filter_by(StrategyId=u).delete()
            StrategyFlow.query.filter_by(StrategyId=u).delete()
            StrategyPosition.query.filter_by(StrategyId=u).delete()
            Strategy.query.filter_by(Id=u).delete()
    else:
        return {"code": 0, "msg": "类型不服"}
    db.session.commit()
    return {"code": 1, "data": "success"}


@main.route('/bf/loop_test/strategy/recalculator', methods=['GET'])
def recalculator():
    strategy_id = request.args.get("strategyId")
    profit_calculator.recalculator_strategy(strategy_id)
    return jsonify({"code": 1, "data": "success"})


@main.route('/bf/loop_test/strategy/recalculator_recent_day', methods=['GET'])
def recalculator_recent_day():
    strategy_id = request.args.get("strategyId")
    profit_calculator.recalculator_strategy_recent_day(strategy_id)
    return jsonify({"code": 1, "data": "success"})


@main.route('/bf/loop_test/strategy/bindFund', methods=['GET'])
@filter(role_type)
def get_bind_fund():
    fund_id = request.args.get("fundId")
    fsr = FundStrategyRelation.query.filter_by(FundId=fund_id).all()
    return {"code": 1, "data": [i.StrategyId for i in fsr]}


@main.route('/bf/loop_test/strategy/bindFund', methods=['post'])
@filter(role_type)
def bind_fund():
    fund_id = request.json["fundId"]
    strategy_ids = request.json["strategyIds"]
    FundStrategyRelation.query.filter_by(FundId=fund_id).delete()
    for si in strategy_ids:
        db.session.add(FundStrategyRelation(StrategyId=si, FundId=fund_id))
    db.session.commit()
    return {"code": 1, "data": "success"}


@main.route('/bf/loop_test/strategy/getTradeDate', methods=['GET'])
@filter(role_type)
def get_trade_date():
    StrategyId = request.args.get("StrategyId")
    sql = "select TradeDate from strategy_flow where StrategyId=%s group by TradeDate" % StrategyId
    res_sql = db.session.execute(sql).fetchall()
    res_list = list(set([str(r[0])[:10] for r in res_sql]))
    res_list.sort(reverse=True)
    return {"code": 1, "data": [{"TradeDate": r} for r in res_list]}


@main.route('/bf/loop_test/strategy/getByStrategyId', methods=['GET'])
@filter(role_type)
def get_date_by_id():
    id = request.args.get("Id")
    trade_date = request.args.get("TradeDate")
    strategy_flow = StrategyFlow.query.filter(
        and_(StrategyFlow.StrategyId == id, StrategyFlow.TradeDate.like(trade_date + '%'))).all()
    return {"code": 1, "data": to_list(strategy_flow)}


@main.route('/bf/loop_test/saveStrategyFixData', methods=['POST'])
@filter(role_type)
def save_strategy_fix_data():
    # 修改回测流水
    req = request.json
    req_type = req["type"]
    row = req["row"]
    tf = StrategyFlow(**row)
    strategy_id = int(row["StrategyId"])
    tf.StrategyId = strategy_id
    profit_calculator.update_one_strategy_trade_flow(strategy_id, tf, req_type)
    return {"code": 1, "data": "success"}


@main.route('/bf/loop_test/getStrategyData', methods=['POST'])
def getStrategyData():
    req = request.json
    res_df = pd.DataFrame(columns=["Date"])
    res_json = {}
    for id in req["Ids"]:
        id = str(id)
        df = pd.read_sql("select Date,Profit from strategy_balance where StrategyId=%s" % id, con=db.engine)
        df.rename(columns={"Profit": id}, inplace=True)
        res_df = pd.merge(res_df, df, on="Date", how="left" if len(res_df) > len(df) else "right")
    res_df["Date"] = res_df["Date"].apply(lambda x: x.strftime('%Y-%m-%d'))
    res_json["x"] = list(res_df["Date"])
    res_json_y = {}
    for id in req["Ids"]:
        id = str(id)
        res_df[id].fillna(0, inplace=True)
        res_df[id] = round(res_df[id].cumsum(), 3)
        res_json_y[id] = list(res_df[id])
    res_json["y"] = res_json_y
    return jsonify({"code": 1, "data": res_json})


@main.route('/bf/loop_test/testtest', methods=['POST'])
def test_fang():
    return ""


@main.after_request
def releaseDB(response):
    db.session.close()
    return response


@main.route('/bf/looptest/pools', methods=['GET'])
def get_pools():
    # 获取参数 最近N日
    day = int(request.args.get("day"))
    n_day = (pd.datetime.today() - relativedelta(days=day)).strftime("%Y-%m-%d")

    # 获取票池列表
    df_code = get_all_code()
    df_code["fullCode"] = df_code["code"] + "." + df_code["indexClass"]
    pool_list = list()
    pool_keys = [key.decode() for key in cache.keys("pool.desc_*") if len(key.decode().split("_")) == 2]
    for pool_key in pool_keys:
        pool_res = dict()
        pool_name = pool_key[len("pool.desc_"):]
        pool_res["poolName"] = pool_name
        pool_res["poolDesc"] = cache.get(pool_key).decode()
        record_keys = cache.keys("pool.records_{0}*".format(pool_name))
        record_list = list()
        for key in record_keys:
            records = json.loads(cache.get(key))
            record_list.extend(records)
        df_pool = pd.DataFrame(record_list)
        df_pool = pd.merge(left=df_pool, right=df_code, left_on="Code", right_on="fullCode", how="left")
        df_pool["Date"] = df_pool["Date"].apply(lambda x: pd.datetime.strptime(x, "%Y-%m-%dT%H:%M:%S"))
        df_pool.sort_values(by=["Date", "Code"], ascending=True, inplace=True)
        df_pool["Day"] = df_pool["Date"].apply(lambda x: x.strftime("%Y-%m-%d"))
        pool_res["poolIn"] = df_pool.loc[(df_pool["Day"] >= n_day) & (df_pool["Direction"] == 1)].to_dict(
            orient="records")
        pool_res["poolOut"] = df_pool.loc[(df_pool["Day"] >= n_day) & (df_pool["Direction"] == -1)].to_dict(
            orient="records")
        df = df_pool.groupby(by=["Code"]).tail(1)
        df = df.loc[df["Direction"] == 1, :]
        pool_res["pool"] = df.to_dict(orient="records")
        pool_res["lastUpdateDate"] = df_pool["Day"].max()
        pool_list.append(pool_res)
    return jsonify({"code": 1, "data": pool_list})


def get_file_content(file):
    if file and file.filename.endswith(".cs"):
        # 接收上传的策略文件读取文本内容
        file_name = "./{0}_{1}".format(file.filename, time.time())
        file.save(file_name)
        with open(file_name, mode="r", encoding="UTF-8-sig") as f:
            lines = f.readlines()
            content = "".join(lines)
        os.remove(file_name)
        return content
    return None


@main.route('/bf/looptest/strategyParams', methods=['POST', 'GET'])
def looptest_strategy_params():
    # 正则表达式匹配策略参数
    file = request.files['file']
    content = get_file_content(file)
    if content:
        params_list = list()
        match_list = re.findall(r"new StrategyConfigItem\s*\{.*?\};", content, re.S)
        for st in match_list:
            name = re.search("(Name = \")(.*)(\")", st)[2]
            key = re.search("(Key = \")(.*)(\")", st)[2]
            value = re.search("(Value = \")(.*)(\")", st)[2]
            params_list.append({"name": name, "key": key, "value": value})
        return jsonify({"code": 1, "data": {"paramsList": params_list, "content": content}})
    else:
        return jsonify({"code": -1, "data": "文件格式不正确"})


@main.route('/bf/looptest/rpc_remote', methods=['POST', 'GET'])
def looptest_rpc_remote():
    """
    远程RPC调用回测
    :return:
    """
    method_name = "RunLoopTestMulti"  # RPC远程命令名
    file = request.files['file']
    strategy_name = "{0}_{1}".format(request.form["strategyName"], pd.datetime.now().strftime(DATETIME_FORMAT))
    content = get_file_content(file)
    if content:
        res = list()
        # 读取请求包含的策略参数
        params_list = json.loads(request.form["params"])
        # 正则表达式替换策略参数
        res_str = "\n".join(["""dictStrategyConfigItem["{0}"] = new StrategyConfigItem
                    {{
                        Name = "{1}",
                        Key = "{0}",
                        Value = "{2}"
                    }};""".format(dic["key"], dic["name"], dic["value"]) for dic in params_list])
        new_content = re.sub(r"(.*private void InitDictParameter\(\)\s+{\s+)([\s\S]*};)(public void Init.*)",
                             r"\1%s\3" % res_str, content)
        codes = request.form['codes']
        print(codes)
        addresses = request.form['ips']
        code_list = [code for code in codes.split(",") if len(code) > 0]
        address_list = [{"ip": address.split(':')[0], "port": int(address.split(':')[1])} for address in
                        addresses.split(',') if
                        len(address.split(':')) == 2]
        # 计算每个节点需要回测的票数
        single_code_count = math.ceil(len(code_list) / len(address_list))
        current = 0  # 当前回测节点数
        while True:
            if len(address_list) > current:
                current_address = address_list[current]
                current_codes = code_list[current * single_code_count:(current + 1) * single_code_count]
                request_dict = {
                    "Content": new_content,
                    "Codes": ",".join(current_codes),
                    "StrategyUniqueName": strategy_name,
                }
                # 发送rpc回测请求，如果异常保留异常结果
                try:
                    rpc_client.request(current_address["ip"], current_address["port"], method_name, request_dict)
                    res.append("{0}:{1}请求成功".format(current_address["ip"], current_address["port"]))
                except Exception as e:
                    res.append(
                        "{0}:{1}请求失败，请检查".format(current_address["ip"], current_address["port"]))
                current += 1
                if len(current_codes) < single_code_count:
                    break
            else:
                break
        # 记录回测节点数到redis
        cache.set("looptest.count_{0}".format(strategy_name), current)
        return jsonify({"code": 1, "data": "\t".join(res)})
    else:
        return jsonify({"code": -1, "data": "文件格式不正确"})


@main.route('/bf/looptest/records', methods=['POST', 'GET'])
def looptest_records():
    """
    获取历史回测结果列表
    :return:
    """
    remote_list = list()
    # 读取历史回测记录，值为回测节点数
    keys = [key.decode() for key in cache.keys("looptest.count_*")]
    for key in keys:
        all_count = int(cache.get(key))  # 回测节点数
        strategy_name = key[len("looptest.count_"):]  # 回测名称
        complete_count = len(cache.keys("looptest.detail_{0}_*".format(strategy_name)))  # 已完成节点数
        has_report = len(cache.keys("looptest.report_{0}".format(strategy_name))) > 0  # 是否合并生成报表
        remote_list.append({
            "strategyName": strategy_name,
            "nodeCount": all_count,
            "completeCount": complete_count,
            "hasReport": has_report,
            "reportType": "remote"
        })
    keys = [key.decode() for key in cache.keys("looptest.report_local_*")]
    for key in keys:
        strategy_name = key[len("looptest.report_local_"):]  # 回测名称
        remote_list.append({
            "strategyName": strategy_name,
            "nodeCount": 1,
            "completeCount": 1,
            "hasReport": True,
            "reportType": "local"
        })
    return jsonify({"code": 1, "data": remote_list})


@main.route('/bf/looptest/remove_report', methods=['POST', 'GET'])
def looptest_remove_report():
    """
    删除指定回测结果
    :return:
    """
    strategy_name = request.args.get("strategy")
    report_type = request.args.get("type")
    if strategy_name and report_type:
        if report_type == "remote":
            remove_list = ["looptest.count_{0}", "looptest.detail_{0}_*", "looptest.report_{0}"]
        else:
            remove_list = ["looptest.report_local_{0}"]
        for remove_key in remove_list:
            keys = [key.decode() for key in cache.keys(remove_key.format(strategy_name))]
            for key in keys:
                cache.delete(key)
    else:
        return jsonify({"code": -1, "data": "找不到策略名字"})

    return jsonify({"code": 1, "data": "删除成功"})


@main.route('/bf/looptest/report_json', methods=['POST', 'GET'])
def looptest_get_report_json():
    strategy_name = request.args.get("strategyName")
    report_type = request.args.get("reportType")
    if strategy_name and report_type:
        key_starts = "looptest.report_" if report_type == "remote" else "looptest.report_local_"
        res = json.loads(cache.get("{0}{1}".format(key_starts, strategy_name)))
        return jsonify({"code": 1, "data": res})
    return jsonify({"code": -1, "data": "请求参数不正确"})
