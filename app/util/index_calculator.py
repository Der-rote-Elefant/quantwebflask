import numpy as np
import pandas as pd
import math
from ..util import common


def get_index_target(list_date_time, day_profits):
    """
    计算全部指标
    :param list_date_time:
    :param day_profits:
    :return:
    """
    dict_function = {
        "年化收益%": calculate_year_profit_rate,
        "R方值": calculate_rsq,
        "夏普值": calculate_sharp,
        "周胜率%": calculate_week_win_rate,
        "周连续亏损": calculate_constant_loss_week,
        "周连续盈利": calculate_constant_win_week,
        "月胜率%": calculate_month_win_rate,
        "月连续亏损": calculate_constant_month_loss_week,
        "月连续盈利": calculate_constant_month_win_week,
        "最大回撤%": calculate_max_draw_down,
        "平均回撤%": calculate_mean_drawn_down,
        # "交易效率": calculate_trade_efficient
    }
    target = list()
    for key, func in dict_function.items():
        target.append((key, func(list_date_time, day_profits)))
    return target


def get_common_index_target(list_date_time, day_profits):
    """
    计算普通指标
    :param list_date_time:
    :param day_profits:
    :return:
    """
    dict_function = {
        "年化收益%": calculate_year_profit_rate,
        "R方值": calculate_rsq,
        "夏普值": calculate_sharp,
        "周胜率%": calculate_week_win_rate,
        "周连续亏损": calculate_constant_loss_week,
        "周连续盈利": calculate_constant_win_week,
        "月胜率%": calculate_month_win_rate,
        "月连续亏损": calculate_constant_month_loss_week,
        "月连续盈利": calculate_constant_month_win_week,
        # "交易效率": calculate_trade_efficient
    }
    target = list()
    for key, func in dict_function.items():
        target.append((key, func(list_date_time, day_profits)))
    return target


def get_draw_down_index_target(list_date_time, day_profits):
    """
    计算回撤指标
    :param list_date_time:
    :param day_profits:
    :return:
    """
    dict_function = {
        "最大回撤%": calculate_max_draw_down,
        "平均回撤%": calculate_mean_drawn_down,
    }

    target = list()
    for key, func in dict_function.items():
        target.append((key, func(list_date_time, day_profits)))
    return target


def calculate_year_profit_rate(list_date_time, day_profits):
    """
    计算年化收益率
    :param list_date_time:日期列表
    :param day_profits:收益列表
    :return:
    """
    profit_rate = 0
    if len(list_date_time) > 1:
        days = (list_date_time[-1] - list_date_time[0]).days
        profit_rate = (day_profits[-1] - day_profits[0]) / days * 365
    return round(profit_rate, 2)


def regression(y):
    """
    一元线性回归
    :param y:
    :return:
    """
    result = list()
    if len(y) > 0:
        sum_x = 0
        sum_y = 0
        sum_x_x = 0
        sum_x_y = 0
        for i in range(len(y)):
            sum_x = sum_x + i
            sum_y = sum_y + y[i]
            sum_x_y = sum_x_y + i * y[i]
            sum_x_x = sum_x_x + i * i

        mean_x = sum_x / len(y)
        mean_y = sum_y / len(y)

        k = 0 if (sum_x_x - mean_x * sum_x) == 0 else (sum_x_y - mean_y * sum_x) / (sum_x_x - mean_x * sum_x)
        b = mean_y - k * mean_x
        for i in range(len(y)):
            result.append(i * k + b)
    return result


def calculate_rsq(list_date_time, day_profits):
    """
    计算R平方值
    :param day_profits:
    :return:
    """
    regression_profits = regression(day_profits)
    mean_x = np.mean(regression_profits)
    mean_y = np.mean(day_profits)
    ssr = 0
    x_square = 0
    y_square = 0
    for i in range(0, len(regression_profits)):
        differ_x = regression_profits[i] - mean_x
        differ_y = day_profits[i] - mean_y
        ssr = ssr + (differ_x * differ_y)
        x_square = x_square + differ_x ** 2
        y_square = y_square + differ_y ** 2

    sst = math.sqrt(x_square * y_square)
    result = 0 if sst == 0 else round(ssr / sst, 2)
    return result


def calculate_sharp(list_date_time, day_profits):
    """
    计算夏普比率
    :param day_profits:
    :return:
    """
    df = pd.DataFrame(day_profits, columns=["Profit"])
    df["Profit"] = df["Profit"].diff()
    if len(df["Profit"] > 0):
        df.ix[0, "Profit"] = 0
    profit_percent = list(df["Profit"])
    avg = np.average(profit_percent)
    std = np.std(profit_percent)
    sharp = 0 if std == 0 or np.isnan(std) else avg / std * math.sqrt(252)
    # 对于不同采样频率的k值情况：
    # Daily = sqrt(252)（最小粒度是按天计）
    # Weekly = sqrt(52)（最小粒度是按星期计）
    # Monthly = sqrt(12)（最小粒度是按月计）
    return round(sharp, 2)


def calculate_max_draw_down(list_date_time, profit_rates):
    """
    计算最大回撤
    :param profit_rates:
    :return:
    """
    max_draw_down = 0
    if len(profit_rates) > 0:
        max_draw_down = get_day_draw_downs(list_date_time, profit_rates)
    return round(np.max(max_draw_down), 2)


def calculate_mean_drawn_down(list_date_time, profit_rates):
    """
    计算平均回撤
    :param profit_rates:
    :return:
    """
    list_drawn_down = get_day_draw_downs(list_date_time, profit_rates)
    list_drawn_down = list(set(list_drawn_down))
    list_drawn_down.sort(reverse=True)
    result = 0 if len(list_drawn_down) < 6 else np.mean(list_drawn_down[1:6])
    return round(result, 2)


def get_week_profits(list_date_time, day_profits):
    """
    获取每周收益
    :param list_date_time:
    :param day_profits:
    :return:
    """
    df = pd.DataFrame()
    df["Date"] = pd.DataFrame(list_date_time, columns=["Date"])["Date"]
    df["Profit"] = pd.DataFrame(day_profits, columns=["Profit"])["Profit"]
    df["Date"] = df["Date"].apply(lambda x: x.strftime(common.LT["week"]))
    df["Profit"] = df["Profit"].diff()
    if len(df["Profit"]) > 0:
        df.ix[0, "Profit"] = 0
    # 每周收益
    list_week_profit = list()
    for d, g in df.groupby("Date"):
        list_week_profit.append(g["Profit"].sum())
    return list_week_profit


def get_day_draw_downs(list_date_time, day_profits):
    """
    获取每天回撤
    :param list_date_time:
    :param day_profits:
    :return:
    """
    df = pd.DataFrame()
    df["Date"] = pd.DataFrame(list_date_time, columns=["Date"])["Date"]
    df["Profit"] = pd.DataFrame(day_profits, columns=["Profit"])["Profit"]
    df["MaxProfit"] = df["Profit"].expanding().max()
    df["DrawDown"] = df["MaxProfit"] - df["Profit"]
    return list(df["DrawDown"])


def get_month_profits(list_date_time, day_profits):
    """
    获取每月收益
    :param list_date_time:
    :param day_profits:
    :return:
    """
    df = pd.DataFrame()
    df["Date"] = pd.DataFrame(list_date_time, columns=["Date"])["Date"]
    df["Profit"] = pd.DataFrame(day_profits, columns=["Profit"])["Profit"]
    df["Date"] = df["Date"].apply(lambda x: x.strftime(common.LT["month"]))
    df["Profit"] = df["Profit"].diff()
    if len(df["Profit"]) > 0:
        df.ix[0, "Profit"] = 0
    # 每周收益
    list_month_profit = list()
    for d, g in df.groupby("Date"):
        list_month_profit.append(g["Profit"].sum())
    return list_month_profit


def calculate_week_win_rate(list_date_time, day_profits):
    """
    计算周胜率
    :param list_date_time:
    :param day_profits:
    :return:
    """
    list_week_profit = get_week_profits(list_date_time, day_profits)
    week_array = np.array(list_week_profit)
    win_array = week_array[np.where(week_array > 0)]
    result = 0
    if len(list_week_profit) > 0:
        result = float(len(win_array)) / len(list_week_profit) * 100
    return round(result, 2)


def calculate_month_win_rate(list_date_time, day_profits):
    """
    计算月胜率
    :param list_date_time:
    :param day_profits:
    :return:
    """
    list_month_profit = get_month_profits(list_date_time, day_profits)
    month_array = np.array(list_month_profit)
    win_array = month_array[np.where(month_array > 0)]
    result = 0
    if len(list_month_profit) > 0:
        result = float(len(win_array)) / len(list_month_profit) * 100
    return round(result, 2)


def calculate_constant_loss_week(list_date_time, day_profits):
    """
    计算连续亏损周数
    :param list_date_time:
    :param day_profits:
    :return:
    """
    list_week_profit = get_week_profits(list_date_time, day_profits)
    list_loss_count = list()
    count = 0
    for profit in list_week_profit:
        if profit <= 0:
            count = count + 1
        else:
            list_loss_count.append(count)
            count = 0

    if count > 0:
        list_loss_count.append(count)
    result = 0 if len(list_loss_count) <= 0 else float(np.max(list_loss_count))
    return round(result, 2)


def calculate_constant_win_week(list_date_time, day_profits):
    """
    计算连续盈利周数
    :param list_date_time:
    :param day_profits:
    :return:
    """
    list_week_profit = get_week_profits(list_date_time, day_profits)
    list_win_count = list()
    count = 0
    for profit in list_week_profit:
        if profit >= 0:
            count = count + 1
        else:
            list_win_count.append(count)
            count = 0

    if count > 0:
        list_win_count.append(count)
    result = 0 if len(list_win_count) <= 0 else float(np.max(list_win_count))
    return round(result, 2)


def calculate_constant_month_loss_week(list_date_time, day_profits):
    """
    计算连续亏损月数
    :param list_date_time:
    :param day_profits:
    :return:
    """
    list_month_profit = get_month_profits(list_date_time, day_profits)
    list_loss_count = list()
    count = 0
    for profit in list_month_profit:
        if profit <= 0:
            count = count + 1
        else:
            list_loss_count.append(count)
            count = 0

    if count > 0:
        list_loss_count.append(count)
    result = 0 if len(list_loss_count) <= 0 else float(np.max(list_loss_count))
    return round(result, 2)


def calculate_constant_month_win_week(list_date_time, day_profits):
    """
    计算连续盈利月数
    :param list_date_time:
    :param day_profits:
    :return:
    """
    list_month_profit = get_month_profits(list_date_time, day_profits)
    list_win_count = list()
    count = 0
    for profit in list_month_profit:
        if profit >= 0:
            count = count + 1
        else:
            list_win_count.append(count)
            count = 0

    if count > 0:
        list_win_count.append(count)
    result = 0 if len(list_win_count) <= 0 else float(np.max(list_win_count))
    return round(result, 2)


def calculate_trade_efficient(list_date_time, day_profits):
    """计算交易效率"""
    return round(float(0), 2)
