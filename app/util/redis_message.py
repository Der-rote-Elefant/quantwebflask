from ..extensions import cache

GLOBAL_CHANNEL = "web.message.center"


def start_pub_sub():
    print("*" * 50)
    message_center = cache.provider_class.pubsub()
    message_center.subscribe(GLOBAL_CHANNEL)
    print("subscribe")
    for message in message_center.listen():
        print(message)
        yield 'data: %s\n\n' % message['data']
