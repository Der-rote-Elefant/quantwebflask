import pandas as pd
import numpy as np
stock_path = "./instrument/stocks.csv"


def start(stock_path):
    df = pd.read_csv(stock_path, dtype={"ProductID": np.str}).loc[:,
         ["ProductID", "ProductClass", "InstrumentName", "ExchangeID"]]
    df = df.set_index("ProductID")
    return df


stock_map = start(stock_path)
