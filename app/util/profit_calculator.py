from ..models import *
import requests
from ..util import common
from sqlalchemy import and_, or_
import pandas as pd
import datetime
from ..util.log_util import logger
import uuid
from ..extensions import cache
import pymysql

dict_future_type = {
    "IC": 200,
    "IF": 300,
    "IH": 300
}

dict_order_type = {
    "Open": 0,
    "Close": 1
}

dict_direction_type = {
    "Short": 0,
    "Long": 1
}

error_cache_key = "error2wechat-"


def save_error2db(error_msg):
    logger.error(error_msg)
    key = error_cache_key + str(uuid.uuid1())
    cache.set(key, error_msg)


class TempPositionProfit:
    # 方向,昨收,昨持仓,当前价格,当前持仓,持仓收益,开仓收益,平仓收益,当日总收益
    def __init__(self, code, direction, open_price=0, last_close=0, last_vol=0, now_price=0, now_vol_change=0,
                 hold_profit=0, open_profit=0,
                 close_profit=0):
        self.code = code
        self.direction = direction
        self.open_price = open_price
        self.last_close = last_close
        self.last_vol = last_vol
        self.now_price = now_price
        self.now_vol_change = now_vol_change
        self.hold_profit = hold_profit
        self.open_profit = open_profit
        self.close_profit = close_profit
        self.sum_profit = self.__get_sum_profit
        self.hold_position_for_open = 0  # 记录在开仓流水中已经计算过持仓收益的仓位数
        self.hold_position_for_close = 0  # 记录在平仓流水中已经计算过持仓收益的仓位数
        self.code_price = 1
        if "IC" in self.code:
            self.code_price = dict_future_type["IC"]
        elif "IF" in self.code:
            self.code_price = dict_future_type["IF"]
        elif "IH" in self.code:
            self.code_price = dict_future_type["IH"]

    def __get_sum_profit(self):
        """
        当日盈亏
        :return:
        """
        return self.hold_profit + self.open_profit + self.close_profit

    def __get_sum_profit_ratio(self):
        """
        当日盈亏比 %
        :return:
        """
        profit = self.hold_profit + self.open_profit + self.close_profit
        cost = self.open_price * (
            self.last_vol if self.now_vol_change <= 0 else (self.last_vol + self.now_vol_change)) * self.code_price
        ratio = profit / cost * 100 if cost != 0 else 0
        return ratio

    def __get_day_marketvalue(self):
        """
        持仓市值
        """
        return self.now_price * (self.last_vol + self.now_vol_change) * self.code_price

    def __get_position_profit(self):
        """
        持仓盈亏
        """
        return (self.now_price - self.open_price) * (self.last_vol + self.now_vol_change) * self.code_price * (
            1 if self.direction == 1 else -1)

    def __get_position_profit_ratio(self):
        return 0 if (self.last_vol + self.now_vol_change <= 0) else \
            (self.now_price - self.open_price) / self.open_price * 100 * (1 if self.direction == 1 else -1)

    def to_dict(self):
        try:
            profit_dict = {}
            profit_dict["Code"] = self.code
            profit_dict["Direction"] = self.direction
            profit_dict["OpenPrice"] = self.open_price
            profit_dict["LastVol"] = self.last_vol
            profit_dict["NowVol"] = self.last_vol + self.now_vol_change
            profit_dict["NowPrice"] = self.now_price
            profit_dict["LastPrice"] = self.last_close
            profit_dict["DaySumProfit"] = round(self.__get_sum_profit(), 2)
            profit_dict["DaySumProfitRatio"] = round(self.__get_sum_profit_ratio(), 2)
            profit_dict["MarketValue"] = round(self.__get_day_marketvalue(), 2)
            profit_dict["PositionProfit"] = round(self.__get_position_profit(), 2)
            profit_dict["PositionProfitRatio"] = round(self.__get_position_profit_ratio(), 2)
            return profit_dict
        except Exception as e:
            print(self)
            print(e)




def get_real_close(code):
    if ("SK" in code) or ("ETF" in code):
        close = StockDayM1.query.with_entities(StockDayM1.Close).filter(StockDayM1.Code == code).first()
        price = 0 if close is None else float(close[0])
    else:
        tick = FutureTick.query.with_entities(FutureTick.Price).filter(
            FutureTick.InstrumentID == code.split('.')[0]).first()
        price = 0 if tick is None else float(tick[0])
    if price == 0:
        print(code, price, datetime.datetime.now(), code, "实时价格获取失败,检查是否当日停牌")
        price = get_history_close(code, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    return price


def get_history_close(code, day):
    if code.endswith(".SK"):
        instrument_type = 1
    elif code.endswith(".SKIN"):
        instrument_type = 2
    elif code.endswith(".FU"):
        instrument_type = 3
    elif code.endswith(".ETF"):
        instrument_type = 10

    day = datetime.datetime.strptime(day, common.MODEL_FORMAT)
    url = "http://www.wdl421.cn/api/dayapi.php?code={0}&type={1}&starttime={2}&cycle={3}&endtime={4}".format(
        code.split('.')[0], instrument_type, day.strftime('%Y-%m-%d'), "M5" if code.endswith(".FU") else "Day",
        day.strftime('%Y-%m-%d'))
    session = requests.session()
    session.keep_alive = False
    index_lines = session.get(url)
    index_lines.encoding = "UTF-8-SIG"
    lines = index_lines.text.splitlines()
    close = 0 if len(lines) <= 0 else float(lines[-1].split(',')[4])
    if close == 0 and instrument_type != 3:
        print(code, close, day, "获取价格失败，重新取最近价格")
        url = "http://www.wdl421.cn/api/dayapiforsuspendedstock.php?code={0}&day={1}".format(
            code, day.strftime('%Y-%m-%d'))
        session = requests.session()
        session.keep_alive = False
        index_lines = session.get(url)
        index_lines.encoding = "UTF-8-SIG"
        lines = index_lines.text.splitlines()
        if len(lines) <= 0:
            close = 0
            print(code, close, day, "重新获取最近价格失败！")
        else:
            close = float(lines[-1].split(',')[4])
    return close


def get_adjust_info(date):
    # db_adjust_conn = pymysql.connect(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com", user="stock",
    #                                  password="bfstock330", database="stock", charset="utf8")

    sql_cmd = "select * from xr where Date = '{0}'".format(date)
    df = pd.read_sql(sql=sql_cmd, con=db.get_engine(bind="adj"))
    # print(df.head())
    return df


def get_market_value(position_list, last_position_list=None, df_adjust=None, ask_last=True):
    long_market_value = 0
    short_market_value = 0
    adjust_cash_value = 0
    for position in position_list:
        if ask_last is True:
            close = get_real_close(position.Code)
        else:
            close = get_history_close(position.Code, position.Date)

        if position.Direction == 1:

            br = 0  # 分红比例
            v0 = position.Vol
            v1 = position.Vol

            if (df_adjust is not None) and len(df_adjust[df_adjust["Code"] == position.Code]) > 0:
                adjust_row = df_adjust[df_adjust["Code"] == position.Code].iloc[0]
                br = adjust_row["BonusRatioRmb"] / 10

                for last_position in last_position_list:
                    if last_position.Code == position.Code:
                        v1 = last_position.Vol

            long_market_value += close * v0

            code_cash_value = br * v1
            adjust_cash_value += code_cash_value

            if code_cash_value > 0:
                logger.error("{0},基金ID：{1},股票{2},昨日持有{3}股,分红现金{4}".format(position.Date, position.FundId, position.Code,
                                                                          position.Vol, code_cash_value))
        else:
            future_type = position.Code[:2]
            future_price = dict_future_type[future_type]
            short_market_value += close * position.Vol * future_price
    return long_market_value, short_market_value, adjust_cash_value


def get_short_market_cost(position_list):
    cost = 0
    for position in position_list:
        if position.Direction == 0:
            future_type = position.Code[:2]
            future_price = dict_future_type[future_type]
            cost += position.Price * position.Vol * future_price
    return cost


def get_positions_by_trade_flow(fund_id, flow_day, last_position_list, df_trade_flow, df_adjust):
    # 根据流水，更新持仓信息
    position_list = list()

    day_position_dic = dict()
    for position in last_position_list:
        key = "{0}_{1}".format(position.Code, position.Direction)
        adjust_vol = position.Vol
        adjust_price = position.Price

        if len(df_adjust[df_adjust["Code"] == position.Code]) > 0 and adjust_vol != 0:
            adjust_row = df_adjust[df_adjust["Code"] == position.Code].iloc[0]
            adjust_vol = float(position.Vol * (10 + adjust_row["DividendRatio"] + adjust_row["TransferRatio"]) / 10)
            adjust_price = float(position.Vol * float(position.Price) / adjust_vol)
            if adjust_vol != position.Vol:
                logger.error(
                    "{0},基金ID：{1},股票{2},昨日持有{3}股,除权后持有{4}".format(position.Date, position.FundId, position.Code,
                                                                  position.Vol, adjust_vol))
        day_position_dic[key] = Position(FundId=position.FundId, Code=position.Code, Vol=adjust_vol,
                                         Direction=position.Direction, Date=flow_day.strftime(common.MODEL_FORMAT),
                                         Price=adjust_price)

    # 当日空流水
    if len(df_trade_flow["Code"]) == 0:
        first_zero_hold = True
        for day_position in day_position_dic.values():
            if day_position.Vol == 0:
                if first_zero_hold:
                    first_zero_hold = False
                    position_list.append(day_position)
                else:
                    continue
            position_list.append(day_position)

            if day_position.Vol < 0:
                logger.error("当日空流水 持仓计算错误" + str(to_dict(day_position)))
        return position_list

    for index, row in df_trade_flow.iterrows():
        if row["Code"] == "CNY" or row["Code"] == "CREDIT":
            continue
        flow_key = "{0}_{1}".format(row["Code"], row["Direction"])
        if flow_key in day_position_dic:
            day_position = day_position_dic[flow_key]

            # 负持仓问题
            if row["Type"] == dict_order_type["Close"]:
                if row["Vol"] > day_position.Vol:
                    logger.error("持仓计算错误,平仓手数大于持仓手数:" + str(to_dict(day_position)) + ":" + str(row["Vol"]))
                    logger.error("按照除权进行计算,请确保仓位全平操作")
                    # 同时添加一笔红冲流水记录
                    if day_position.Vol != 0:
                        row["Price"] = row["Price"] * row["Vol"] / day_position.Vol
                        row["Vol"] = day_position.Vol
                        df_trade_flow.ix[index, "Vol"] = row["Vol"]
                        df_trade_flow.ix[index, "Price"] = row["Price"]
                    else:
                        logger.error("对0持仓执行了平仓操作,检查是否流水错误" + str(to_dict(day_position)) + ":" + str(row["Vol"]))

            # 更新持仓成本价
            if row["Type"] == dict_order_type["Open"]:
                if day_position.Vol + row["Vol"] == 0:
                    day_position.Price = 0
                else:
                    day_position.Price = (float(day_position.Price) * day_position.Vol + row["Price"] * row["Vol"]) / (
                            day_position.Vol + row["Vol"])
            # 更新持仓数量
            vol = row["Vol"] if row["Type"] == dict_order_type["Open"] else -1 * row["Vol"]
            day_position.Vol = day_position.Vol + vol
        else:
            # 将新开仓的流水存至持仓
            if row["Type"] == dict_order_type["Open"]:
                new_position = Position(FundId=fund_id, Code=row["Code"], Vol=row["Vol"], Direction=row["Direction"],
                                        Date=row["TradeDate"].strftime(common.MODEL_FORMAT), Price=row["Price"])
                day_position_dic[flow_key] = new_position
            else:
                # 对无历史仓位的股票进行平仓(新股流水)
                new_position = Position(FundId=fund_id, Code=row["Code"], Vol=-row["Vol"], Direction=row["Direction"],
                                        Date=row["TradeDate"].strftime(common.MODEL_FORMAT), Price=row["Price"])
                day_position_dic[flow_key] = new_position

    first_zero_hold = True
    for day_position in day_position_dic.values():
        # 若为0仓位不保存，则在仓位全清的情况下 找最近的持仓时会找到全清仓位前一天的状态；若保存,则所有0持仓都会被不断记录
        # 仅记录第一个0持仓，作为当日持仓状态
        if day_position.Vol == 0:
            if first_zero_hold:
                first_zero_hold = False
                position_list.append(day_position)
            else:
                continue

        position_list.append(day_position)

        if day_position.Vol < 0:
            # print("持仓计算错误",to_dict(day_position))
            logger.error("持仓计算错误" + str(to_dict(day_position)))
    return position_list, df_trade_flow


# 计算日收益Balance
def get_balance_by_trade_flow(last_balance, flow_day, df_trade_flow, position_last_day_list, position_day_list,
                              df_adjust):
    day_balance = Balance(Date=last_balance.Date, FundId=last_balance.FundId,
                          Cash=last_balance.Cash, MarketValue=last_balance.MarketValue,
                          ShortMarketValue=last_balance.ShortMarketValue, Profit=last_balance.Profit,
                          InitialFunds=last_balance.InitialFunds)
    day_balance.Date = flow_day.strftime(common.MODEL_FORMAT)
    change_fund = 0
    short_money = 0

    for index, row in df_trade_flow.iterrows():
        if row["Code"] != "CNY" and row["Code"] != "CREDIT":
            future_type = row["Code"][:2]
            if future_type in dict_future_type.keys():
                money = row["Price"] * row["Vol"] * dict_future_type[future_type]
                if row["Direction"] == dict_direction_type["Short"]:
                    short_money = (short_money + money) if row["Type"] == dict_order_type["Open"] else (
                            short_money - money)
            else:
                money = row["Price"] * row["Vol"]

            day_balance.Cash = round(
                (float(day_balance.Cash) - money) if row["Type"] == dict_order_type["Open"] else (
                        float(day_balance.Cash) + money), 10)
        elif row["Code"] == "CREDIT":
            money = row["Price"] * row["Vol"]
            day_balance.Cash = round(
                (float(day_balance.Cash) - money) if row["Type"] == dict_order_type["Open"] else (
                        float(day_balance.Cash) + money), 10)

    # 等其他流水处理完毕 最后处理资金流水
    cash_before_change = day_balance.Cash
    for index, row in df_trade_flow.iterrows():
        if row["Code"] == "CNY":
            money = row["Price"] * row["Vol"]
            change_fund = (change_fund + money) if row["Type"] == dict_order_type["Close"] else (
                    change_fund - money)
            day_balance.Cash = round(
                (float(day_balance.Cash) - money) if row["Type"] == dict_order_type["Open"] else (
                        float(day_balance.Cash) + money), 10)

    ask_last = flow_day.strftime(common.LT["day"]) == datetime.datetime.today().strftime(common.LT["day"])

    # 计算市值以及分红 分红加入现金中
    long_market_value, short_market_value, adjust_cash = get_market_value(position_day_list, position_last_day_list,
                                                                          df_adjust, ask_last=ask_last)

    day_balance.MarketValue = long_market_value
    day_balance.ShortMarketValue = short_market_value

    # 分红收益 不是现金流水加入的钱 所以放在cash_before_change
    cash_before_change = cash_before_change + adjust_cash
    # print("{0},现金分红{1},原始现金{2}".format(day_balance.Date,adjust_cash,day_balance.Cash))
    day_balance.Cash = float(day_balance.Cash + adjust_cash)

    short_market_cost = get_short_market_cost(position_day_list)

    today_market_value = (
            float(day_balance.MarketValue) + float(cash_before_change) + float(short_money) + float(
        short_market_cost) - float(
        day_balance.ShortMarketValue))
    today_net_value = today_market_value / float(last_balance.InitialFunds)
    day_balance.InitialFunds = round((float(last_balance.InitialFunds) + change_fund / today_net_value), 8)

    day_balance.Profit = round((float(day_balance.MarketValue) + float(day_balance.Cash) + short_money - change_fund - (
            float(last_balance.MarketValue) + float(last_balance.Cash)) + (
                                        -1 * float(day_balance.ShortMarketValue) + float(
                                    last_balance.ShortMarketValue)) + short_money) / float(
        last_balance.InitialFunds) * 100, 5)
    return day_balance


def calculator_today(fund_id):
    relations = FundAccountRelation.query.join(Account, Account.Id == FundAccountRelation.AccountId).filter(
        FundAccountRelation.FundId == fund_id).all()
    account_id_list = [relation.AccountId for relation in relations]
    trade_flow_list = Tradeflow.query.filter(
        and_(Tradeflow.AccountId.in_(account_id_list),
             Tradeflow.TradeDate >= datetime.datetime.today().strftime(common.TRADEFLOW_FORMAT))).all()
    if trade_flow_list is not None and len(trade_flow_list) > 0:
        df_result = pd.DataFrame(to_list(trade_flow_list, date_format=False))
        df_result["TradeDate_Format"] = df_result["TradeDate"].apply(
            lambda x: x)
        df_result["TradeDate"] = df_result["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x.strftime(common.MODEL_FORMAT)[:10], "%Y-%m-%d"))
        update_fund_trade_flow(fund_id, account_id_list, df_result,
                               delete_fund_info_before_update)


# 导入流水方式 全新的Fund初始化
def update_fund_trade_flow(fund_id, account_list, df_data, delete_function):
    result_position_list = list()
    result_balance_list = list()
    trade_date_list = list(set(df_data["TradeDate"]))
    trade_date_list.sort()
    last_balance, last_position_list = get_specific_day_info(fund_id, trade_date_list[0])
    for trade_date in trade_date_list:
        # 查询除权信息表
        df_trade_adjust = get_adjust_info(trade_date)

        df_oneday_data = df_data[df_data["TradeDate"] == trade_date]
        position_list, new_df_data = get_positions_by_trade_flow(fund_id, trade_date, last_position_list,
                                                                 df_oneday_data, df_trade_adjust)
        for position in position_list:
            result_position_list.append(position)

        balance = get_balance_by_trade_flow(last_balance, trade_date, new_df_data,
                                            last_position_list, position_list, df_trade_adjust)
        result_balance_list.append(balance)
        last_balance = balance
        last_position_list = position_list

    trade_flow_list = list()
    for index, row in df_data.iterrows():
        trade_flow = Tradeflow(AccountId=row["AccountId"], Code=row["Code"], Price=row["Price"],
                               Vol=row["Vol"], TradeDate=row["TradeDate_Format"].strftime(common.MODEL_FORMAT),
                               Direction=row["Direction"], Type=row["Type"])
        trade_flow_list.append(trade_flow)

    start_day = trade_date_list[0].strftime(common.MODEL_FORMAT)
    end_day = (trade_date_list[-1] + datetime.timedelta(days=1)).strftime(common.MODEL_FORMAT)
    # db.session.begin(subtransactions=True)
    delete_function(fund_id, account_list, start_day, end_day)
    db.session.add_all(trade_flow_list)
    db.session.add_all(result_balance_list)
    db.session.add_all(result_position_list)
    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        save_error2db("基金{0} 更新事务出错".format(fund_id))


def get_specific_day_info(fund_id, day):
    last_balance = Balance.query.filter(
        and_(Balance.FundId == fund_id, Balance.Date < day.strftime(common.MODEL_FORMAT))).order_by(
        Balance.Date.desc()).first()
    if last_balance is None:
        fund = Fund.query.filter(Fund.Id == fund_id).first()
        last_balance = Balance(Date=day.strftime(common.MODEL_FORMAT), FundId=fund_id,
                               Cash=fund.InitialFunds, MarketValue=0,
                               ShortMarketValue=0, Profit=0,
                               InitialFunds=fund.InitialFunds)
    last_position = Position.query.filter(
        and_(Position.FundId == fund_id, Position.Date < day.strftime(common.MODEL_FORMAT))).order_by(
        Position.Date.desc()).first()
    if last_position is None:
        last_day = datetime.datetime.min.strftime(common.MODEL_FORMAT)
    else:
        last_day = last_position.Date.strftime(common.MODEL_FORMAT)
    last_positions = Position.query.filter(
        and_(Position.FundId == fund_id, Position.Date == last_day)).order_by(
        Position.Date.desc()).all()
    if last_positions is None:
        last_positions = list()
    return last_balance, list(last_positions)


def deal_trade_flow(fund_id, df_data):
    trade_day_list = list(set(df_data["TradeDate"]))
    trade_day_list.sort()
    start_day = trade_day_list[0]
    relations = FundAccountRelation.query.join(Account, Account.Id == FundAccountRelation.AccountId).filter(
        FundAccountRelation.FundId == fund_id).all()
    account_id_list = [relation.AccountId for relation in relations]
    trade_flow_list = Tradeflow.query.filter(
        and_(Tradeflow.AccountId.in_(account_id_list),
             Tradeflow.TradeDate >= start_day.strftime(common.MODEL_FORMAT))).all()
    df_trade_flow = pd.DataFrame(to_list(trade_flow_list))
    if not df_trade_flow.empty:
        df_trade_flow["TradeDate_Format"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x[:10], "%Y-%m-%d"))
        df_trade_flow.drop(columns=["Id"], inplace=True)
        df_trade_flow = df_trade_flow[list(df_data.columns)]
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x.strftime("%Y-%m-%d %H:%M:%S"), common.MODEL_FORMAT))
        df_trade_flow = df_trade_flow[~df_trade_flow["TradeDate"].isin(trade_day_list)]
        df_result = pd.concat([df_data, df_trade_flow], axis=0, ignore_index=True)
    else:
        df_result = df_data
    update_fund_trade_flow(fund_id, account_id_list, df_result, delete_fund_info_before_update)


def update_one_trade_flow(fund_id, trade_flow, action):
    # 证券代码, 买卖数量, 买卖日期, 买卖价格, 证券类型, 买卖方向, 多空, 账户
    # CNY, 1000, 2019 / 05 / 18, 1, 现金, 卖出, 1, 332
    trade_flow.Vol = int(trade_flow.Vol)
    trade_flow.Price = float(trade_flow.Price)
    relations = FundAccountRelation.query.join(Account, Account.Id == FundAccountRelation.AccountId).filter(
        FundAccountRelation.FundId == fund_id).all()
    account_id_list = [relation.AccountId for relation in relations]
    update_day = trade_flow.TradeDate[:10]
    trade_flow_list = Tradeflow.query.filter(
        and_(Tradeflow.AccountId.in_(account_id_list),
             Tradeflow.TradeDate >= update_day)).all()
    # 删除对应流水
    for tf in trade_flow_list:
        if trade_flow.Id is not None and tf.Id == trade_flow.Id:
            trade_flow_list.remove(tf)
            break

    # 如果不是删除流水则都需要更新
    if action != "delete":
        trade_flow_list.insert(0, trade_flow)

    df_trade_flow = pd.DataFrame(to_list(trade_flow_list))
    df_trade_flow.drop(columns=["Id"], inplace=True)
    df_trade_flow["TradeDate_Format"] = df_trade_flow["TradeDate"].apply(
        lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
    df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
        lambda x: datetime.datetime.strptime(x[:10], "%Y-%m-%d"))
    update_fund_trade_flow(fund_id, account_id_list, df_trade_flow, delete_fund_info_before_update)


def delete_fund_info_before_update(fund_id, account_id_list, start_day, end_day):
    # 更新前先清空之后的数据
    Tradeflow.query.filter(and_(Tradeflow.AccountId.in_(account_id_list),
                                Tradeflow.TradeDate >= start_day,
                                Tradeflow.TradeDate < end_day)).delete(synchronize_session=False)

    Position.query.filter(and_(Position.FundId == fund_id,
                               Position.Date >= start_day,
                               Position.Date < end_day)).delete(synchronize_session=False)

    Balance.query.filter(and_(Balance.FundId == fund_id,
                              Balance.Date >= start_day,
                              Balance.Date < end_day)).delete(synchronize_session=False)


def get_base_profit_dict(base_code, start_date):
    # 指数收益
    dict_base_profit = dict()
    url = "http://www.wdl421.cn/api/dayapi.php?code={0}&type=2&starttime={1}&cycle=Day".format(
        base_code, start_date.strftime('%Y-%m-%d'))
    index_lines = requests.get(url)
    index_lines.encoding = "UTF-8-SIG"
    lines = index_lines.text.splitlines()
    last_price = 0
    first_price = 0
    for line in lines:
        arr = line.split(',')
        index_date = datetime.datetime.strptime(arr[0], "%Y-%m-%d %H:%M:%S")
        close_price = float(arr[4])
        if len(dict_base_profit) == 0:
            dict_base_profit[index_date] = 0
            first_price = close_price
        else:
            profit = (close_price - last_price) / first_price * 100
            dict_base_profit[index_date] = profit
        last_price = close_price
    return dict_base_profit


def get_temp_position_profit(fund_id):
    dict_profit = dict()  # 单股收益字典 {code:[昨收,昨持仓,当前价格,当前持仓,持仓收益,开仓收益,平仓收益,当日总收益]}
    today = datetime.datetime.today()
    today = datetime.date(today.year, today.month, today.day)
    yesterday = Balance.query.filter(and_(Balance.FundId == fund_id, Balance.Date < today)).order_by(
        Balance.Date.desc()).first().Date

    relations = FundAccountRelation.query.join(Account, Account.Id == FundAccountRelation.AccountId).filter(
        FundAccountRelation.FundId == fund_id).all()
    account_id_list = [relation.AccountId for relation in relations]
    today_trade_flow = Tradeflow.query.filter(
        and_(Tradeflow.AccountId.in_(account_id_list),
             Tradeflow.TradeDate >= today)).all()
    yesterday_position = Position.query.filter(
        and_(Position.FundId == fund_id, Position.Date == yesterday)).all()

    # 昨日持仓
    day_position_dic = dict()
    for position in yesterday_position:
        key = "{0}_{1}".format(position.Code, position.Direction)
        day_position_dic[key] = Position(FundId=position.FundId, Code=position.Code, Vol=position.Vol,
                                         Direction=position.Direction, Date=today.strftime(common.TRADEFLOW_FORMAT),
                                         Price=float(position.Price))
    day_flow_key_list = list()
    for flow in today_trade_flow:
        day_flow_key_list.append("{0}_{1}".format(flow.Code, flow.Direction))
        if flow.Code == "CNY":
            continue
        code_price = 1
        if "IC" in flow.Code:
            code_price = dict_future_type["IC"]
        elif "IF" in flow.Code:
            code_price = dict_future_type["IF"]
        elif "IH" in flow.Code:
            code_price = dict_future_type["IH"]
        flow_key = "{0}_{1}".format(flow.Code, flow.Direction)

        last_close = get_history_close(flow.Code, yesterday.strftime('%Y-%m-%d %H:%M:%S'))
        now_price = get_real_close(flow.Code)
        open_profit = 0
        hold_profit = 0
        close_profit = 0
        now_vol_change = 0
        if flow_key not in dict_profit:
            dict_profit[flow_key] = TempPositionProfit(code=flow.Code, direction=flow.Direction, last_close=last_close,
                                                       now_price=now_price)
            if flow_key in day_position_dic:
                dict_profit[flow_key].open_price = day_position_dic[flow_key].Price

        if flow_key in day_position_dic:
            day_position = day_position_dic[flow_key]
            dict_profit[flow_key].last_vol = day_position.Vol
            if flow.Type == 0:
                # 若为开仓 则收益按照开仓和持仓计算 确保流水已经合并 否则hold_profit计算重复
                try:
                    now_vol_change = float(flow.Vol)
                    dict_profit[flow_key].open_price = (float(flow.Price) * float(
                        flow.Vol) + day_position.Price * day_position.Vol) / (float(flow.Vol) + day_position.Vol)
                    open_profit = (now_price - float(flow.Price)) * float(flow.Vol) * (
                        1 if flow.Direction == 1 else -1) * code_price
                    hold_profit = (now_price - last_close) * (
                            day_position.Vol - dict_profit[flow_key].hold_position_for_close) * (
                                      1 if flow.Direction == 1 else -1) * code_price
                    dict_profit[flow_key].hold_position_for_open = dict_profit[
                                                                       flow_key].hold_position_for_open + day_position.Vol
                except Exception as e:
                    msg = "行权数据出错;flow--%s;position--%s;" % (str(to_dict(flow)), str(to_dict(day_position)))
                    save_error2db(msg)
            else:
                # 若为平仓 则收益按照平仓和持仓计算
                now_vol_change = - float(flow.Vol)
                close_profit = (float(flow.Price) - last_close) * float(flow.Vol) * (
                    1 if flow.Direction == 1 else -1) * code_price
                hold_profit = (now_price - last_close) * (
                        day_position.Vol - dict_profit[flow_key].hold_position_for_open - float(flow.Vol)) * (
                                  1 if flow.Direction == 1 else -1) * code_price
                dict_profit[flow_key].hold_position_for_close = dict_profit[
                                                                    flow_key].hold_position_for_close + day_position.Vol
        else:
            # 说明是新开仓位 若是平仓则说明流水有误差
            if flow.Type == 0:
                now_vol_change = float(flow.Vol)
                dict_profit[flow_key].open_price = float(flow.Price)
                open_profit = (now_price - float(flow.Price)) * float(flow.Vol) * (
                    1 if flow.Direction == 1 else -1) * code_price
            else:
                now_vol_change = -float(flow.Vol)
                # print("利用流水计算当日盈利出现负持仓,检查是否为新股平仓！！---》",flow_key)
                logger.error("利用流水计算当日盈利出现负持仓,检查是否为新股平仓！！---》" + flow_key)
        dict_profit[flow_key].now_vol_change = dict_profit[flow_key].now_vol_change + now_vol_change
        dict_profit[flow_key].hold_profit = dict_profit[flow_key].hold_profit + hold_profit
        dict_profit[flow_key].open_profit = dict_profit[flow_key].open_profit + open_profit
        dict_profit[flow_key].close_profit = dict_profit[flow_key].close_profit + close_profit

    for code_dir in day_position_dic:
        # 没有进行操作的历史持仓 按照持仓计算收益
        if code_dir not in day_flow_key_list:
            code = code_dir.split('_')[0]
            dir = code_dir.split('_')[1]
            code_price = 1
            if "IC" in code:
                code_price = dict_future_type["IC"]
            elif "IF" in code:
                code_price = dict_future_type["IF"]
            elif "IH" in code:
                code_price = dict_future_type["IH"]
            day_position = day_position_dic[code_dir]
            last_close = get_history_close(code, yesterday.strftime('%Y-%m-%d %H:%M:%S'))
            now_price = get_real_close(code)
            if code_dir not in dict_profit:
                dict_profit[code_dir] = TempPositionProfit(code=code, direction=int(dir), last_close=last_close,
                                                           now_price=now_price, last_vol=day_position.Vol,
                                                           now_vol_change=0, open_price=day_position.Price)
            hold_profit = (now_price - last_close) * day_position.Vol * (
                1 if day_position.Direction == 1 else -1) * code_price
            dict_profit[code_dir].hold_profit = dict_profit[code_dir].hold_profit + hold_profit
    return dict_profit


# 回测相关api


def deal_looptest_flow(strategy_id, df_data):
    trade_day_list = list(set(df_data["TradeDate"]))
    trade_day_list.sort()
    start_day = trade_day_list[0]

    loop_test_flow_list = StrategyFlow.query.filter(
        and_(StrategyFlow.StrategyId == strategy_id,
             StrategyFlow.TradeDate >= start_day.strftime(common.MODEL_FORMAT))).all()

    df_trade_flow = pd.DataFrame(to_list(loop_test_flow_list))
    if not df_trade_flow.empty:
        df_trade_flow["TradeDate_Format"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x[:10], "%Y-%m-%d"))
        df_trade_flow.drop(columns=["Id"], inplace=True)
        df_trade_flow = df_trade_flow[list(df_data.columns)]
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x.strftime("%Y-%m-%d %H:%M:%S"), common.MODEL_FORMAT))
        df_trade_flow = df_trade_flow[~df_trade_flow["TradeDate"].isin(trade_day_list)]
        df_result = pd.concat([df_data, df_trade_flow], axis=0, ignore_index=True)
    else:
        df_result = df_data
    update_loop_test_flow(strategy_id, df_result, delete_strategy_info_before_update)


def deal_adjust_looptest_flow(fund_id, strategy_id, df_data):
    trade_day_list = list(set(df_data["TradeDate"]))
    trade_day_list.sort()

    start_day = trade_day_list[0].strftime(common.MODEL_FORMAT)
    # end_day = (trade_day_list[-1] + datetime.timedelta(days=1)).strftime(common.MODEL_FORMAT)
    # end_day = trade_day_list[-1].strftime(common.MODEL_FORMAT)
    balance_list = []
    position_list = []
    for date in trade_day_list:
        day_fund_balance = Balance.query.filter(
            and_(Balance.FundId == fund_id, Balance.Date == date.strftime(common.MODEL_FORMAT))).first()
        # 第一个balance应该做对齐操作
        day_strategy_balance = StrategyBalance(Date=day_fund_balance.Date, StrategyId=strategy_id,
                                               Cash=day_fund_balance.Cash, MarketValue=day_fund_balance.MarketValue,
                                               ShortMarketValue=day_fund_balance.ShortMarketValue,
                                               Profit=day_fund_balance.Profit,
                                               InitialFunds=day_fund_balance.InitialFunds)
        day_fund_positions = Position.query.filter(
            and_(Position.FundId == fund_id, Position.Date == date.strftime(common.MODEL_FORMAT))).all()
        balance_list.append(day_strategy_balance)
        for day_fund_position in day_fund_positions:
            day_strategy_position = StrategyPosition(StrategyId=strategy_id, Code=day_fund_position.Code,
                                                     Vol=day_fund_position.Vol,
                                                     Direction=day_fund_position.Direction,
                                                     Date=day_fund_position.TradeDate, Price=day_fund_position.Price)
            position_list.append(day_strategy_position)

    delete_strategy_info_before_update(strategy_id, start_day,
                                       (trade_day_list[-1] + datetime.timedelta(days=1)).strftime(common.MODEL_FORMAT))

    db.session.add_all(balance_list)
    db.session.add_all(position_list)
    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()


def update_loop_test_flow(strategy_id, df_data, delete_function):
    result_position_list = list()
    result_balance_list = list()
    trade_date_list = list(set(df_data["TradeDate"]))
    trade_date_list.sort()
    last_balance, last_position_list = get_specific_loop_test_day_info(strategy_id, trade_date_list[0])
    for trade_date in trade_date_list:
        print("strategy_id:", strategy_id, "计算date:", trade_date)
        # if contain_today or trade_date.strftime(common.LT["day"]) != datetime.datetime.today().strftime(
        #         common.LT["day"]):
        df_oneday_data = df_data[df_data["TradeDate"] == trade_date]
        position_list, new_df_data = get_positions_by_loop_test_flow(strategy_id, trade_date, last_position_list,
                                                                     df_oneday_data)
        for position in position_list:
            result_position_list.append(position)

        balance = get_balance_by_loop_test_flow(last_balance, trade_date, new_df_data,
                                                position_list)
        result_balance_list.append(balance)
        last_balance = balance
        last_position_list = position_list

    trade_flow_list = list()
    for index, row in df_data.iterrows():
        trade_flow = StrategyFlow(StrategyId=strategy_id, Code=row["Code"], Price=row["Price"],
                                  Vol=row["Vol"], TradeDate=row["TradeDate_Format"].strftime(common.MODEL_FORMAT),
                                  Direction=row["Direction"], Type=row["Type"])
        trade_flow_list.append(trade_flow)

    start_day = trade_date_list[0].strftime(common.MODEL_FORMAT)
    end_day = (trade_date_list[-1] + datetime.timedelta(days=1)).strftime(common.MODEL_FORMAT)
    # db.session.begin(subtransactions=True)
    delete_function(strategy_id, start_day, end_day)
    db.session.add_all(trade_flow_list)
    db.session.add_all(result_balance_list)
    db.session.add_all(result_position_list)
    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()


def update_one_strategy_trade_flow(strategy_id, trade_flow, action):
    # 证券代码, 买卖数量, 买卖日期, 买卖价格, 证券类型, 买卖方向, 多空, 策略ID
    # CNY, 1000, 2019 / 05 / 18, 1, 现金, 卖出, 1, 1
    trade_flow.Vol = int(trade_flow.Vol)
    trade_flow.Price = float(trade_flow.Price)
    update_day = trade_flow.TradeDate[:10]
    trade_flow_list = StrategyFlow.query.filter(
        and_(StrategyFlow.StrategyId == trade_flow.StrategyId,
             StrategyFlow.TradeDate >= update_day)).all()
    # 删除对应流水
    for tf in trade_flow_list:
        if trade_flow.Id is not None and tf.Id == trade_flow.Id:
            trade_flow_list.remove(tf)
            break

    # 如果不是删除流水则都需要更新
    if action != "delete":
        trade_flow_list.insert(0, trade_flow)

    df_trade_flow = pd.DataFrame(to_list(trade_flow_list))
    df_trade_flow.drop(columns=["Id"], inplace=True)
    df_trade_flow["TradeDate_Format"] = df_trade_flow["TradeDate"].apply(
        lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
    df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
        lambda x: datetime.datetime.strptime(x[:10], "%Y-%m-%d"))
    update_loop_test_flow(strategy_id, df_trade_flow, delete_strategy_info_before_update)


def get_specific_loop_test_day_info(strategy_id, day):
    last_balance = StrategyBalance.query.filter(
        and_(StrategyBalance.StrategyId == strategy_id,
             StrategyBalance.Date < day.strftime(common.MODEL_FORMAT))).order_by(
        StrategyBalance.Date.desc()).first()
    if last_balance is None:
        strategy = Strategy.query.filter(Strategy.Id == strategy_id).first()
        last_balance = StrategyBalance(Date=day.strftime(common.MODEL_FORMAT), StrategyId=strategy_id,
                                       Cash=strategy.InitialFund, MarketValue=0,
                                       ShortMarketValue=0, Profit=0,
                                       InitialFunds=strategy.InitialFund)
    last_position = StrategyPosition.query.filter(
        and_(StrategyPosition.StrategyId == strategy_id,
             StrategyPosition.Date < day.strftime(common.MODEL_FORMAT))).order_by(
        StrategyPosition.Date.desc()).first()
    if last_position is None:
        last_day = datetime.datetime.min.strftime(common.MODEL_FORMAT)
    else:
        last_day = last_position.Date.strftime(common.MODEL_FORMAT)
    last_positions = StrategyPosition.query.filter(
        and_(StrategyPosition.StrategyId == strategy_id, StrategyPosition.Date == last_day)).order_by(
        StrategyPosition.Date.desc()).all()
    if last_positions is None:
        last_positions = list()
    return last_balance, list(last_positions)


def get_positions_by_loop_test_flow(strategy_id, flow_day, last_position_list, df_trade_flow):
    # 根据流水，更新持仓信息
    position_list = list()
    if len(df_trade_flow["Code"]) == 0:
        return last_position_list

    day_position_dic = dict()
    for position in last_position_list:
        key = "{0}_{1}".format(position.Code, position.Direction)
        day_position_dic[key] = StrategyPosition(StrategyId=position.StrategyId, Code=position.Code, Vol=position.Vol,
                                                 Direction=position.Direction,
                                                 Date=flow_day.strftime(common.MODEL_FORMAT),
                                                 Price=position.Price)
    for index, row in df_trade_flow.iterrows():
        if row["Code"] == "CNY" or row["Code"] == "CREDIT":
            continue
        flow_key = "{0}_{1}".format(row["Code"], row["Direction"])
        if flow_key in day_position_dic:
            day_position = day_position_dic[flow_key]

            # 负持仓问题
            if row["Type"] == dict_order_type["Close"]:
                # # if row["Vol"] > day_position.Vol and (row["Vol"]-day_position.Vol)*row["Price"]<=5000:
                # #     row["Vol"] = day_position.Vol
                # #     df_trade_flow.ix[index, "Vol"] = day_position.Vol
                # if abs(row["Vol"] - day_position.Vol) * row["Price"] <= 5000:
                #     row["Vol"] = day_position.Vol
                #     df_trade_flow.ix[index, "Vol"] = day_position.Vol

                if row["Vol"] > day_position.Vol:
                    # print("持仓计算错误,平仓手数大于持仓手数:", to_dict(day_position),row["Vol"])
                    # print("按照除权进行计算,请确保仓位全平操作")
                    msg = "持仓计算错误,平仓手数大于持仓手数:" + str(to_dict(day_position)) + str(row["Vol"]) + "按照除权进行计算,请确保仓位全平操作"
                    # logger.error("持仓计算错误,平仓手数大于持仓手数:" + str(to_dict(day_position)) + str(row["Vol"]))
                    # logger.error("按照除权进行计算,请确保仓位全平操作")
                    save_error2db(msg)
                    # 同时添加一笔红冲流水记录
                    if day_position.Vol != 0:
                        row["Price"] = row["Price"] * row["Vol"] / day_position.Vol
                        row["Vol"] = day_position.Vol
                        df_trade_flow.ix[index, "Vol"] = row["Vol"]
                        df_trade_flow.ix[index, "Price"] = row["Price"]
                    else:
                        # print("对0持仓执行了平仓操作,检查是否流水错误",to_dict(day_position),row["Vol"])
                        msg = "对0持仓执行了平仓操作,检查是否流水错误" + str(to_dict(day_position)) + str(row["Vol"])
                        save_error2db(msg)

            # 更新持仓成本价
            if row["Type"] == dict_order_type["Open"]:
                if day_position.Vol + row["Vol"] == 0:
                    day_position.Price = 0
                else:
                    day_position.Price = (float(day_position.Price) * day_position.Vol + row["Price"] * row["Vol"]) / (
                            day_position.Vol + row["Vol"])
            # 更新持仓数量
            vol = row["Vol"] if row["Type"] == dict_order_type["Open"] else -1 * row["Vol"]
            day_position.Vol = day_position.Vol + vol
        else:
            # 将新开仓的流水存至持仓
            if row["Type"] == dict_order_type["Open"]:
                new_position = StrategyPosition(StrategyId=strategy_id, Code=row["Code"], Vol=row["Vol"],
                                                Direction=row["Direction"],
                                                Date=row["TradeDate"].strftime(common.MODEL_FORMAT), Price=row["Price"])
                day_position_dic[flow_key] = new_position
            else:
                # 对无历史仓位的股票进行平仓(新股流水) 或者同一个code录入了2条多余的平仓记录 第一条导致day_position_dic存入了该code 处理第二条时由于读到day_position_dic有持仓当作了行权票
                new_position = StrategyPosition(StrategyId=strategy_id, Code=row["Code"], Vol=-row["Vol"],
                                                Direction=row["Direction"],
                                                Date=row["TradeDate"].strftime(common.MODEL_FORMAT), Price=row["Price"])
                day_position_dic[flow_key] = new_position

    first_zero_hold = True
    for day_position in day_position_dic.values():
        # 若为0仓位不保存，则在仓位全清的情况下 找最近的持仓时会找到全清仓位前一天的状态；若保存,则所有0持仓都会被不断记录
        # 仅记录第一个0持仓，作为当日持仓状态
        if day_position.Vol == 0:
            if first_zero_hold:
                first_zero_hold = False
                position_list.append(day_position)
            else:
                continue

        position_list.append(day_position)

        if day_position.Vol < 0:
            # print("持仓计算错误",to_dict(day_position))
            msg = "持仓计算错误:" + str(to_dict(day_position))
            save_error2db(msg)
    return position_list, df_trade_flow


# 计算日收益Balance
def get_balance_by_loop_test_flow(last_balance, flow_day, df_trade_flow, position_day_list):
    day_balance = StrategyBalance(Date=last_balance.Date, StrategyId=last_balance.StrategyId,
                                  Cash=last_balance.Cash, MarketValue=last_balance.MarketValue,
                                  ShortMarketValue=last_balance.ShortMarketValue, Profit=last_balance.Profit,
                                  InitialFunds=last_balance.InitialFunds)
    day_balance.Date = flow_day.strftime(common.MODEL_FORMAT)
    change_fund = 0
    short_money = 0

    for index, row in df_trade_flow.iterrows():
        if row["Code"] != "CNY" and row["Code"] != "CREDIT":
            future_type = row["Code"][:2]
            if future_type in dict_future_type.keys():
                money = row["Price"] * row["Vol"] * dict_future_type[future_type]
                if row["Direction"] == dict_direction_type["Short"]:
                    short_money = (short_money + money) if row["Type"] == dict_order_type["Open"] else (
                            short_money - money)
            else:
                money = row["Price"] * row["Vol"]

            day_balance.Cash = round(
                (float(day_balance.Cash) - money) if row["Type"] == dict_order_type["Open"] else (
                        float(day_balance.Cash) + money), 10)
        elif row["Code"] == "CREDIT":
            money = row["Price"] * row["Vol"]
            day_balance.Cash = round(
                (float(day_balance.Cash) - money) if row["Type"] == dict_order_type["Open"] else (
                        float(day_balance.Cash) + money), 10)

    # 等其他流水处理完毕 最后处理资金流水
    cash_before_change = day_balance.Cash
    for index, row in df_trade_flow.iterrows():
        if row["Code"] == "CNY":
            money = row["Price"] * row["Vol"]
            change_fund = (change_fund + money) if row["Type"] == dict_order_type["Close"] else (
                    change_fund - money)
            day_balance.Cash = round(
                (float(day_balance.Cash) - money) if row["Type"] == dict_order_type["Open"] else (
                        float(day_balance.Cash) + money), 10)

    ask_last = flow_day.strftime(common.LT["day"]) == datetime.datetime.today().strftime(common.LT["day"])
    long_market_value, short_market_value, adjust_cash = get_market_value(position_day_list, ask_last=ask_last)

    day_balance.MarketValue = long_market_value
    day_balance.ShortMarketValue = short_market_value

    short_market_cost = get_short_market_cost(position_day_list)
    today_market_value = (
            float(day_balance.MarketValue) + float(cash_before_change) + float(short_money) + float(
        short_market_cost) - float(
        day_balance.ShortMarketValue))
    today_net_value = today_market_value / float(last_balance.InitialFunds)
    day_balance.InitialFunds = round((float(last_balance.InitialFunds) + change_fund / today_net_value), 8)

    day_balance.Profit = round((float(day_balance.MarketValue) + float(day_balance.Cash) + short_money - change_fund - (
            float(last_balance.MarketValue) + float(last_balance.Cash)) + (
                                        -1 * float(day_balance.ShortMarketValue) + float(
                                    last_balance.ShortMarketValue)) + short_money) / float(
        last_balance.InitialFunds) * 100, 5)
    return day_balance


def delete_strategy_info_before_update(strategy_id, start_day, end_day):
    # 更新前先清空之后的数据
    StrategyFlow.query.filter(and_(StrategyFlow.StrategyId == strategy_id,
                                   StrategyFlow.TradeDate >= start_day,
                                   StrategyFlow.TradeDate < end_day)).delete(synchronize_session=False)

    StrategyPosition.query.filter(and_(StrategyPosition.StrategyId == strategy_id,
                                       StrategyPosition.Date >= start_day,
                                       StrategyPosition.Date < end_day)).delete(synchronize_session=False)

    StrategyBalance.query.filter(and_(StrategyBalance.StrategyId == strategy_id,
                                      StrategyBalance.Date >= start_day,
                                      StrategyBalance.Date < end_day)).delete(synchronize_session=False)


def recalculator_strategy(strategy_id):
    strategy_flow_list = StrategyFlow.query.filter(StrategyFlow.StrategyId == strategy_id).all()
    df_trade_flow = pd.DataFrame(to_list(strategy_flow_list))
    if not df_trade_flow.empty:
        df_trade_flow["TradeDate_Format"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x[:10], "%Y-%m-%d"))
        df_trade_flow.drop(columns=["Id"], inplace=True)
        # df_trade_flow = df_trade_flow[list(df_data.columns)]
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x.strftime("%Y-%m-%d %H:%M:%S"), common.MODEL_FORMAT))
        # update_loop_test_flow(strategy_id, df_trade_flow, delete_strategy_info_before_recalculator)
        update_loop_test_flow(strategy_id, df_trade_flow, delete_strategy_info_before_update)


def recalculator_strategy_recent_day(strategy_id):
    '''
    重新计算最近几日回测数据
    '''
    last_flow = StrategyFlow.query.filter(StrategyFlow.StrategyId == strategy_id).order_by(
        StrategyFlow.TradeDate.desc()).first()
    strategy_flow_list = StrategyFlow.query.filter(and_(StrategyFlow.StrategyId == strategy_id,
                                                        StrategyFlow.TradeDate >= last_flow.TradeDate.strftime(
                                                            common.TRADEFLOW_FORMAT))).all()
    df_trade_flow = pd.DataFrame(to_list(strategy_flow_list))
    # print(df_trade_flow)
    if not df_trade_flow.empty:
        df_trade_flow["TradeDate_Format"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x[:10], "%Y-%m-%d"))
        df_trade_flow.drop(columns=["Id"], inplace=True)
        # df_trade_flow = df_trade_flow[list(df_data.columns)]
        df_trade_flow["TradeDate"] = df_trade_flow["TradeDate"].apply(
            lambda x: datetime.datetime.strptime(x.strftime("%Y-%m-%d %H:%M:%S"), common.MODEL_FORMAT))
        # update_loop_test_flow(strategy_id, df_trade_flow, delete_strategy_info_before_recalculator)
        update_loop_test_flow(strategy_id, df_trade_flow, delete_strategy_info_before_update)


def update_one_accrual_flow(fund_id, accrual_flow, action):
    accrual_flow.Vol = int(accrual_flow.Vol)
    accrual_flow.Price = float(accrual_flow.Price)
    accrual_flow_list = AccrualFlow.query.filter(AccrualFlow.FundId == fund_id).all()
    # 删除对应流水
    for af in accrual_flow_list:
        if accrual_flow.Id is not None and af.Id == accrual_flow.Id:
            accrual_flow_list.remove(af)
            break

    # 如果不是删除流水则都需要更新
    if action != "delete":
        accrual_flow_list.insert(0, accrual_flow)

    df_trade_flow = pd.DataFrame(to_list(accrual_flow_list))
    if len(df_trade_flow) > 0:
        df_trade_flow.drop(columns=["Id"], inplace=True)

    trade_flow_list = []
    for index, row in df_trade_flow.iterrows():
        trade_flow = AccrualFlow(FundId=fund_id, Price=row["Price"],
                                 Vol=row["Vol"], AccrualDate=row["AccrualDate"],
                                 Direction=row["Direction"])
        trade_flow_list.append(trade_flow)

    AccrualFlow.query.filter(AccrualFlow.FundId == fund_id).delete(synchronize_session=False)

    db.session.add_all(trade_flow_list)
    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
