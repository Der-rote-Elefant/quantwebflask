import json
from app.util.rpc_request import RPCRequestService
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol


def request(host, port, method_name, request_dict=None):
    transport = TSocket.TSocket(host, port)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)
    client = RPCRequestService.Client(protocol)
    transport.open()
    if request_dict is None or not isinstance(request_dict, dict):
        request_dict = dict()
    data = json.dumps(request_dict)
    result = client.Request(method_name, data)
    transport.close()
    return result


if __name__ == '__main__':
    res = request("localhost", 10000, "Ping")
    print(res)
