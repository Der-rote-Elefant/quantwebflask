# -*- coding: UTF-8 -*-
import numpy as np
import pandas as pd
import math
import datetime

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = ['csv', 'py', '.cs']
LT = {"day": "%Y-%m-%d", "week": "%Y-%W", "month": "%Y-%m", "year": "%Y"}
MODEL_FORMAT = "%Y-%m-%d %H:%M:%S"
TRADEFLOW_FORMAT = "%Y-%m-%d 00:00:00"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_time_section(date):
    date = datetime.datetime.strptime(date, "%Y-%m-%d")
    last_date = date - datetime.timedelta(minutes=1)
    future_date = date + datetime.timedelta(days=1) - datetime.timedelta(minutes=1)
    last_date = last_date.strftime(MODEL_FORMAT)
    future_date = future_date.strftime(MODEL_FORMAT)
    return last_date, future_date
