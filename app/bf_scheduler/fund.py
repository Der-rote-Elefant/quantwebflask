from ..extensions import cache, db
from ..products.views import count_real_time_profit
from ..models import Fund
from .. import app
import json
from datetime import datetime
from .ths.util.wechat import Wechat
import os
from ..util.log_util import logger


def count_real_time_profit_time():
    with app.app_context():
        logger.error("%s 定时器开始" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        ids = [f.Id for f in Fund.query.filter_by(FundType=0).all()]
        for i in ids:
            result = count_real_time_profit(i)
            cache_key = "realTimeProfit-" + str(i)
            cache.set(cache_key, json.dumps(result))
        db.session.close()
        logger.error("%s 定时器结束" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def send_error_msg():
    try:
        logger.error("%s :send_error_msg 执行一次" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        wechat = Wechat(users=["DDC", "XZR", "ZCJ"])
        cache_key = "error2wechat-*"
        i = 0
        err_dir = "./err_dir"
        file_name = os.path.join(err_dir, datetime.now().strftime("%Y%m%d.log"))
        if not os.path.exists(err_dir):
            os.mkdir(err_dir)
        for key in cache.keys(cache_key):
            with open(file_name, mode="a+") as f:
                i += 1
                err_msg = cache.get(key)
                f.write("%s\n" % str(err_msg, encoding="utf-8"))
                cache.delete(key)
        if i > 0:
            wechat.send_message("有异常数据，注意处理")
    except Exception as e:
        logger.error("%s :send_error_msg出现异常" % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        logger.error(e)
