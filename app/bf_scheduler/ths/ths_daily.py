# -*- coding: utf-8 -*-

from iFinDPy import *
from datetime import datetime
from datetime import timedelta
import pandas as pd
import schedule
from util.wechat import Wechat
from util.ths_api import ThsApi
from ths_config import *
import time
from util import mysql_db
import numpy as np
from util import mongo_db
import pytz
from util.redis_db import RedisHelper


def check_trade_day(func):
    """
    装饰器 用来同意判断是否交易日，登录登出同花顺
    :param func:
    :return:
    """

    def inner(*args, **kwargs):
        today_str = date.today().strftime(DATE_FORMAT)
        date_str = get_last_trade_day()
        if today_str == date_str:
            ths_api = ThsApi(THS_CONFIG["user"], THS_CONFIG["password"])
            if ths_api.login():
                func(ths_api, today_str, *args, **kwargs)
                ths_api.logout()
            else:
                message_send = Wechat(WARN_USERS)
                message_send.send_message("func:{0}出错,同花顺登录失败".format(func.__name__))

    return inner


def get_last_trade_day():
    """
    获取最近一个交易日日期
    :return: 最近一个交易日日期的字符串
    """
    mysql_helper = mysql_db.MysqlDBBase(host=MYSQL_TRADE_CONFIG["host"],
                                        port=MYSQL_TRADE_CONFIG["port"],
                                        user_id=MYSQL_TRADE_CONFIG["user"],
                                        password=MYSQL_TRADE_CONFIG["password"],
                                        db_name=MYSQL_TRADE_CONFIG["db"])
    df_date = mysql_helper.query("select MAX(Date) as Date from trade_date")
    date_str = df_date["Date"].iat[-1].strftime(DATE_FORMAT)
    return date_str


def get_industry(stock_helper, industry_code):
    """
    获取industry_code对应的id以及最近成分
    :param stock_helper:
    :param industry_code:
    :return:
    """
    df_industry = stock_helper.query(
        'select Id,Name,industrycode from industry where industrycode=%(industrycode)s',
        params={'industrycode': industry_code})
    industry_id = df_industry["Id"].iat[-1]

    df_last_date = stock_helper.query(
        'select MAX(Date) as Date from stock_industry where IndustryID=%(IndustryID)s',
        params={'IndustryID': industry_id})
    last_day_str = df_last_date["Date"].iat[-1]

    last_df = stock_helper.query(
        'select Code,IndustryID,Date,Weight from stock_industry where IndustryID=%(IndustryID)s and Date=%(industrycode)s',
        params={'IndustryID': industry_id, 'industrycode': last_day_str})
    return industry_id, last_df


def update_trade_day():
    """
    更新交易日
    :return:None
    """
    ths_api = ThsApi(THS_CONFIG["user"], THS_CONFIG["password"])
    if ths_api.login():
        date_str = date.today().strftime(DATE_FORMAT)
        is_trade_day = ths_api.is_trade_day(date_str)
        if is_trade_day:
            mysql_helper = mysql_db.MysqlDBBase(host=MYSQL_TRADE_CONFIG["host"],
                                                port=MYSQL_TRADE_CONFIG["port"],
                                                user_id=MYSQL_TRADE_CONFIG["user"],
                                                password=MYSQL_TRADE_CONFIG["password"],
                                                db_name=MYSQL_TRADE_CONFIG["db"])
            df = pd.DataFrame({"Date": [date.today(), ]})
            mysql_helper.save(table_name="trade_date", df=df)

        ths_api.logout()
        print(datetime.now(), "更新交易日完成")
    else:
        message_send = Wechat(WARN_USERS)
        message_send.send_message("func:{0}出错,同花顺登录失败")


def update_block():
    """更新板块指数列表"""
    update_index_component()
    update_stock_list()
    update_st()
    update_credit_list()


@check_trade_day
def update_credit_list(ths_api, today_str):
    """
    更新融资融券列表
    :return:None
    """

    stock_helper = mysql_db.MysqlDBBase(host=MYSQL_STOCK_CONFIG["host"],
                                        port=MYSQL_STOCK_CONFIG["port"],
                                        user_id=MYSQL_STOCK_CONFIG["user"],
                                        password=MYSQL_STOCK_CONFIG["password"],
                                        db_name=MYSQL_STOCK_CONFIG["db"])
    industry_id, last_df = get_industry(stock_helper, "rzrq")
    df_stock = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str, is_replace=False)
    df_rzrq = ths_api.get_basic_data(",".join(df_stock["THSCODE"]), today_str)
    df_rzrq = df_rzrq[df_rzrq["ths_is_mt_ss_underlying_stock"].isin(["是", ])]
    order = ["Code", "IndustryID", "Date"]
    df_rzrq["IndustryID"] = df_rzrq["thscode"].apply(lambda x: industry_id)
    df_rzrq["Date"] = df_rzrq["thscode"].apply(lambda x: date.today())
    df_rzrq.rename(
        columns={'thscode': 'Code', 'IndustryID': 'IndustryID', 'Date': 'Date'},
        inplace=True)
    df_rzrq = df_rzrq.sort_values(by="Code", axis=0, ascending=True)
    df_rzrq = df_rzrq[order]
    print(len(df_rzrq["Code"]), len(last_df["Code"]))
    if not last_df.empty and len(df_rzrq["Code"]) == len(last_df["Code"]):
        all_code = (np.array(df_rzrq["Code"]) == np.array(df_rzrq["Code"])).all()
        if not all_code:
            stock_helper.save(table_name="stock_industry", df=df_rzrq)
    else:
        stock_helper.save(table_name="stock_industry", df=df_rzrq)
    print("融资融券列表更新成功")


@check_trade_day
def update_stock_list(ths_api, today_str):
    """
    更新股票列表
    :return:
    """

    stock_helper = mysql_db.MysqlDBBase(host=MYSQL_STOCK_CONFIG["host"],
                                        port=MYSQL_STOCK_CONFIG["port"],
                                        user_id=MYSQL_STOCK_CONFIG["user"],
                                        password=MYSQL_STOCK_CONFIG["password"],
                                        db_name=MYSQL_STOCK_CONFIG["db"])
    df_db = stock_helper.query(
        'select ProductID,ProductClass,InstrumentID,InstrumentName,ExchangeID from stock')
    df_db["Code"] = df_db["InstrumentID"] + df_db["ProductClass"]
    order = ["ProductID", "ProductClass", "InstrumentID", "InstrumentName", "ExchangeID"]
    df_stock = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str)
    # print(df_stock)
    if not df_stock.empty:
        df_stock = df_stock.fillna(value=0)
        df_temp = pd.DataFrame()
        df_temp["ProductID"] = df_stock["THSCODE"].apply(lambda x: x[:x.index(".")])
        df_temp["ProductClass"] = df_stock["THSCODE"].apply(lambda x: "SK")
        df_temp["InstrumentID"] = df_temp["ProductID"]
        df_temp["InstrumentName"] = df_stock["SECURITY_NAME"]
        df_temp["ExchangeID"] = df_stock["THSCODE"].apply(
            lambda x: "SSE" if x.startswith("6") else "SZSE")
        df_temp["Code"] = df_temp["InstrumentID"] + df_temp["ProductClass"]
        df_temp = df_temp[~df_temp["Code"].isin(df_db["Code"])]
        print(df_temp)
        if not df_temp.empty and len(df_temp["Code"]) > 0:
            df_save = df_temp[order]
            stock_helper.save(table_name="stock", df=df_save)

            industry_id, last_df = get_industry(stock_helper, "new")
            df_new = pd.DataFrame()
            df_new["Code"] = df_save["InstrumentID"].apply(lambda x: "{0}.SK".format(x))
            df_new["IndustryID"] = df_save["InstrumentID"].apply(lambda x: industry_id)
            df_new["Date"] = df_save["InstrumentID"].apply(lambda x: date.today())
            stock_helper.save(table_name="stock_industry", df=df_new)
        print("股票全市场列表更新成功")


@check_trade_day
def update_index_component(ths_api, today_str):
    """
    更新指数成分
    :return:None
    """
    stock_helper = mysql_db.MysqlDBBase(host=MYSQL_STOCK_CONFIG["host"],
                                        port=MYSQL_STOCK_CONFIG["port"],
                                        user_id=MYSQL_STOCK_CONFIG["user"],
                                        password=MYSQL_STOCK_CONFIG["password"],
                                        db_name=MYSQL_STOCK_CONFIG["db"])
    index_list = ["000905.SH", "000300.SH", "000016.SH", "000852.SH"]
    for index_code in index_list:
        industry_id, last_df = get_industry(stock_helper, index_code)
        # print(last_df)
        df_stock = ths_api.get_index_component(index_code, today_str)
        if not df_stock.empty:
            df_stock = df_stock.fillna(value=0)
            order = ["Code", "IndustryID", "Date", "Weight"]
            df_stock["IndustryID"] = df_stock["THSCODE"].apply(lambda x: industry_id)
            df_stock.rename(
                columns={'THSCODE': 'Code', 'IndustryID': 'IndustryID', 'DATE': 'Date',
                         'WEIGHT': 'Weight'},
                inplace=True)
            df_stock = df_stock.sort_values(by="Code", axis=0, ascending=True)
            df_stock = df_stock[order]
            if not last_df.empty and len(df_stock["Code"]) == len(last_df["Code"]):
                all_code = (np.array(df_stock["Code"]) == np.array(last_df["Code"])).all()
                all_weight = (np.array(df_stock["Weight"]) == np.array(last_df["Weight"])).all()
                if not all_code or not all_weight:
                    stock_helper.save(table_name="stock_industry", df=df_stock)
            else:
                stock_helper.save(table_name="stock_industry", df=df_stock)
        print("指数：{0}更新成功".format(index_code))


@check_trade_day
def update_st(ths_api, today_str):
    """
    增量更新st
    :return:
    """
    stock_helper = mysql_db.MysqlDBBase(host=MYSQL_STOCK_CONFIG["host"],
                                        port=MYSQL_STOCK_CONFIG["port"],
                                        user_id=MYSQL_STOCK_CONFIG["user"],
                                        password=MYSQL_STOCK_CONFIG["password"],
                                        db_name=MYSQL_STOCK_CONFIG["db"])
    for block_id in ST_BLOCK_IDS:
        industry_id, last_df = get_industry(stock_helper, block_id)
        # print(last_df)
        df_st = ths_api.get_block_stocks(block_id, today_str)
        if not df_st.empty:
            df_st = df_st.fillna(value=0)
            order = ["Code", "IndustryID", "Date"]
            df_st["IndustryID"] = df_st["THSCODE"].apply(lambda x: industry_id)
            df_st.rename(
                columns={'THSCODE': 'Code', 'IndustryID': 'IndustryID', 'DATE': 'Date'},
                inplace=True)
            df_st = df_st.sort_values(by="Code", axis=0, ascending=True)
            df_st = df_st[order]
            if not last_df.empty and len(df_st["Code"]) == len(last_df["Code"]):
                all_code = (np.array(df_st["Code"]) == np.array(last_df["Code"])).all()
                print(all_code)
                if not all_code:
                    stock_helper.save(table_name="stock_industry", df=df_st)
            else:
                if not last_df.empty:
                    print(len(df_st["Code"]), len(last_df["Code"]))
                stock_helper.save(table_name="stock_industry", df=df_st)
        print(block_id)

    print("st更新成功")


@check_trade_day
def update_m1(ths_api, today_str):
    """
    更新分钟线
    :return:
    """
    today = date.today()
    start_str = datetime(today.year, today.month, today.day, 9, 15, 0).strftime(DATETIME_FORMAT)
    end_str = datetime(today.year, today.month, today.day, 15, 15, 0).strftime(DATETIME_FORMAT)
    stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str)
    stock_list = list(stock_list["THSCODE"])
    index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
    df_index = ths_api.get_minute_bar(index_list, start_str, end_str, M1_PARAM, M1_ORDER, is_index=True)
    df_stock = ths_api.get_minute_bar(stock_list, start_str, end_str, M1_PARAM, M1_ORDER, is_index=False)
    df_index = format_data_frame(df_index, M1_DICT, is_minute=True)
    df_stock = format_data_frame(df_stock, M1_DICT, is_minute=True)
    # todo assemble save to db
    mongodb = mongo_db.MongoDatabase(user_id=MONGO_CONFIG["user"],
                                     password=MONGO_CONFIG["password"],
                                     host=MONGO_CONFIG["host"],
                                     port=MONGO_CONFIG["port"],
                                     db_name='Stock')
    mongodb.insert_dataframe("THS_1Min", df_index)
    mongodb.insert_dataframe("THS_1Min", df_stock)


@check_trade_day
def update_day(ths_api, today_str):
    """
    更新日线
    :return:
    """
    stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str)
    stock_list = list(stock_list["THSCODE"])
    index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
    df_stock = ths_api.get_day_bar(stock_list, today_str, today_str, DAY_PARAM, DAY_ORDER, is_index=False)
    df_index = ths_api.get_day_bar(index_list, today_str, today_str, INDEX_DAY_PARAM, INDEX_DAY_ORDER,
                                   is_index=True)
    df_stock.columns = DAY_COLUMS
    df_index.columns = DAY_COLUMS
    df_index.to_csv("./index_day.csv", header=True, index=0)
    df_stock.to_csv("./stock_day.csv", header=True, index=0)
    stock_helper = mysql_db.MysqlDBBase(host=MYSQL_STOCK_CONFIG["host"],
                                        port=MYSQL_STOCK_CONFIG["port"],
                                        user_id=MYSQL_STOCK_CONFIG["user"],
                                        password=MYSQL_STOCK_CONFIG["password"],
                                        db_name=MYSQL_STOCK_CONFIG["db"])
    # todo assemble save to mysql
    stock_helper.save("", df_index)
    stock_helper.save("", df_stock)


@check_trade_day
def update_m5_full_day(ths_api, today_str):
    today = date.today()
    start_str = datetime(today.year, today.month, today.day, 9, 15, 0).strftime(
        DATETIME_FORMAT)
    end_str = datetime(today.year, today.month, today.day, 14, 50, 0).strftime(DATETIME_FORMAT)
    stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str, is_replace=False)
    stock_list = list(stock_list["THSCODE"])
    df_stock = ths_api.get_minute_bar(stock_list, start_str, end_str, M1_PARAM, M1_ORDER, interval=5,
                                      is_index=False)
    index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
    df_index = ths_api.get_minute_bar(index_list, start_str, end_str, M1_PARAM, M1_ORDER, interval=5,
                                      is_index=True)
    # todo dbName
    stock_helper = mysql_db.MysqlDBBase(host=MYSQL_STOCK_CONFIG["host"],
                                        port=MYSQL_STOCK_CONFIG["port"],
                                        user_id=MYSQL_STOCK_CONFIG["user"],
                                        password=MYSQL_STOCK_CONFIG["password"],
                                        db_name="")
    df_stock.columns = M5_COLUMNS
    df_index.columns = M5_COLUMNS
    stock_helper.save("{0}_{1}".format(today.year, today.month), df_index)
    stock_helper.save("{0}_{1}".format(today.year, today.month), df_stock)


@check_trade_day
def update_m5(ths_api, today_str, start_hour, start_minute, end_hour, end_minute, channel):
    """
    获取5分钟线保存到mongodb
    :param ths_api:
    :param today_str:
    :param start_hour:
    :param start_minute:
    :param end_hour:
    :param end_minute:
    :param channel:
    :return:
    """
    today = date.today()
    start_str = datetime(today.year, today.month, today.day, start_hour, start_minute, 0).strftime(
        DATETIME_FORMAT)
    end_str = datetime(today.year, today.month, today.day, end_hour, end_minute, 0).strftime(DATETIME_FORMAT)
    stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str, is_replace=False)
    stock_list = list(stock_list["THSCODE"])
    df_stock = ths_api.get_minute_bar(stock_list, start_str, end_str, M1_PARAM, M1_ORDER, interval=5,
                                      is_index=False)
    df_stock = format_data_frame(df_stock, M1_DICT, is_minute=True)

    index_list = ["000001.SH", "399300.SZ", "000905.SH"]
    df_index = ths_api.get_minute_bar(index_list, start_str, end_str, M1_PARAM, M1_ORDER, interval=5,
                                      is_index=True)
    df_index = format_data_frame(df_index, M1_DICT, is_minute=True)
    mongodb = mongo_db.MongoDatabase(user_id=MONGO_CONFIG["user"],
                                     password=MONGO_CONFIG["password"],
                                     host=MONGO_CONFIG["host"],
                                     port=MONGO_CONFIG["port"],
                                     db_name='Close')
    mongodb.insert_dataframe("BarM5", df_index)
    mongodb.insert_dataframe("BarM5", df_stock)

    redis_helper = RedisHelper(host=REDIS_CONFIG["host"],
                               port=REDIS_CONFIG["port"],
                               password=REDIS_CONFIG["password"],
                               channel=channel)  # 实例化
    redis_helper.publish(channel)


def format_data_frame(df, rename_dict, is_minute=True):
    """
    格式化DataFrame
    :param df:
    :param rename_dict:
    :param is_minute:
    :return:
    """
    pacific = pytz.timezone('Asia/Shanghai')
    df.rename(columns=rename_dict,
              inplace=True)
    if is_minute:
        df["Date"] = pd.to_datetime(df["Date"], format="%Y-%m-%d %H:%M")
        df["Date"] = df["Date"].apply(lambda x: pacific.localize(x - timedelta(minutes=5)))
    else:
        df["Date"] = pd.to_datetime(df["Date"], format="%Y-%m-%d")
        df["Date"] = df["Date"].apply(
            lambda x: pacific.localize(datetime.strptime(x.strftime(DATE_FORMAT), DATE_FORMAT)))
    df["_id"] = df.apply(
        lambda row: row["Code"] + "_" + row["Date"].strftime("%Y-%m-%d %H:%M:%S"), axis=1)
    order = ["_id", "Code", "Date", "Open", "High", "Low", "Close", "Volume", "OpenInterest"]
    df = df[order]
    return df


@check_trade_day
def update_realtime(ths_api, today_str):
    """
    获取实时日线插入到mongodb
    :return:
    """
    mongodb = mongo_db.MongoDatabase(user_id=MONGO_CONFIG["user"],
                                     password=MONGO_CONFIG["password"],
                                     host=MONGO_CONFIG["host"],
                                     port=MONGO_CONFIG["port"],
                                     db_name='Close')

    stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, today_str, is_replace=False)
    stock_list = list(stock_list["THSCODE"])
    df_stock = ths_api.get_realtime_bar(stock_list, REAL_PARAM, M1_ORDER, is_index=False)
    df_stock = format_data_frame(df_stock, M1_DICT, is_minute=False)
    mongodb.insert_dataframe("DayAP", df_stock)

    index_list = ["000001.SH", "399300.SZ", "000905.SH"]
    df_index = ths_api.get_realtime_bar(index_list, REAL_PARAM, M1_ORDER, is_index=True)
    df_index = format_data_frame(df_index, M1_DICT, is_minute=False)
    mongodb.insert_dataframe("DayAP", df_index)

    redis_helper = RedisHelper(host=REDIS_CONFIG["host"],
                               port=REDIS_CONFIG["port"],
                               password=REDIS_CONFIG["password"],
                               channel="close")  # 实例化
    redis_helper.publish("close")


def update_close():
    """
    更新尾盘Bar发送redis消息
    :return:
    """
    update_m5(14, 50, 14, 50, "latest")
    update_realtime()


def clear_close():
    """
    清除当天尾盘Bar
    :return:
    """
    mongodb = mongo_db.MongoDatabase(user_id=MONGO_CONFIG["user"],
                                     password=MONGO_CONFIG["password"],
                                     host=MONGO_CONFIG["host"],
                                     port=MONGO_CONFIG["port"],
                                     db_name='Close')
    mongodb.delete("BarM5", {})
    mongodb.delete("DayAP", {})


def main():
    """
    定时任务
    :return:
    """
    schedule.every().day.at("08:00").do(update_trade_day)
    schedule.every().day.at("09:00").do(update_block)

    schedule.every().day.at("14:45").do(update_m5, 9, 15, 14, 45, "history")
    schedule.every().day.at("14:50").do(update_close)
    # schedule.every().day.at("15:10").do(update_m1)
    # schedule.every().day.at("15:20").do(update_m5_full_day)
    # schedule.every().day.at("17:00").do(update_day)

    schedule.every().day.at("17:30").do(clear_close)
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    main()
    # update_m5(is_last=False)
    # print(datetime.now())
    # update_close()
    # print(datetime.now())
    # clear_close()
    # update_realtime()
