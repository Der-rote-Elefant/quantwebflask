WARN_USERS = ["DDC", ]  # ["DDC", "XZR", "ZCJ"]
M1_PARAM = "open;high;low;close;volume;amount"
M1_ORDER = ["thscode", "time", "open", "high", "low", "close", "volume", "amount"]
M1_DICT = {
    "thscode": "Code",
    "time": "Date",
    "open": "Open",
    "high": "High",
    "low": "Low",
    "close": "Close",
    "volume": "Volume",
    "amount": "OpenInterest"
}

M5_COLUMNS = ["ProductID", "date", "open", "high", "low", "close", "volume", "openInterest"]
DAY_COLUMS = ["productid", "date", "open", "high", "low", "close", "volume", "openInterest", "traded_market_value",
              "market_value", "turnover", "adjust_price", "PE_TTM", "PS_TTM", "PC_TTM", "PB", "vwap" ]
DAY_PARAM = "open,high,low,close,volume,amount,turnoverRatio,floatCapitalOfAShares,totalCapital,pe,ps,pcf,pb,avgPrice"
INDEX_DAY_PARAM = "open,high,low,close,volume,amount,turnoverRatio,floatCapital,totalCapital,pe_ttm_index,ps,pcf,pb_mrq,avgPrice"
DAY_ORDER = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'turnoverRatio', 'adj_close',
             'floatCapitalOfAShares', 'totalCapital', 'pe', 'ps', 'pcf', 'pb', 'avgPrice']

REAL_PARAM = "open;high;low;latest;volume;amount"
INDEX_DAY_ORDER = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'turnoverRatio', 'adj_close',
                   'floatCapital', 'totalCapital', 'pe_ttm_index', 'ps', 'pcf', 'pb_mrq', 'avgPrice']
ACTIVE_A_BLOCK_ID = "001005010"
INACTIVE_A_BLOCK_ID = "001005334011"
ST_BLOCK_IDS = ["001005334012", "001005334013"]
FILE_DATE_FORMAT = "%Y%m%d"
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
LATEST_STOCK_DIR = "./data/Latest/Stock/"
LATEST_INDEX_DIR = "./data/Latest/Index/"
HISTORY_STOCK_DIR = "./data/History/Stock/"
HISTORY_INDEX_DIR = "./data/History/Index/"
INDEX_LIST_PATH = "./data/index.csv"
HISTORY_DAY_PATH = "./data/history_day.csv"
HISTORY_M1_PATH = "./data/history_m1.csv"
HISTORY_START_DATE = "2010-01-01"
HISTORY_END_DATE = "2019-02-28"
THS_CONFIG = {
    "user": "bftz032",
    "password": "134159"
}
REDIS_CONFIG = {
    "host": "192.168.1.184",  # 121.40.248.25
    "port": 6379,
    "password": "Baofang660"
}
MONGO_CONFIG = {
    "user": "root",
    "password": "Baofang660",
    "host": "192.168.1.184",
    "port": 27017,
}
MYSQL_TRADE_CONFIG = {
    "user": "bftrader",
    "password": "baofangtrade220",
    "host": "rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
    "port": 3306,
    "db": "bftrader"
}
MYSQL_STOCK_CONFIG = {
    "user": "stock",
    "password": "bfstock330",
    "host": "rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
    "port": 3306,
    "db": "stock"
}
