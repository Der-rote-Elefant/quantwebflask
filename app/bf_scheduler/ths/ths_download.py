# -*- coding: utf-8 -*-

from iFinDPy import *
from datetime import datetime
from dateutil.relativedelta import relativedelta
import pandas as pd
import os

day_param = "open,high,low,close,volume,amount,turnoverRatio,floatCapitalOfAShares,totalCapital,pe,ps,pcf,pb,avgPrice"
day_index_param = "open,high,low,close,volume,amount,turnoverRatio,floatCapital,totalCapital,pe_ttm_index,ps,pcf,pb_mrq,avgPrice"
day_order = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'turnoverRatio', 'adj_close',
             'floatCapitalOfAShares', 'totalCapital', 'pe', 'ps', 'pcf', 'pb', 'avgPrice']
index_day_order = ['thscode', 'time', 'open', 'high', 'low', 'close', 'volume', 'amount', 'turnoverRatio', 'adj_close',
                   'floatCapital', 'totalCapital', 'pe_ttm_index', 'ps', 'pcf', 'pb_mrq', 'avgPrice']
active_a_block_id = "001005010"
inactive_a_block_id = "001005334011"
DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
start_date = date(2010, 1, 1)  # 设置起始日期
end_date = datetime.today()  # 设置历史结束日期
LATEST_STOCK_DIR = "./Latest/Stock/"
LATEST_INDEX_DIR = "./Latest/Index/"

history_start_date_str = start_date.strftime(DATE_FORMAT)
history_end_date_str = end_date.strftime(DATE_FORMAT)

all_stock_list = list()
all_index_list = list(pd.read_csv("./index.csv", header=None)[0])


def get_query_count():
    ths_data_count = THS_DataStatistics()
    usage = ths_data_count["tables"]["QuotesDataStat"]["usage"]
    limit = ths_data_count["tables"]["QuotesDataStat"]["limit"]
    ratio = ths_data_count["tables"]["QuotesDataStat"]["ratio"]
    return usage


def get_block_stocks(block_id):
    params = "{0};{1}".format(date.today().strftime(DATE_FORMAT), block_id)
    thsDataDataPool = THS_DataPool('block', params, 'date:Y,security_name:Y,thscode:Y')
    dataPool = THS_Trans2DataFrame(thsDataDataPool)
    return dataPool


def get_histroy_date_list():
    ths_date = THS_DateQuery('SSE', 'dateType:0,period:D,dateFormat:0', '2010-01-01', '2019-04-02')
    date_list = ths_date["tables"]["time"]
    return date_list


def replace_code(stock_code, is_index=False):
    index = stock_code.index(".")
    if not is_index:
        return "%s.SK" % stock_code[:index]
    else:
        return "%s.SKIN" % stock_code[:index]


def get_day_bar(stock_code, start_date_str, end_date_str, param, column_order):
    adj_param = "close"
    thsDataHistoryQuotes = THS_HistoryQuotes(stock_code, param,
                                             'Interval:D,CPS:1,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                             start_date_str, end_date_str)
    thsDataHistoryQuotes_adj_close = THS_HistoryQuotes(stock_code, adj_param,
                                                       'Interval:D,CPS:7,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                                       start_date_str, end_date_str)
    historyQuotes = THS_Trans2DataFrame(thsDataHistoryQuotes)
    history_adj_close = THS_Trans2DataFrame(thsDataHistoryQuotes_adj_close)
    historyQuotes["adj_close"] = history_adj_close["close"]
    historyQuotes["avgPrice"] = historyQuotes["avgPrice"]  # .apply(lambda x: '%.2f' % x)
    historyQuotes["date_time"] = historyQuotes["time"]  # .apply(lambda x: x)  # 作为行索引方便以日期过滤
    historyQuotes['date_time'] = pd.to_datetime(historyQuotes['date_time'])
    historyQuotes = historyQuotes.set_index("date_time")  # 将时间作为索引列
    historyQuotes = historyQuotes[column_order]
    return historyQuotes


def save_to_csv(stock_code, df, is_index=False):
    # 按月输出bar结果
    temp_start = start_date
    while temp_start <= end_date:
        temp_end = temp_start + relativedelta(months=+1) + relativedelta(days=-1)
        start = temp_start.strftime(DATE_FORMAT)
        end = temp_end.strftime(DATE_FORMAT)
        curr_day_bar = df[start:end]
        if not curr_day_bar.empty:
            dirs = "./Stock/%s%s/" % (str(temp_start.year), str(temp_start.month).zfill(2))
            file_name = "%s.csv" % replace_code(stock_code, is_index)
            if not os.path.exists(dirs):
                os.makedirs(dirs)
            curr_day_bar.to_csv(os.path.join(dirs, file_name), header=None, index=0)

        temp_start = temp_start + relativedelta(months=+1)


def get_all_history_day_bar(stock_list, start_date_str, end_date_str, is_index=False):
    index = 0
    for stock_code in stock_list:
        df = get_day_bar(stock_code, start_date_str, end_date_str, is_index)
        if not df.empty:
            save_to_csv(stock_code, df, is_index)
        index += 1
        if is_index:
            print("指数进度：", index, "/", len(stock_list))
        else:
            print("股票进度：", index, "/", len(stock_list))
        break


def get_stock_list(block_id_list):
    stock_list = list()
    for block_id in block_id_list:
        stock_pool = get_block_stocks(block_id)
        print(stock_pool)
        stock_list.extend(stock_pool["THSCODE"])
    return stock_list


def download_stocks_today_bar(stock_list, is_index=False):
    today_str = date.today().strftime(DATE_FORMAT)
    if is_index:
        column_order = index_day_order
    else:
        column_order = day_order
    stock_df = get_day_bar(",".join(stock_list), today_str, today_str, column_order, is_index=is_index)
    if is_index:
        stock_path = "%s%s" % (LATEST_INDEX_DIR, "Day")
    else:
        stock_path = "%s%s" % (LATEST_STOCK_DIR, "Day")
    stock_file_name = "%s.csv" % date.today().strftime("%Y-%m-%d")
    if not os.path.exists(stock_path):
        os.makedirs(stock_path)
    stock_df.to_csv(os.path.join(stock_path, stock_file_name), header=None, index=0)


def download_all_today_bar():
    ths_login = THS_iFinDLogin('sdjs003', '754042')
    if (ths_login == 0 or ths_login == -201):
        stock_list = get_stock_list([active_a_block_id, ])
        download_stocks_today_bar(stock_list, is_index=False)
        download_stocks_today_bar(all_index_list, is_index=True)
        # 登出函数
        THS_iFinDLogout()
    else:
        print("登录失败")


def main():
    # ST, 001005334012
    # *ST, 001005334013

    thsLogin = THS_iFinDLogin('sdjs003', '754042')
    if (thsLogin == 0 or thsLogin == -201):
        # all_stock_list = get_stock_list(stock_block_ids)
        # all_index_list = get_stock_list(index_block_ids)
        # get_all_history_day_bar(all_stock_list, history_start_date_str, history_end_date_str, day_order)
        # get_all_history_day_bar(all_index_list, history_start_date_str, history_end_date_str, index_day_order,
        #                         is_index=True)

        # 登出函数
        THS_iFinDLogout()
    else:
        print("登录失败")


if __name__ == '__main__':
    main()
