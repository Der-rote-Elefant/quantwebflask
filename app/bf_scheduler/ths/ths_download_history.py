# -*- coding: utf-8 -*-

from iFinDPy import *
from datetime import datetime
from datetime import date
import pandas as pd
import os
import math
from util.wechat import Wechat
from dateutil.relativedelta import relativedelta
import sys
from ths_config import *

message = Wechat(users=WARN_USERS)
current_user = ""
users = {
    'yxt046': {'password': '666888', 'is_limit': False},
    # 'sdjs003': {'password': '754042', 'is_limit': False},
    # 'sdxx003': {'password': '562090', 'is_limit': False},
    # 'nhqh016': {'password': '666888', 'is_limit': False},
    # 'nhqh062': {'password': '666888', 'is_limit': False},
    # 'mdzg001': {'password': '803315', 'is_limit': False},
    # 'bftz033': {'password': '605003', 'is_limit': False},
    # 'ctsec19029': {'password': '753427', 'is_limit': False},
    # 'ctsec19030': {'password': '439839', 'is_limit': False},
    'nhqh020': {'password': '666888', 'is_limit': False},
    'tskj011': {'password': '397357', 'is_limit': False},
    'tskj010': {'password': '435129', 'is_limit': False},

    # 'bftz032': {'password': '134159', 'is_limit': False},
}


def get_block_stocks(block_id):
    params = "{0};{1}".format(date(2019, 2, 28).strftime(DATE_FORMAT), block_id)
    thsDataDataPool = THS_DataPool('block', params, 'date:Y,security_name:Y,thscode:Y')
    dataPool = THS_Trans2DataFrame(thsDataDataPool)
    return dataPool


def replace_code(stock_code, is_index=False):
    index = stock_code.index(".")
    if not is_index:
        return "%s.SK" % stock_code[:index]
    else:
        return "%s.SKIN" % stock_code[:index]


def get_day_bar(stock_code, start_date_str, end_date_str, param, column_order):
    adj_param = "close"
    thsDataHistoryQuotes = THS_HistoryQuotes(stock_code, param,
                                             'Interval:D,CPS:1,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                             start_date_str, end_date_str)
    thsDataHistoryQuotes_adj_close = THS_HistoryQuotes(stock_code, adj_param,
                                                       'Interval:D,CPS:7,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                                       start_date_str, end_date_str)
    historyQuotes = THS_Trans2DataFrame(thsDataHistoryQuotes)
    history_adj_close = THS_Trans2DataFrame(thsDataHistoryQuotes_adj_close)
    historyQuotes["adj_close"] = history_adj_close["close"]
    historyQuotes["date_time"] = historyQuotes["time"]  # .apply(lambda x: x)  # 作为行索引方便以日期过滤
    historyQuotes['date_time'] = pd.to_datetime(historyQuotes['date_time'])
    historyQuotes = historyQuotes.set_index("date_time")  # 将时间作为索引列
    historyQuotes = historyQuotes[column_order]
    return historyQuotes


def get_m1_bar(stock_code, start_date_str, end_date_str, param, column_order):
    single_year = 2
    start_time = datetime.strptime(start_date_str, DATETIME_FORMAT)
    end_time = datetime.strptime(end_date_str, DATETIME_FORMAT)
    query_count = math.ceil((end_time.year - start_time.year + 1) / single_year)
    df_m1 = pd.DataFrame()
    for i in range(query_count):
        temp_start_time = start_time + relativedelta(years=+(i * single_year))
        start_time_str = temp_start_time.strftime(DATETIME_FORMAT)
        if i == query_count - 1:
            end_time_str = end_time.strftime(DATETIME_FORMAT)
        else:
            temp_end_time = temp_start_time + relativedelta(years=+single_year) + relativedelta(days=-1)
            temp_end_time = datetime(temp_end_time.year, temp_end_time.month, temp_end_time.day, 15, 15, 0)
            end_time_str = temp_end_time.strftime(DATETIME_FORMAT)

        print(stock_code, param, start_time_str, end_time_str)
        ths_m1_sequence = THS_HighFrequenceSequence(stock_code, param,
                                                    'CPS:0,MaxPoints:50000,Fill:Previous,Interval:1',
                                                    start_time_str, end_time_str)
        if ths_m1_sequence["errorcode"] == 0:
            ths_m1 = THS_Trans2DataFrame(ths_m1_sequence)
            if not ths_m1.empty:
                ths_m1["date_time"] = ths_m1["time"]  # .apply(lambda x: x)  # 作为行索引方便以日期过滤
                ths_m1['date_time'] = pd.to_datetime(ths_m1['date_time'])
                ths_m1 = ths_m1.set_index("date_time")  # 将时间作为索引列
                df_m1 = df_m1.append(ths_m1)
        elif ths_m1_sequence["errorcode"] == -4302:
            global current_user
            print(current_user + "数据超出限制")
            message.send_message(current_user + "数据超出限制")
            users[current_user]["is_limit"] = True
            THS_iFinDLogout()
            result = login()
            if result:
                ths_m1_sequence = THS_HighFrequenceSequence(stock_code, param,
                                                            'CPS:0,MaxPoints:50000,Fill:Previous,Interval:1',
                                                            start_time_str, end_time_str)
                if ths_m1_sequence["errorcode"] == 0:
                    ths_m1 = THS_Trans2DataFrame(ths_m1_sequence)
                    if not ths_m1.empty:
                        ths_m1["date_time"] = ths_m1["time"]  # .apply(lambda x: x)  # 作为行索引方便以日期过滤
                        ths_m1['date_time'] = pd.to_datetime(ths_m1['date_time'])
                        ths_m1 = ths_m1.set_index("date_time")  # 将时间作为索引列
                        df_m1 = df_m1.append(ths_m1)
            else:
                message.send_message("数据超出限制")
                sys.exit(0)

    if not df_m1.empty:
        df_m1.sort_index()
        df_m1 = df_m1[column_order]
    return df_m1


def get_stock_list(block_id_list):
    stock_list = list()
    for block_id in block_id_list:
        stock_pool = get_block_stocks(block_id)
        stock_list.extend(stock_pool["THSCODE"])
    return stock_list


def save_to_csv(stock_code, df, file_path, history_path, is_index=False):
    # 按月输出bar结果
    start_date = datetime.strptime(HISTORY_START_DATE, DATE_FORMAT)
    end_date = datetime.strptime(HISTORY_END_DATE, DATE_FORMAT)
    temp_start = start_date
    while temp_start <= end_date:
        temp_end = temp_start + relativedelta(months=+1) + relativedelta(days=-1)
        start = temp_start.strftime(DATE_FORMAT)
        end = temp_end.strftime(DATE_FORMAT)
        curr_df = df[start:end]
        if not curr_df.empty:
            dir_name = "%s%s/" % (str(temp_start.year), str(temp_start.month).zfill(2))
            dir_name = os.path.join(file_path, dir_name)
            file_name = "%s.csv" % replace_code(stock_code, is_index)
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)
            curr_df.to_csv(os.path.join(dir_name, file_name), header=None, index=0)

        temp_start = temp_start + relativedelta(months=+1)

    if not df.empty:
        df_record = pd.read_csv(history_path, header=None)
        df_record = df_record.append([stock_code, ])
        df_record.to_csv(history_path, header=None, index=0)


def download_stocks_day_bar(stock_list, is_index=False):
    if is_index:
        param = INDEX_DAY_PARAM
        column_order = list(INDEX_DAY_ORDER)
        stock_path = "%s%s" % (HISTORY_INDEX_DIR, "Day")
    else:
        param = DAY_PARAM
        column_order = list(DAY_ORDER)
        stock_path = "%s%s" % (HISTORY_STOCK_DIR, "Day")

    column_order.remove("thscode")
    current = 0
    for stock_code in stock_list:
        df_record = pd.read_csv(HISTORY_DAY_PATH, header=None)
        if stock_code not in df_record[0].values:
            try:
                stock_df = get_day_bar(stock_code, HISTORY_START_DATE, HISTORY_END_DATE, param, column_order)
                save_to_csv(stock_code, stock_df, stock_path, HISTORY_DAY_PATH, is_index=is_index)
            except Exception as e:
                print(stock_code)
                print(e)

        current += 1
        print("%d/%d" % (current, len(stock_list)))


def download_stocks_m1_bar(stock_list, is_index=False):
    if is_index:
        stock_path = "%s%s" % (HISTORY_INDEX_DIR, "M1")
    else:
        stock_path = "%s%s" % (HISTORY_STOCK_DIR, "M1")

    start_time = datetime.strptime(HISTORY_START_DATE, DATE_FORMAT)
    end_time = datetime.strptime(HISTORY_END_DATE, DATE_FORMAT)
    start_str = datetime(start_time.year, start_time.month, start_time.day, 9, 15, 0).strftime(DATETIME_FORMAT)
    end_str = datetime(end_time.year, end_time.month, end_time.day, 15, 15, 0).strftime(DATETIME_FORMAT)
    m1_order = list(M1_ORDER)
    m1_order.remove("thscode")
    current = 0
    for stock_code in stock_list:
        df_record = pd.read_csv(HISTORY_M1_PATH, header=None)
        if df_record.empty or stock_code not in df_record[0].values:
            try:
                stock_df = get_m1_bar(stock_code, start_str, end_str, M1_PARAM, m1_order)
                save_to_csv(stock_code, stock_df, stock_path, HISTORY_M1_PATH, is_index=is_index)
            except Exception as e:
                print(stock_code)
                print(e)

        current += 1
        print("%d/%d" % (current, len(stock_list)))


def download_all_day_bar(stock_list, index_list):
    download_stocks_day_bar(index_list, is_index=True)
    download_stocks_day_bar(stock_list, is_index=False)


def download_all_m1_bar(stock_list, index_list):
    # download_stocks_m1_bar(index_list, is_index=True)
    download_stocks_m1_bar(stock_list, is_index=False)


def login():
    for user in users.keys():
        if not users[user]["is_limit"]:
            print(user, users[user]["password"])
            login_result = THS_iFinDLogin(user, users[user]["password"])
            global current_user
            if login_result == 0 or login_result == -201:
                current_user = user
                break
            else:
                current_user = ""
                users[user]["is_limit"] = True
                break
        else:
            current_user = ""

    return current_user != ""


def download_history():
    try:
        print('download_latest-startTime:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        # login_result = THS_iFinDLogin('mdzg001', '803315')
        # if login_result == 0 or login_result == -201:
        result = login()
        if result:
            stock_list = get_stock_list([ACTIVE_A_BLOCK_ID, INACTIVE_A_BLOCK_ID])
            df_stock = pd.DataFrame(stock_list)
            df_stock.to_csv("./data/stock.csv", header=None, index=0)
            index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
            # download_all_day_bar(stock_list, index_list)
            download_all_m1_bar(stock_list, index_list)

            # 登出函数
            THS_iFinDLogout()
        else:
            print("登录失败")
            error_msg = "同花顺下载历史登录失败"
            message.send_message(error_msg)

        print('download_latest:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    except Exception as e:
        error_msg = "同花顺下载历史数据出错：%s" % e
        message.send_message(error_msg)
        raise e


def main():
    download_history()


if __name__ == '__main__':
    main()
