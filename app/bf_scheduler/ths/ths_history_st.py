from iFinDPy import *
from datetime import date
import pandas as pd
from ths_config import *

st_block_ids = ["001005334012", "001005334013"]


def get_block_stocks(block_id, date):
    params = "{0};{1}".format(date, block_id)
    thsDataDataPool = THS_DataPool('block', params, 'date:Y,security_name:Y,thscode:Y')
    dataPool = THS_Trans2DataFrame(thsDataDataPool)
    return dataPool


def get_histroy_date_list():
    ths_date = THS_DateQuery('SSE', 'dateType:0,period:D,dateFormat:0', '2010-01-01',
                             date.today().strftime(DATE_FORMAT))
    date_list = ths_date["tables"]["time"]
    return date_list


def main():
    ths_login = THS_iFinDLogin('yxt046', '666888')
    if (ths_login == 0 or ths_login == -201):
        date_list = get_histroy_date_list()
        i = 0
        for date in date_list:
            for st_block_id in st_block_ids:
                df = get_block_stocks(st_block_id, date)
                df.to_csv("./data/history_st.csv", header=None, index=0, mode='a+')
            i += 1
            print("%d/%d" % (i, len(date_list)))


if __name__ == '__main__':
    main()
