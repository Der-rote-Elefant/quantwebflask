# -*- coding: utf-8 -*-

from iFinDPy import *
from datetime import datetime
import pandas as pd
import os
import math
import schedule
from util.wechat import Wechat
from util.ths_api import ThsApi
from ths_config import *
import time
from util import mysql_db
import numpy as np


def update_trade_day():
    ths_api = ThsApi("bftz032", "134159")
    if ths_api.login():
        date_str = date.today().strftime(DATE_FORMAT)
        is_trade_day = ths_api.is_trade_day(date_str)
        if is_trade_day:
            mysql_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                                port=3306,
                                                user_id="bftrader",
                                                password="baofangtrade220",
                                                db_name="bftrader")
            df = pd.DataFrame({"Date": [date.today(), ]})
            mysql_helper.save(table_name="trade_date", df=df)

        ths_api.logout()
    else:
        pass


def update_block():
    update_index_component(["000016.SH", "000300.SH", "000852.SZ", "000905.SZ"])
    update_stock_list()
    update_st()


def update_credit_list():
    date_str = date.today().strftime(DATE_FORMAT)
    mysql_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                        port=3306,
                                        user_id="bftrader",
                                        password="baofangtrade220",
                                        db_name="bftrader")

    df_date = mysql_helper.query("select Date from trade_date")
    today_str = df_date.iat[-1, -1].strftime(DATE_FORMAT)
    if today_str == date_str:
        ths_api = ThsApi("mdzg001", "803315")
        if ths_api.login():
            stock_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                                port=3306,
                                                user_id="stock",
                                                password="bfstock330",
                                                db_name="stock")
            df_industry = stock_helper.query(
                'select Id,Name,industrycode from industry where industrycode=%(industrycode)s',
                params={'industrycode': "rzrq"})
            industry_id = df_industry["Id"].iat[-1]
            last_df = pd.DataFrame()
            df_stock = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, date.today().strftime(DATE_FORMAT),
                                                is_replace=False)
            df_rzrq = ths_api.get_basic_data(",".join(df_stock["THSCODE"]),
                                             date.today().strftime(DATE_FORMAT))
            df_rzrq = df_rzrq[df_rzrq["ths_is_mt_ss_underlying_stock"].isin(["是", ])]
            order = ["Code", "IndustryID", "Date"]
            df_rzrq["IndustryID"] = df_rzrq["thscode"].apply(lambda x: industry_id)
            df_rzrq["Date"] = df_rzrq["thscode"].apply(lambda x: date.today())
            df_rzrq.rename(
                columns={'thscode': 'Code', 'IndustryID': 'IndustryID', 'Date': 'Date'},
                inplace=True)
            df_rzrq = df_rzrq.sort_values(by="Code", axis=0, ascending=True)
            df_rzrq = df_rzrq[order]
            print(df_rzrq)
            if not last_df.empty and len(df_rzrq["Code"]) == len(last_df["Code"]):
                all_code = (np.array(df_rzrq["Code"]) == np.array(df_rzrq["Code"])).all()
                if not all_code:
                    stock_helper.save(table_name="stock_industry", df=df_rzrq)
            else:
                stock_helper.save(table_name="stock_industry", df=df_rzrq)

            last_df = df_rzrq

            ths_api.logout()
        else:
            pass


def update_stock_list():
    date_str = date.today().strftime(DATE_FORMAT)
    mysql_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                        port=3306,
                                        user_id="bftrader",
                                        password="baofangtrade220",
                                        db_name="bftrader")

    df_date = mysql_helper.query("select Date from trade_date")
    today_str = df_date.iat[-1, -1].strftime(DATE_FORMAT)
    if today_str == date_str:
        ths_api = ThsApi("nhqh016", "666888")
        if ths_api.login():
            stock_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                                port=3306,
                                                user_id="stock",
                                                password="bfstock330",
                                                db_name="stock")
            df_db = stock_helper.query(
                'select ProductID,ProductClass,InstrumentID,InstrumentName,ExchangeID from stock')
            df_db["Code"] = df_db["InstrumentID"] + df_db["ProductClass"]
            order = ["ProductID", "ProductClass", "InstrumentID", "InstrumentName", "ExchangeID"]
            for date1 in df_date["Date"]:
                if datetime.strptime(date1.strftime(DATE_FORMAT), DATE_FORMAT) >= datetime(2016, 6, 28, 0, 0, 0):
                    df_stock = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, date1.strftime(DATE_FORMAT))
                    if not df_stock.empty:
                        df_stock = df_stock.fillna(value=0)
                        df_temp = pd.DataFrame()
                        df_temp["ProductID"] = df_stock["THSCODE"].apply(lambda x: x[:x.index(".")])
                        df_temp["ProductClass"] = df_stock["THSCODE"].apply(lambda x: "SK")
                        df_temp["InstrumentID"] = df_temp["ProductID"]
                        df_temp["InstrumentName"] = df_stock["SECURITY_NAME"]
                        df_temp["ExchangeID"] = df_stock["THSCODE"].apply(
                            lambda x: "SSE" if x.startswith("6") else "SZSE")
                        df_temp["Code"] = df_temp["InstrumentID"] + df_temp["ProductClass"]
                        df_temp = df_temp[~df_temp["Code"].isin(df_db["Code"])]

                        print(df_temp)
                        if not df_temp.empty and len(df_temp["Code"]) > 0:
                            df_save = df_temp[order]
                            stock_helper.save(table_name="stock", df=df_save)

                        df_db = df_db.append(df_temp)
                print(date1)

            ths_api.logout()
        else:
            pass


def update_index_component(index_list):
    date_str = date.today().strftime(DATE_FORMAT)
    mysql_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                        port=3306,
                                        user_id="bftrader",
                                        password="baofangtrade220",
                                        db_name="bftrader")

    df_date = mysql_helper.query("select Date from trade_date")
    today_str = df_date.iat[-1, -1].strftime(DATE_FORMAT)
    if today_str == date_str:
        ths_api = ThsApi("tskj010", "435129")
        if ths_api.login():
            stock_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                                port=3306,
                                                user_id="stock",
                                                password="bfstock330",
                                                db_name="stock")
            for index_code in index_list:
                df_industry = stock_helper.query(
                    'select Id,Name,industrycode from industry where industrycode=%(industrycode)s',
                    params={'industrycode': index_code})
                industry_id = df_industry["Id"].iat[-1]
                last_df = pd.DataFrame()
                for date1 in df_date["Date"]:
                    df_stock = ths_api.get_index_component(index_code, date1.strftime(DATE_FORMAT))
                    if not df_stock.empty:
                        df_stock = df_stock.fillna(value=0)
                        order = ["Code", "IndustryID", "Date", "Weight"]
                        df_stock["IndustryID"] = df_stock["THSCODE"].apply(lambda x: industry_id)
                        df_stock.rename(
                            columns={'THSCODE': 'Code', 'IndustryID': 'IndustryID', 'DATE': 'Date',
                                     'WEIGHT': 'Weight'},
                            inplace=True)
                        df_stock = df_stock.sort_values(by="Code", axis=0, ascending=True)
                        df_stock = df_stock[order]
                        if not last_df.empty and len(df_stock["Code"]) == len(last_df["Code"]):
                            all_code = (np.array(df_stock["Code"]) == np.array(last_df["Code"])).all()
                            all_weight = (np.array(df_stock["Weight"]) == np.array(last_df["Weight"])).all()
                            if not all_code or not all_weight:
                                stock_helper.save(table_name="stock_industry", df=df_stock)
                        else:
                            stock_helper.save(table_name="stock_industry", df=df_stock)

                    last_df = df_stock
                    print(date1)

        ths_api.logout()
    else:
        pass


def update_index_list():
    ths_api = ThsApi("bftz032", "134159")
    if ths_api.login():
        date_str = date.today().strftime(DATE_FORMAT)
        is_trade_day = ths_api.is_trade_day(date_str)
        if is_trade_day:
            df_stock = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID, date_str)
            # todo df_stock.to_sql()

        ths_api.logout()
    else:
        pass


def update_st():
    date_str = date.today().strftime(DATE_FORMAT)
    mysql_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                        port=3306,
                                        user_id="bftrader",
                                        password="baofangtrade220",
                                        db_name="bftrader")

    df_date = mysql_helper.query("select Date from trade_date")
    today_str = df_date.iat[-1, -1].strftime(DATE_FORMAT)
    if today_str == date_str:
        ths_api = ThsApi("tskj010", "435129")
        if ths_api.login():
            stock_helper = mysql_db.MysqlDBBase(host="rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com",
                                                port=3306,
                                                user_id="stock",
                                                password="bfstock330",
                                                db_name="stock")

            for block_id in ST_BLOCK_IDS:
                df_industry = stock_helper.query(
                    'select Id,Name,industrycode from industry where industrycode=%(industrycode)s',
                    params={'industrycode': block_id})
                industry_id = df_industry["Id"].iat[-1]
                last_df = pd.DataFrame()
                for date1 in df_date["Date"]:
                    df_st = ths_api.get_block_stocks(block_id, date1.strftime(DATE_FORMAT))
                    if not df_st.empty:
                        df_st = df_st.fillna(value=0)
                        order = ["Code", "IndustryID", "Date"]
                        df_st["IndustryID"] = df_st["THSCODE"].apply(lambda x: industry_id)
                        df_st.rename(
                            columns={'THSCODE': 'Code', 'IndustryID': 'IndustryID', 'DATE': 'Date'},
                            inplace=True)
                        df_st = df_st.sort_values(by="Code", axis=0, ascending=True)
                        df_st = df_st[order]
                        if not last_df.empty:
                            if len(df_st["Code"]) == len(last_df["Code"]):
                                all_code = (np.array(df_st["Code"]) == np.array(last_df["Code"])).all()
                                print(all_code)
                                if not all_code:
                                    stock_helper.save(table_name="stock_industry", df=df_st)
                            else:
                                print(len(df_st["Code"]), len(last_df["Code"]))
                                stock_helper.save(table_name="stock_industry", df=df_st)
                        else:
                            if not last_df.empty:
                                print(len(df_st["Code"]), len(last_df["Code"]))
                            stock_helper.save(table_name="stock_industry", df=df_st)

                        last_df = df_st
                    print(date1)

        ths_api.logout()
    else:
        pass


def update_m1():
    ths_api = ThsApi("bftz032", "134159")
    if ths_api.login():
        date_str = date.today().strftime(DATE_FORMAT)
        is_trade_day = ths_api.is_trade_day(date_str)
        if is_trade_day:
            today = date.today()
            start_str = datetime(today.year, today.month, today.day, 9, 15, 0).strftime(DATETIME_FORMAT)
            end_str = datetime(today.year, today.month, today.day, 15, 15, 0).strftime(DATETIME_FORMAT)
            stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID)
            index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
            df_index = ths_api.get_m1_bar(index_list, start_str, end_str, M1_PARAM, M1_ORDER, is_index=True)
            df_stock = ths_api.get_m1_bar(stock_list, start_str, end_str, M1_PARAM, M1_ORDER, is_index=False)
            # todo assemble save to mysql

        ths_api.logout()
    else:
        pass


def update_day():
    ths_api = ThsApi("bftz032", "134159")
    if ths_api.login():
        date_str = date.today().strftime(DATE_FORMAT)
        is_trade_day = ths_api.is_trade_day(date_str)
        if is_trade_day:
            today_str = date.today().strftime(DATE_FORMAT)
            stock_list = ths_api.get_block_stocks(ACTIVE_A_BLOCK_ID)
            index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
            stock_df = ths_api.get_day_bar(stock_list, today_str, today_str, DAY_PARAM, DAY_ORDER, is_index=False)
            index_df = ths_api.get_day_bar(index_list, today_str, today_str, INDEX_DAY_PARAM, INDEX_DAY_ORDER,
                                           is_index=True)
        # todo assemble save to mysql

        ths_api.logout()
    else:
        pass


def main():
    schedule.every().day.at("08:00").do(update_trade_day)
    schedule.every().day.at("09:00").do(update_block)
    schedule.every().day.at("15:10").do(update_m1)
    schedule.every().day.at("15:30").do(update_day)
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    # main()
    # update_trade_day()
    # update_index_component(["000905.SH", "000300.SH", "000016.SH", "000852.SH"])
    # update_st()
    # update_stock_list()
    update_credit_list()
