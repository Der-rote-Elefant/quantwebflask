# -*- coding: utf-8 -*-

from iFinDPy import *
from datetime import datetime
import pandas as pd
import os
import math
import schedule
from util.wechat import Wechat
from ths_config import *
import time

message = Wechat(users=WARN_USERS)
active_stock_list = list()
user_id = "bftz032"
password = "134159"


def ths_login(user, pwd):
    login_result = THS_iFinDLogin(user, pwd)
    if login_result == 0 or login_result == -201:
        return True
    else:
        error_msg = "'同花顺登录失败-error:%d-Time:%s' % (login_result, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))"
        print(error_msg)
        message.send_message(error_msg)
        return False


def get_block_stocks(block_id):
    params = "{0};{1}".format(date.today().strftime(DATE_FORMAT), block_id)
    thsDataDataPool = THS_DataPool('block', params, 'date:Y,security_name:Y,thscode:Y')
    dataPool = THS_Trans2DataFrame(thsDataDataPool)
    return dataPool


def replace_code(stock_code, is_index=False):
    index = stock_code.index(".")
    if not is_index:
        return "%s.SK" % stock_code[:index]
    else:
        return "%s.SKIN" % stock_code[:index]


def get_day_bar(stock_code, start_date_str, end_date_str, param, column_order, is_index=False):
    adj_param = "close"
    thsDataHistoryQuotes = THS_HistoryQuotes(stock_code, param,
                                             'Interval:D,CPS:1,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                             start_date_str, end_date_str)
    thsDataHistoryQuotes_adj_close = THS_HistoryQuotes(stock_code, adj_param,
                                                       'Interval:D,CPS:7,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                                       start_date_str, end_date_str)
    historyQuotes = THS_Trans2DataFrame(thsDataHistoryQuotes)
    history_adj_close = THS_Trans2DataFrame(thsDataHistoryQuotes_adj_close)
    historyQuotes["thscode"] = historyQuotes["thscode"].apply(lambda x: replace_code(x, is_index))
    historyQuotes["adj_close"] = history_adj_close["close"]
    historyQuotes = historyQuotes[column_order]
    return historyQuotes


def get_m1_bar(stock_list, start_date_str, end_date_str, param, column_order, is_index=False):
    single_count = 500
    query_count = math.ceil(len(stock_list) / single_count)
    df_m1 = pd.DataFrame()
    for i in range(query_count):
        if i + 1 * single_count > len(stock_list):
            query_list = stock_list[i * single_count:len(stock_list)]
        else:
            query_list = stock_list[i * single_count:(i + 1) * single_count]
        ths_m1_sequence = THS_HighFrequenceSequence(",".join(query_list), param,
                                                    'CPS:0,MaxPoints:50000,Fill:Previous,Interval:1',
                                                    start_date_str, end_date_str)
        ths_m1 = THS_Trans2DataFrame(ths_m1_sequence)
        ths_m1["thscode"] = ths_m1["thscode"].apply(lambda x: replace_code(x, is_index=is_index))
        df_m1 = df_m1.append(ths_m1)

    df_m1 = df_m1[column_order]
    return df_m1


def get_stock_list(block_id_list):
    stock_list = list()
    for block_id in block_id_list:
        stock_pool = get_block_stocks(block_id)
        stock_list.extend(stock_pool["THSCODE"])
    return stock_list


def download_stocks_day_bar(stock_list, is_index=False):
    today_str = date.today().strftime(DATE_FORMAT)
    if is_index:
        param = INDEX_DAY_PARAM
        column_order = INDEX_DAY_ORDER
        stock_path = "%s%s" % (LATEST_INDEX_DIR, "Day")
    else:
        param = DAY_PARAM
        column_order = DAY_ORDER
        stock_path = "%s%s" % (LATEST_STOCK_DIR, "Day")

    if not os.path.exists(stock_path):
        os.makedirs(stock_path)

    stock_df = get_day_bar(",".join(stock_list), today_str, today_str, param, column_order, is_index=is_index)
    stock_file_name = "%s.csv" % date.today().strftime(FILE_DATE_FORMAT)
    stock_df.to_csv(os.path.join(stock_path, stock_file_name), header=None, index=0)


def download_stocks_m1_bar(stock_list, is_index=False):
    today = date.today()
    start_str = datetime(today.year, today.month, today.day, 9, 15, 0).strftime(DATETIME_FORMAT)
    end_str = datetime(today.year, today.month, today.day, 15, 15, 0).strftime(DATETIME_FORMAT)
    stock_df = get_m1_bar(stock_list, start_str, end_str, M1_PARAM, M1_ORDER, is_index=is_index)
    if is_index:
        stock_path = "%s%s" % (LATEST_INDEX_DIR, "M1")
    else:
        stock_path = "%s%s" % (LATEST_STOCK_DIR, "M1")

    if not os.path.exists(stock_path):
        os.makedirs(stock_path)

    stock_file_name = "%s.csv" % today.strftime(FILE_DATE_FORMAT)
    stock_df.to_csv(os.path.join(stock_path, stock_file_name), header=None, index=0)


def download_all_day_bar(stock_list, index_list):
    download_stocks_day_bar(stock_list, is_index=False)
    download_stocks_day_bar(index_list, is_index=True)


def download_all_m1_bar(stock_list, index_list):
    download_stocks_m1_bar(stock_list, is_index=False)
    download_stocks_m1_bar(index_list, is_index=True)


def download_latest():
    try:
        print('download_latest-startTime:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        login_result = THS_iFinDLogin('bftz032', '134159')
        if login_result == 0 or login_result == -201:
            date_offset = THS_DateOffset('SSE', 'dateType:0,period:D,offset:-0,dateFormat:0',
                                         date.today().strftime(DATE_FORMAT))
            if len(date_offset["tables"]) > 0 and date_offset["tables"]["time"][0] == date.today().strftime(
                    DATE_FORMAT):
                stock_list = get_stock_list([ACTIVE_A_BLOCK_ID, ])
                index_list = list(pd.read_csv(INDEX_LIST_PATH, header=None)[0])
                download_all_day_bar(stock_list, index_list)
                download_all_m1_bar(stock_list, index_list)
                message.send_message("%s数据下载成功" % date.today().strftime(DATE_FORMAT))
            # 登出函数
            THS_iFinDLogout()
        else:
            print("登录失败")
            error_msg = "同花顺下载当日登录失败"
            message.send_message(error_msg)

        print('download_latest:%s' % (datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    except Exception as e:
        error_msg = "同花顺当天下载数据出错：%s" % e
        message.send_message(error_msg)
        raise e


def main():
    schedule.every().day.at("17:00").do(download_latest)
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    main()
