# -*- coding: utf-8 -*-
import pandas as pd
import pymongo


class MongoDatabase(object):

    def __init__(self, user_id, password, host, port, db_name):
        url = "mongodb://{0}:{1}@{2}".format(user_id, password, host)
        self.conn = pymongo.MongoClient(host=url, port=port)
        self.db = self.conn[db_name]

    def get_state(self):
        return self.conn is not None and self.db is not None

    def insert_one(self, collection, data):
        if self.get_state():
            ret = self.db[collection].insert_one(data)
            return ret.inserted_id
        else:
            return ""

    def insert_dataframe(self, collection, df):
        if self.get_state():
            data = self.__dataframe_to_json(df)
            ret = self.db[collection].insert_many(data, False)
            return ret
        else:
            return ""

    def insert_many(self, collection, data):
        if self.get_state():
            ret = self.db[collection].insert_many(data, False)
            return ret.inserted_ids
        else:
            return ""

    def update(self, collection, upkeydate, data, upsert=False):
        # data format:
        # {key:[old_data,new_data]}
        data_filter = {}
        data_revised = {}
        for key in upkeydate.keys():
            data_filter[key] = upkeydate[key][1]
        for key in data.keys():
            data_revised[key] = data[key]
        if self.get_state():
            return self.db[collection].update_many(data_filter, {"$set": data_revised}, upsert).modified_count

        return 0

    def find(self, col, condition, column=None):
        if self.get_state():
            if column is None:
                return self.db[col].find(condition)
            else:
                return self.db[col].find(condition, column)
        else:
            return None

    def delete(self, col, condition):
        if self.get_state():
            return self.db[col].delete_many(filter=condition).deleted_count
        return 0

    def __dataframe_to_json(self, df):
        result = list()
        columns = df.columns.values.tolist()
        for index, row in df.iterrows():
            row_dict = dict()
            for column in columns:
                row_dict[column] = row[column]
            result.append(row_dict)
        return result


if __name__ == '__main__':
    db = MongoDatabase('root', 'Baofang660', '192.168.1.184', 27017, 'Close')
    print(db.get_state())
