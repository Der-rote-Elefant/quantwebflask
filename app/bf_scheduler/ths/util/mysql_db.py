# -*- coding: utf-8 -*-
import pandas as pd
from sqlalchemy import create_engine
import sqlalchemy
import warnings

warnings.filterwarnings("ignore")


class MysqlDBBase(object):
    str_list = ["productid", "Code"]
    date_list = ["date", "Date"]
    long_n2_list = ["openInterest"]
    n2_list = ['open', 'high', 'low', 'close', 'volume', 'adjust_price', 'vwap', 'traded_market_value', 'market_value']
    n6_list = ['turnover', 'PE_TTM', 'PS_TTM', 'PC_TTM', 'PB']

    def __init__(self, host, port, user_id, password, db_name):
        self.db_name = db_name
        # conn_str = 'mysql+mysqldb://stock:bfstock330@rm-bp1l731gac61ue24qo.mysql.rds.aliyuncs.com:3306/%s?charset=utf8' % db_name
        self.conn = create_engine(
            "mysql+mysqldb://{0}:{1}@{2}:{3}/{4}?charset=utf8".format(user_id, password, host, port, db_name))

    def __mapping_df_types(self, df):
        type_dict = {}
        for i, j in zip(df.columns, df.dtypes):
            if str(i) in MysqlDBBase.str_list:
                type_dict.update({i: sqlalchemy.String(20)})
            if str(i) in MysqlDBBase.date_list:
                type_dict.update({i: sqlalchemy.DateTime()})
            if str(i) in MysqlDBBase.long_n2_list:
                type_dict.update({i: sqlalchemy.Numeric(25, 2)})
            if str(i) in MysqlDBBase.n2_list:
                type_dict.update({i: sqlalchemy.Numeric(18, 2)})
            if str(i) in MysqlDBBase.n6_list:
                type_dict.update({i: sqlalchemy.Numeric(18, 6)})
        return type_dict

    def query(self, sql_cmd, params=None):
        df = pd.read_sql(sql=sql_cmd, con=self.conn, params=params)
        return df

    def save(self, table_name, df):
        type_dict = self.__mapping_df_types(df)
        df.to_sql(table_name, con=self.conn, if_exists='append', index=False, dtype=type_dict)
