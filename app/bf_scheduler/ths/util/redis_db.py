import redis


class RedisHelper:

    def __init__(self, host, port, password, channel):
        self.__conn = redis.Redis(host=host, port=port, decode_responses=True, password=password)
        self.chan_sub = channel
        self.chan_pub = channel

    def publish(self, msg):
        self.__conn.publish(self.chan_pub, msg)  # 开始发布消息

        return True

    def subscribe(self):
        pub = self.__conn.pubsub()  # 开始订阅
        pub.subscribe(self.chan_sub)  # 订阅频道
        pub.parse_response()  # 准备接收
        return pub


if __name__ == '__main__':
    redis_helper = RedisHelper(host="192.168.1.184", port=6379, password="Baofang660", channel="history")  # 实例化
    redis_helper.publish("history")

    redis_helper = RedisHelper(host="192.168.1.184", port=6379, password="Baofang660", channel="latest")  # 实例化
    redis_helper.publish("latest")

    redis_helper = RedisHelper(host="192.168.1.184", port=6379, password="Baofang660", channel="close")  # 实例化
    redis_helper.publish("close")

