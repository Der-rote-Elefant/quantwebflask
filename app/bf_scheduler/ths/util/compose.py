#!/usr/bin/env python 
# -*- coding:utf-8 -*-
'''
@author: zcj
@file: MinBar.py
@time: 2019/3/8 14:30
'''

import pandas as pd
import os
import datetime
import copy
from queue import Queue
import threading
import pytz
from multiprocessing import Pool


class Bar:
    def __init__(self, row, code):
        self.code = code
        self.date = row['date']
        self.open = float(row['open'])
        self.high = float(row['high'])
        self.low = float(row['low'])
        self.close = float(row['close'])
        self.volume = float(row['volume'])
        self.op = float(row['openInterest'])
        self.vwap = self.op / self.volume if self.volume > 0 else 0

    def add_bar(self, next_bar, use_self_open=True):
        if not use_self_open:
            self.open = next_bar.open
        self.high = max(self.high, next_bar.high)
        self.low = min(self.low, next_bar.low)
        self.close = next_bar.close
        self.volume += next_bar.volume
        self.op += next_bar.op
        self.vwap = self.op / self.volume if self.volume > 0 else 0


class oneminbar:
    def __init__(self, code, df):
        self.code = code
        bar_list = []
        daystr = df['day'].iloc[0]
        datelist = [format(daystr + ' 09:30:00'), format(daystr + ' 13:00:00'), format(daystr + ' 09:31:00'),
                    format(daystr + ' 13:01:00')]
        amopenbar = None
        pmopenbar = None
        for index, row in df.iterrows():
            _bar = Bar(row, code)
            strbardate = row['date'].strftime("%Y-%m-%d %H:%M:%S")
            if strbardate == datelist[0]:
                amopenbar = _bar
                continue
            if strbardate == datelist[1]:
                pmopenbar = _bar
                continue
            if strbardate == datelist[2]:
                _bar.add_bar(amopenbar, 2)
            if strbardate == datelist[3]:
                _bar.add_bar(pmopenbar, 2)
            _bar.date = _bar.date + datetime.timedelta(minutes=-1)
            bar_list.append(_bar)
        self.bars = bar_list


class CycleBar(object):
    def __init__(self, barlist, cycle):
        self.barlist = barlist
        self.cycle = cycle
        self.DBName = format('THS_' + cycle)
        self.mongodbbarlist = []
        for i in barlist:
            self.mongodbbarlist.append(mongodbbar(i))


class mongodbbar(object):
    def __init__(self, bar):
        self.key = {
            '_id': format(bar.code + '_' + bar.date.strftime("%Y-%m-%d %H:%M:%S"))
        }
        self.value = {
            'Close': bar.close,
            'Code': bar.code,
            'Date': pacific.localize(bar.date),
            'High': bar.high,
            'Low': bar.low,
            'Open': bar.open,
            'OpenInterest': bar.op,
            'Volume': bar.volume,
            'VWAP': round(bar.vwap, 2)
        }


class mongodbbarlist(object):
    def __init__(self, barlist):
        self.list = []
        for bar in barlist:
            value = {
                '_id': format(bar.code + '_' + bar.date.strftime("%Y-%m-%d %H:%M:%S")),
                'Close': bar.close,
                'Code': bar.code,
                'Date': bar.date,
                'High': bar.high,
                'Low': bar.low,
                'Open': bar.open,
                'OpenInterest': bar.op,
                'Volume': bar.volume,
                'VWAP': round(bar.vwap, 2)
            }
            self.list.append(value)


class barlist:
    def __init__(self, path, Add=True):
        print(path)
        df = pd.DataFrame()
        if Add == False:
            df = pd.read_csv(path, names=['date', 'open', 'high', 'low', 'close', 'volume', 'openInterest'])
            code = code = os.path.split(path)[-1].split('.csv')[0]
            df['code'] = code
            df['day'] = df.apply(lambda x: x.date[0:10], axis=1)
        else:
            df = pd.read_csv(path, names=['code', 'date', 'open', 'high', 'low', 'close', 'volume', 'openInterest'])
            df['day'] = df['date'][0][0:10]
        df = df.fillna(0)
        codelist = df.drop_duplicates(['code'])
        for Code in codelist['code']:
            print(Code)
            codedf = df.loc[df['code'] == Code]
            DateList = []
            for i in codedf['day']:
                if i not in DateList:
                    DateList.append(i)
            for i in DateList:
                print(i)
                daydf = codedf.loc[df["day"] == i]
                daydf["date"] = pd.to_datetime(daydf["date"], format="%Y-%m-%d %H:%M")
                # daydf['date'] = daydf['date'].apply(lambda x: pacific.localize(x))
                daydf.sort_index(axis=0, ascending=True, by='date')
                oneminbarlist = oneminbar(Code, daydf)
                q.put(CycleBar(oneminbarlist.bars, '1Min'))
                FiveMinBars = ComposeBar(oneminbarlist.bars, 300)
                q.put(CycleBar(FiveMinBars, '5Min'))
                FifteenBars = ComposeBar(FiveMinBars, 900)
                q.put(CycleBar(FifteenBars, '15Min'))
                ThirtyBars = ComposeBar(FifteenBars, 1800)
                q.put(CycleBar(ThirtyBars, '30Min'))
                OneHourBars = ComposeBar(FifteenBars, 3200)
                q.put(CycleBar(OneHourBars, '60Min'))
                TwoHourBars = ComposeBar(FifteenBars, 6400)
                q.put(CycleBar(TwoHourBars, '120Min'))
                # print(oneminbarlist)


class Consumer(threading.Thread):
    def __init__(self, T_name, queue, max):
        threading.Thread.__init__(self, name=T_name)
        self.name = T_name
        self.q = queue
        self.max = max

    def run(self):
        while True:
            for i in range(self.max):
                val = self.q.get()
                print(self.name, i, len(val.barlist), val.cycle)
                DB.update_many(val.DBName, val, True)
                # q.task_done()


def list_all_files(dicpath):
    import os
    _files = []
    list = os.listdir(dicpath)  # 列出文件夹下所有的目录与文件
    for i in range(0, len(list)):
        path = os.path.join(dicpath, list[i])
        if os.path.isdir(path):
            _files.extend(list_all_files(path))
        if os.path.isfile(path):
            _files.append(path)
    return _files


def ComposeBar(barlist, cycle):
    NewBarlsit = []
    tempbar = None
    for i in barlist:
        if tempbar == None:
            tempbar = copy.copy(i)
            continue
        else:
            time = i.date - tempbar.date
            if time.seconds < cycle:
                tempbar.Add_Bar(i)
            else:
                NewBarlsit.append(tempbar)
                tempbar = copy.copy(i)
    if tempbar not in NewBarlsit:
        NewBarlsit.append(copy.copy(tempbar))
    return NewBarlsit


def Save_TO_MongBD(q):
    while True:
        minbar = q.get()
        # threading.Thread()
        print(len(minbar.barlist), minbar.cycle)
        # list = mongodbbarlist(minbar.barlist)
        # DB.update_many(minbar.DBName,list)
        # DB.insert_many(minbar.DBName,list)
        for i in minbar.barlist:
            mb = mongodbbar(i)
            DB.update_many(minbar.DBName, mb.key, mb.value, True)
        q.task_done()


def main():
    FilePath = "E:\THSData/1MIN"
    # DB_SK_Day = pymysql.connect("192.168.1.184", "root", "", "stock_day",charset='utf8')
    # Cursor_SK_DAY = DB_SK_Day.cursor()
    # StartYear = 0
    Path_List = list_all_files(FilePath)
    for path in Path_List:
        barlist(path)
        # fopen = open(path,'r')
        # StockCode = os.path.split(path)[-1].split('.csv')[0]
        # myfile = fopen.readlines()
        # IBarList = barlist(rows=myfile,stockcode=StockCode)
        # fopen.close()
        # if len(IBarList.bars) > 0:
        #     toSql(IBarList,DB_SK_Day)
        # print(path)


if __name__ == "__main__":
    starttime = datetime.datetime.now()
    pacific = pytz.timezone('Asia/Shanghai')
    q = Queue(15)
    w = Consumer('T', q, 5)
    w.start()
    # w = threading.Thread(target=Save_TO_MongBD,args=(q,))
    # w.setDaemon(True)
    # w.start()
    # ps = Pool(5)
    DB = MongoDBHelp('mongodb://root:Baofang660@192.168.1.184', 27017, 'Stock')
    print(DB.get_state())
    main()
    endtime = datetime.datetime.now()
    print("转换完成。")
    print(starttime, endtime)
    print((endtime - starttime).seconds)
    # ps.close()
    # ps.join()
    q.join()
    w.join()
