# -*- coding: utf-8 -*-
# @Time    : 2019/1/8 16:31
# @Author  : ddc
# @File    : WeChat.py

import urllib
import urllib.parse
import urllib.request


class Wechat(object):
	"""
	发送消息给微信公众号
	"""

	wechat_url = "http://www.wdl421.cn/api/WeChatMSGapi.php?user={0}&data={1}"

	def __init__(self, users=[]):
		self.users = users

	def send_message(self, message):
		for user in self.users:
			try:
				request_url = Wechat.wechat_url.replace("{0}", user)
				request_url = request_url.replace("{1}", urllib.parse.quote(message))
				urllib.request.urlopen(request_url)
			except Exception as e:
				print("用户：%s 发送微信公众号消息出错：%s" % (user, e))


if __name__ == '__main__':
	wechat = Wechat(users=["DDC"])
	wechat.send_message("test")
	print(wechat)
