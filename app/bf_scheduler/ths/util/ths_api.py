from iFinDPy import *
import pandas as pd
import math


class ThsApi(object):

    def __init__(self, user_id, password):
        self.user_id = user_id
        self.password = password

    def login(self):
        login_result = THS_iFinDLogin(self.user_id, self.password)
        if login_result == 0 or login_result == -201:
            return True
        else:
            error_msg = "'同花顺登录失败-error:%d-Time:%s' % (login_result, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))"
            print(error_msg)
            return False

    def logout(self):
        THS_iFinDLogout()

    def get_block_stocks(self, block_id, date_str, is_replace=True):
        params = "{0};{1}".format(date_str, block_id)
        data_pool = THS_DataPool('block', params, 'date:Y,thscode:Y,security_name:Y')
        df_data_pool = THS_Trans2DataFrame(data_pool)
        if isinstance(df_data_pool, type(pd.DataFrame())) and is_replace:
            if not df_data_pool.empty:
                df_data_pool["THSCODE"] = df_data_pool["THSCODE"].apply(lambda x: self.replace_code(x, is_index=False))
        return df_data_pool

    def get_basic_data(self, stock_list, date_str):
        params = "ths_is_mt_ss_underlying_stock"
        basic_data = THS_BasicData(stock_list, params, date_str)
        df_basic_data = THS_Trans2DataFrame(basic_data)
        if isinstance(df_basic_data, type(pd.DataFrame())):
            if not df_basic_data.empty:
                df_basic_data["thscode"] = df_basic_data["thscode"].apply(
                    lambda x: self.replace_code(x, is_index=False))
        return df_basic_data

    def get_index_component(self, index_code, date_str):
        params = "{0};{1}".format(date_str, index_code)
        data_pool = THS_DataPool('index', params, 'date:Y,thscode:Y,weight:Y')
        df_data_pool = THS_Trans2DataFrame(data_pool)
        if isinstance(df_data_pool, type(pd.DataFrame())):
            if not df_data_pool.empty:
                df_data_pool["THSCODE"] = df_data_pool["THSCODE"].apply(lambda x: self.replace_code(x, is_index=False))
        return df_data_pool

    def is_trade_day(self, date_str):
        date_offset = THS_DateOffset('SSE', 'dateType:0,period:D,offset:-0,dateFormat:0', date_str)
        if len(date_offset["tables"]) > 0 and date_offset["tables"]["time"][0] == date_str:
            return True
        else:
            return False

    def replace_code(self, stock_code, is_index=False):
        index = stock_code.index(".")
        if not is_index:
            return "%s.SK" % stock_code[:index]
        else:
            return "%s.SKIN" % stock_code[:index]

    def get_minute_bar(self, stock_list, start_date_str, end_date_str, param, column_order, interval=1, is_index=False):
        single_count = 500
        query_count = math.ceil(len(stock_list) / single_count)
        df_m1 = pd.DataFrame()
        for i in range(query_count):
            if i + 1 * single_count > len(stock_list):
                query_list = stock_list[i * single_count:len(stock_list)]
            else:
                query_list = stock_list[i * single_count:(i + 1) * single_count]

            print(",".join(query_list))
            ths_m1_sequence = THS_HighFrequenceSequence(",".join(query_list), param,
                                                        'CPS:0,MaxPoints:50000,Fill:Previous,Interval:{0}'.format(
                                                            interval),
                                                        start_date_str, end_date_str)
            ths_m1 = THS_Trans2DataFrame(ths_m1_sequence)
            ths_m1["thscode"] = ths_m1["thscode"].apply(lambda x: self.replace_code(x, is_index=is_index))
            df_m1 = df_m1.append(ths_m1)

        df_m1 = df_m1[column_order]
        # df_m1 = self.__filter_by_amount(df_m1)
        df_m1 = df_m1.fillna(0)
        return df_m1

    def get_realtime_bar(self, stock_list, param, column_order, is_index=False):
        ths_real_sequence = THS_RealtimeQuotes(",".join(stock_list), param)
        ths_real = THS_Trans2DataFrame(ths_real_sequence)
        print(ths_real)
        ths_real["thscode"] = ths_real["thscode"].apply(lambda x: self.replace_code(x, is_index=is_index))
        ths_real["close"] = ths_real["latest"]
        ths_real = ths_real[column_order]
        # ths_real = self.__filter_by_amount(ths_real)
        ths_real = ths_real.fillna(0)
        return ths_real

    def get_day_bar(self, stock_list, start_date_str, end_date_str, param, column_order, is_index=False):
        stock_code = ",".join(stock_list)
        adj_param = "close"
        day_bar = THS_HistoryQuotes(stock_code, param,
                                    'Interval:D,CPS:1,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                    start_date_str, end_date_str)
        day_adj = THS_HistoryQuotes(stock_code, adj_param,
                                    'Interval:D,CPS:7,baseDate:1900-01-01,Currency:YSHB,fill:Previous',
                                    start_date_str, end_date_str)
        df_day_bar = THS_Trans2DataFrame(day_bar)
        df_adj = THS_Trans2DataFrame(day_adj)
        df_day_bar["thscode"] = df_day_bar["thscode"].apply(lambda x: self.replace_code(x, is_index))
        df_day_bar["adj_close"] = df_adj["close"]
        df_day_bar = df_day_bar[column_order]
        # df_day_bar = self.__filter_by_amount(df_day_bar)
        df_day_bar = df_day_bar.fillna(0)
        return df_day_bar

    def __filter_by_amount(self, df):
        df = df.fillna(0)
        group_result = df["amount"].groupby(df["thscode"]).sum()
        amount_list = list()
        for code in group_result.keys():
            if group_result[code] > 0:
                amount_list.append(code)

        df = df[df["thscode"].isin(amount_list)]
        return df
