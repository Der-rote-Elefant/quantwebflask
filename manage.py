# !/usr/bin/env python
import os
from app import app_create

app = app_create(os.getenv('FLASK_CONFIG') or 'fang')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
